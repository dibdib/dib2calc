Dib2Calc (aka Dibdib Calculator)
========

The crazy calculator. Once you get used to it, you will love it :-)

A short introduction is provided when you start it for the first time. (Not
much ... - some more information **below**)

An installable APK file can be downloaded from:

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.gitlab.dibdib.dib2calc/)

https://f-droid.org/packages/com.gitlab.dibdib.dib2calc
(for stable versions)

[ https://www.magentacloud.de/share/x5x-ox77lf (for experimental (!) versions) ]: #

For a PC variant (Win/Mac/Linux) of this app: download latest file from
https://gitlab.com/dibdib/dib2j/-/jobs - and unpack (runs with almost any kind of
Java installation).

Known issues: see https://gitlab.com/dibdib/dib2calc/-/blob/master/known_issues.md

-----

<img src="misc/dib2calc_01.png" height="240">


-----

Some **additional information**:

    Note: This is a very early version. Do expect some major changes, and,
    unfortunately, also a lack of user documentation. Use it for 'playing',
    for the clipboard, and for contributing ... :-)

Basic ideas:

- Instead of having the keyboard cover important parts of the display, and
  having to reduce the size of the virtual keyboard, the whole display is
  used for typing.
- Not only numbers are processed, but also words. This calculator will provide
  some minimalistic string functions and work with binary representations.
- A value ('scalar') can be viewed as a single element of a 'sequence' (atomic
  vector). E.g., a sentence is viewed as a sequence of words, and a word is
  treated like a sentence with just that one word.
- Instead of calculating '(3 + 4) * 5' or the corresponding Lisp notation,
  the simple postfix (RPN) notation is used: '3 4 + 5 *'.

Usage:

- SWIPE does not work. Use the blue keys on the 4 sides instead (SCROLL).
- ESC (see figure) resets the view, a double click will exit (not yet).
- The ZOOM buttons (top left, next to ESC) change the text size.
- The main view is an overlay of keyboard buttons (blue and green) and the
  displayed lines of the calculator's stack (white/black): the first value on
  the stack to be popped is 'X', the second is 'Y', and so on.
- The MODE button changes the way of handling the virtual keyboard and the
  display.
- The first toolbar button LA-NG switches the language.
- The second button indicates and switches the view (VW). This will take you
  to the license, then to the current list of operators, and back.
- Note the blue keys of the keyboard: SCROLL (4), BACKSP, SHIFT (2+1), ENTER.
- GO is the button for applying (executing) operators (commands): type the
  command (e.g. 'ADD' or 'add', after entering 2 values), then tap GO.
- There is a shortcut for some basic operators ('+', '-', '*', '/', ...): they
  work immediately (i.e. without 'GO') in the calculator mode.
- The layer (LY) buttons are not implemented yet.
- The filter (QF-IL/QD-FC) applies to stored data mappings (experimental).

If it gets 'stuck' due to some cut-down:

- First go to the device's settings, clear the app data, and try again.
- If it still cannot load your data, then open a file manager, rename the
'Download/dibdib' folder (e.g. to 'Download/dd_OLD'), and start the app
a-fresh.

Try it out:

- When you run the app the first time, follow the short introduction.
- At the end you will realize that pressing ENTER/ PUSH for an empty text field
  will duplicate the top value.
- Then type '3 4 5 42' as a sequence with blanks, then PUSH, then '2', PUSH,
  and finally '*'. This multiplies each value of the sequence by 2.
- Press the 'VW' button ('VM-MA') several times to cycle through the views.
  Before getting back, you will find a list of the available operators. (Not all
  are fully implemented)
- Try the (blue) SHIFT keys (SHIFT-UP, SHIFT-DOWN): This will give you access
  to more characters.
- Try some letters followed by the extra SHIFT key (above ENTER). This will take
  you to corresponding international (Unicode) characters.
- You can exchange data with other apps via the CLIPBOARD (top right).


Your data is protected (encrypted) if you use your own access code and
password. In case you have a lot of data, make sure to save it to the device's
Download folder via the SAVTO function.

The app requires some extra processing in the background (e.g. for loading and
saving). Depending on your system settings, this might cause a few 'hiccups'
for the time being ...

-----

(Cmp. https://dib2x.github.io)

Copyright (C) 2016-2024  Roland Horsch <gx work
s(at)ma il.de >.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version (or, but only for the 'net.sf.dibdib'
    parts of the source code, the matching LGPL variant of GNU).

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License and the LICENSE file for more details.

See LICENSE file (= GPLv3-or-later: -- https : // www . gnu . org/ licenses/gpl.html --)
and further details under 'LICENSE_all' or 'assets' or 'resources'
(e.g. https://gitlab.com/dibdib/dib2j/blob/master/LICENSE_all)

(Impressum: IMPRESSUM.md @ github.com/dib2x/dib2x.github.io =
https://github.com/dib2x/dib2x.github.io/blob/main/IMPRESSUM.md)
