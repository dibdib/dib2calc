Compatibility

From https://www.apache.org/licenses/GPL-compatibility.html:

The Free Software Foundation considers the Apache License, Version 2.0 to be a free software license, compatible with version 3 of the GPL. The Software Freedom Law Center provides practical advice for developers about including permissively licensed source.

Apache 2 software can therefore be included in GPLv3 projects, because the GPLv3 license accepts our software into GPLv3 works. ...

