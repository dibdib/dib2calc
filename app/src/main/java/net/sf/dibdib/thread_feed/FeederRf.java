// Copyright (C) 2020, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_feed;

import static net.sf.dibdib.thread_any.StringFunc.*;

import com.gitlab.dibdib.picked.common.Codata;
import java.io.File;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Lang.AppState;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.ColorNmz.ColorDistinct;
import net.sf.dibdib.generic.QToken.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_io.IoRunner;
import net.sf.dibdib.thread_io.QOpIo;
import net.sf.dibdib.thread_ui.*;
import net.sf.dibdib.thread_wk.*;

// =====

/** 'View model' provider, enumeration as owner referencing actual implementation. */
public enum FeederRf {

  // =====

  DISCLAIMER("NN"),
  LICENSE("LC", "LICENSE"),
  HELP("HP"),
  ABOUT("AB"),
  LOGIN("LG"),
  INTROCALC("IN"),
  INTROCHAT("X0"),
  CALC("CC", "AWT", "TTY", "CO", "D", "J"),
  CHAT("CH", "QM"),
  // CALENDAR("CN"),
  ;

  // =====

  private FeederIf mFeeder;
  private final String[] mOptionalNames;

  private FeederRf(String... optionalNames) {
    mFeeder = null;
    mOptionalNames = optionalNames;
  }

  // =====

  public static interface FeederIf {

    // =====

    public FeederRf start();

    /**
     * Get initial/ next/ previous feeder.
     *
     * @param handle 0=initial, 1=next
     * @return feeder
     */
    // public FeederRf prepareFeeder(int handle, String... optionalName);

    //    public FeederIf setFeeder(int handle, FeederRf next);

    public int getCountSlides();

    public long getNumSlide30Supp();

    public int getPageOffset();

    public long findSlideSupplement(int vSlide, long bAbsRelSupp);

    public QScript prepareFeed(String... param);

    public QScript getLastFeed();

    /** Filter commands, exclusive usage: platform's UI thread, or mouse handler. */
    public QToken tryOrFilter4Ui(QToken out);
  }

  // =====

  /**
   * Get the actual implementation.
   *
   * @return implementer.
   */
  public FeederIf get() {
    if (null != mFeeder) {
      return mFeeder;
    }
    switch (this) {
      case DISCLAIMER:
        mFeeder = new GenericTextFeeder(this);
        break;
      case LICENSE:
        mFeeder = new LicenseFeeder(this);
        break;
      case HELP:
      case ABOUT:
        mFeeder = new HelpFeeder(this);
        break;
      case LOGIN:
        mFeeder = new LoginFeeder(this);
        break;
      case INTROCALC:
        mFeeder = new CalcFeeder.IntroCalc(this);
        break;
      case CALC:
        mFeeder = new CalcFeeder(this);
        break;
      case CHAT:
        mFeeder = new ChatFeeder(this);
        break;
      default:
        return null;
    }
    return mFeeder;
  }

  public String getShortId2() {
    return mOptionalNames[0];
  }

  public static FeederRf find(FeederIf impl) {
    for (FeederRf fd : FeederRf.values()) {
      if (impl.getClass().getName().toUpperCase(Locale.ROOT).contains(fd.name())) {
        return fd;
      }
    }
    return DISCLAIMER;
  }

  // =====

  public abstract static class GenericFeeder implements FeederIf {

    // =====

    protected final FeederRf me;
    protected int cSlides = 1;
    protected int jPage = 0;
    protected volatile QScript mFeed = null;
    protected volatile long nSlide30Supp = 1L << 30;
    protected volatile long nPointerSlide = -1;

    public GenericFeeder(FeederRf owner) {
      me = owner;
    }

    protected int linesPerSlide() {
      return -1 + (Dib2Constants.UI_PAGE_HEIGHT - 2 * Dib2Constants.UI_PAGE_MARGIN)
          / UiValFeedTag.UI_LINE_SPACING_PT10.i32Fut();
    }

    @Override
    public FeederRf start() {
      // UiValTag.UI_DISPLAY_SPLIT_CANVAS_X.setInitial(Dib2Constants.UI_DSPL_INIT_SPLIT_X);
      Dib2Root.app.feederNext = find(this);
      mFeed = null;
      return me;
    }

    public FeederRf getOwner() {
      return me;
    }

    // To be implemented:
    // public QScript prepareFeed(String... param) {

    @Override
    public QScript getLastFeed() {
      return mFeed;
    }

    @Override
    public QToken tryOrFilter4Ui(QToken cmd) {
      return cmd;
    }

    @Override
    public int getCountSlides() {
      return cSlides;
    }

    @Override
    public long getNumSlide30Supp() {
      return nSlide30Supp;
    }

    @Override
    public int getPageOffset() {
      return jPage;
    }

    @Override
    public long findSlideSupplement(int vNum, long bAbsRelSupp) {
      long nSlide = vNum;
      int supp = 0;
      if (4 == bAbsRelSupp) {
        nSlide = (nSlide30Supp >>> 30);
        supp = (int) (vNum + (nSlide30Supp & 0x3fffffff));
        supp = (0 <= supp) ? supp : 0;
        supp = ((1 << 20) > supp) ? supp : 0;
      } else if (2 == bAbsRelSupp) {
        nSlide = (nSlide30Supp >>> 30) + vNum;
      }
      nSlide = (cSlides <= nSlide) ? cSlides : nSlide;
      nSlide = (1 >= nSlide) ? 1 : nSlide;
      nSlide30Supp = supp | (nSlide << 30);
      return nSlide30Supp;
    }
  }

  // =====

  public static class GenericTextFeeder extends GenericFeeder {

    // =====

    protected int GenericTextFeeder_iLang = 0;
    protected long preparedTick = 0;
    protected long displayedTick = 0;

    public GenericTextFeeder(FeederRf owner) {
      super(owner);
    }

    protected String[] zFeedTxt = Dib2Lang.kUiAgree.clone();
    protected long[] zFeedRefs = null;
    protected int zFeedLinesSkip = 0;

    public void setText(String[] xmTxt) {
      zFeedTxt = xmTxt;
    }

    @Override
    public FeederRf start() {
      super.start();
      UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard] =
          //(Dib2Root.app.appState.ordinal() > Dib2Lang.AppState.DISCLAIMER.ordinal()) ? ColorDistinct.APPLE_GREEN.nmz.rgb0
          ColorDistinct.PURE_RED.nmz.rgb0;
      // Start with English:
      GenericTextFeeder_iLang = 0;
      UiValTag.kBarToolsDelegation = null;
      UiValTag.kBarTools = UiValTag.kBarTools_0;
      return me;
    }

    protected int splitTextLines(String[] all) {
      final int lines = linesPerSlide();
      if (100 > zFeedTxt.length) {
        zFeedTxt = Arrays.copyOf(zFeedTxt, (100 > lines) ? 100 : lines);
      }
      if (all.length > lines) {
        cSlides = 1 + (all.length / lines);
      }
      int start = ((int) (nSlide30Supp >>> 30) - 1) * lines;
      start = (all.length <= start) ? (all.length - 1) : start;
      int end = start + lines;
      end = (all.length < end) ? all.length : end;
      final int len = end - start;
      System.arraycopy(all, start, zFeedTxt, 0, len);
      if (len < zFeedTxt.length) {
        zFeedTxt[len] = null;
      }
      return len;
    }

    protected int prepareTextLines() {
      // Disclaimer and license as default
      String[] out =
          (Dib2Root.app.appState == Dib2Lang.AppState.LOGIN)
              ? Dib2Lang.kFeedLoadSave
              : Dib2Lang.kUiAgree;
      return splitTextLines(out);
    }

    @Override
    public QScript prepareFeed(String... param) {
      preparedTick = DateFunc.currentTimeNanobisLinearized(true);
      int c0 = prepareTextLines();
      String[] lines = zFeedTxt;
      long refEl = 0;
      int count = 0;
      QScript[] script = new QScript[4 + 6 * c0];
      final int linesp = UiValFeedTag.UI_LINE_SPACING_PT10.i32Fut();
      final int height = UiValFeedTag.UI_FONT_SIZE_PT10.i32Fut();
      final int offsX = Dib2Constants.UI_PAGE_MARGIN;
      script[count++] = QScript.makeScriptEl(++refEl, QOpGraph.POSY, Dib2Constants.UI_PAGE_MARGIN);
      final int base =
          linesp
              - ((height * Dib2Constants.UI_FONT_NMZ_DESCENT >> Dib2Constants.UI_FONT_NMZ_SHIFT));
      script[count++] = QScript.makeScriptEl(++refEl, QOpGraph.RBASE, base);
      for (int i0 = 0; i0 < c0; i0 += 1 + zFeedLinesSkip) {
        refEl = (null != zFeedRefs) ? zFeedRefs[i0] : ((i0 * 2 + 1L) << 31);
        String[] tabbed = (null == lines[i0]) ? new String[0] : lines[i0].split("\t");
        if ((count + tabbed.length * 2 + 2) >= script.length) {
          script = Arrays.copyOf(script, 2 * (script.length + tabbed.length));
        }
        int posx = offsX;
        for (String s0 : tabbed) {
          script[count++] = QScript.makeScriptEl(++refEl, QOpGraph.POSX, posx);
          if ((0 >= i0) && s0.equals("@")) { // lines[i0].equals("@")) { //"\t@") && s0.startsWith("@")) {
            script[count++] = QScript.makeScriptEl(++refEl, QOpGraph.ENTRY);
          } else {
            script[count++] = QScript.makeScriptEl(++refEl, QOpGraph.TEXT, s0);
          }
          posx += UiFunc.boundWidthNmz(s0 + 'm', height);
          posx = offsX + (((posx - offsX - 2) / Dib2Constants.UI_PAGE_NMZ_TAB) + 1) * Dib2Constants.UI_PAGE_NMZ_TAB;
        }
        script[count++] = QScript.makeScriptEl(++refEl, QOpGraph.TXLF);
      }
      if ((0 <= ClickRepeater.qPointerX0) && (0 <= ClickRepeater.qPointerY0) && (nPointerSlide == nSlide30Supp)) {
        script[count] = QScript.makeScriptEl(++refEl, QOpGraph.POINTER);
        script[count].parX = ClickRepeater.qPointerX0;
        script[count].parY = ClickRepeater.qPointerY0;
        script[count].parN0 = ColorNmz.ColorDistinct.APPLE_GREEN.argbQ(1);
        ++count;
      }
      QScript out = QScript.makeScript(0);
      out.script = script;
      out.cScript = count;
      // Atomic:
      mFeed = out;
      return out;
    }

    @Override
    public QScript getLastFeed() {
      displayedTick = DateFunc.currentTimeNanobisLinearized(true);
      if (null == mFeed) {
        mFeed = prepareFeed();
      }
      return mFeed;
    }

    @Override
    public QToken tryOrFilter4Ui(QToken cmd) {
      ///// For disclaimer and license.
      boolean disclaimer = (Dib2Root.app.appState.ordinal() <= Dib2Lang.AppState.DISCLAIMER.ordinal());
      if ((QOpFeed.zzAPPLY == cmd.op) || (QOpFeed.zzPUSH == cmd.op)) {
        Dib2Root.app.feederNext = (Dib2Root.app.bAllowDummyPass || !disclaimer) ? Dib2Root.app.mainFeeder : LOGIN;
      } else if ((QOpUi.zzKEY == cmd.op) && (StringFunc.STEP == cmd.parX)) {
        // Tapped.
        if ((displayedTick < preparedTick) || (ClickRepeater.qUiPointerTick < displayedTick)) {
          // Mismatch
          return null;
        }
        // Ok.
        nPointerSlide = nSlide30Supp;
        // TODO Have it calculated with prepareFeed for varying sizes ...
        return cmd;
        //} else if (QOpFeed.zzCHATRECV == cmd.op) {
        //  cmd.op = QOpNet.RCV;
        //  cmd.argX = QWord.createQWord(20 * 60.0);
        //  //Dib2Root.schedulerTrigger.trigger(cmd);

      } else {
        // Get name in case of delegated operation:
        final String nam = cmd.op.name();
        if ("ESCAPE".equals(nam)) {
          if (disclaimer) {
            Dib2Root.app.appState = AppState.EXIT_DONE;
            return null;
          }
          Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
        } else if ((QOpMain.NOP == cmd.op) || (QOpMain.DUP == cmd.op)) {
          final FeederRf next = (!disclaimer)
              ? (FeederRf) Dib2Root.app.mainFeeder
              : (((CALC == Dib2Root.app.mainFeeder) && Dib2Root.app.bAllowDummyPass) ? INTROCALC : LOGIN);
          Dib2Root.app.feederNext = next;
        } else {
          return (cmd.op instanceof QOpMain) ? null : cmd;
        }
      }
      if (Dib2Root.app.appState.ordinal() < Dib2Lang.AppState.ACTIVE.ordinal()) {
        Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
      }
      GenericTextFeeder_iLang = -1;
      return null;
    }
  }

  // =====

  static class LicenseFeeder extends GenericTextFeeder {

    private String[] GenericTextFeeder_license = null;

    public LicenseFeeder(FeederRf owner) {
      super(owner);
    }

    @Override
    public FeederRf start() {
      super.start();
      // Language might have changed ...
      GenericTextFeeder_iLang = -1;
      UiValTag.kBarTools = UiValTag.kBarTools_0;
      // Window not to be split:
      final QToken token = QToken.createTask(QOpUi.zzSET, UiValTag.UI_PANE_SPLIT_GAP_X);
      token.parX = 0;
      UiPres.INSTANCE.wxGateIn4Feed.push(token);
      return me;
    }

    protected int prepareTextLines() {
      if ((Dib2Root.ui.iLang != GenericTextFeeder_iLang) || (null == GenericTextFeeder_license)) {
        GenericTextFeeder_iLang = Dib2Root.ui.iLang;
        GenericTextFeeder_license =
            Dib2Root.platform.getLicense(Dib2Lang.pickTransl(Dib2Lang.kLicensePre));
      }
      GenericTextFeeder_license[2] = "(Me: " + Dib2Root.ccmSto.mUserAddr + ")";
      return splitTextLines(GenericTextFeeder_license);
    }

    // =====

  }

  // =====

  static class HelpFeeder extends GenericTextFeeder {

    // =====

    public HelpFeeder(FeederRf owner) {
      super(owner);
    }

    private static String[] getHelp_lines = null;

    static String[] getHelp() {
      if (null == getHelp_lines) {
        int count = 0;
        String[] lines =
            new String
                [3
                    + QOpMain.values().length
                    + 1
                    + Codata.values().length
                    + 2
                    + DateFunc.DateFormat.values().length
                    + 2];
        lines[count++] =
            "V. " + Dib2Constants.VERSION_STRING + ". " + Dib2Constants.NO_WARRANTY[0];
        lines[count++] =
            "List of available FUNCTIONS (see below, e.g.:"
                + (Dib2Root.ui.bTerminalMode
                    ? " type '\\' + file name, press ENTER, ';EXPORT', ENTER):"
                    : " ^FileName, ^ENTER, 'EXPORT', GO):");
        lines[count++] = "(Not fully implemented yet !)";
        if (!Dib2Root.ui.bTerminalMode) {
          lines[count++] = "(E.g. type '3', press > or ENTER, '4', > or ENTER,";
          lines[count++] = "type 'ADD', (press > or ENTER,) press GO)";
        }
        for (QOpMain funct : QOpMain.values()) {
          String descr = funct.getDescription();
          if ('.' != descr.charAt(0)) {
            lines[count++] = descr;
          }
        }
        if (Dib2Root.ui.bTerminalMode) {
          lines[count++] = "(Use with preceding ';' for commands, '\\' for data.";
          lines[count++] =
              "E.g.: press '\\', type file name, press ENTER, type ';EXPORT', press ENTER)";
        }
        if (!Dib2Root.ui.bTerminalMode) {
          lines[count++] = "";
          lines[count++] = "Constants:";
          for (Codata val : Codata.values()) {
            lines[count++] = val.name() + '\t' + val.quantity;
          }
          lines[count++] = "";
          lines[count++] = "Date format:";
          for (DateFunc.DateFormat val : DateFunc.DateFormat.values()) {
            lines[count++] = val.name() + '\t' + val.descr;
          }
        }
        getHelp_lines = Arrays.copyOf(lines, count);
      }
      return getHelp_lines;
    }

    @Override
    protected int prepareTextLines() {
      return splitTextLines(getHelp());
    }

    @Override
    public QToken tryOrFilter4Ui(QToken cmd) {
      return (cmd.op instanceof QOpMain) ? null : cmd;
    }
  }

  // =====

  public static class LoginFeeder extends GenericTextFeeder {

    // =====

    private String mPathDataFile = null;
    private long mnLoading = 0;
    private boolean mbAccessCodeFirst = true;
    private long mbEmailfirstIntro = 0;
    private String[] zTxt = null;
    // long lastUpdateNanos = 0;

    public LoginFeeder(FeederRf owner) {
      super(owner);
      zTxt = null;
      cSlides = 1;
    }

    public void reset() {
      mbAccessCodeFirst = true;
      mbEmailfirstIntro = 0;
      //lastUpdateNanos = 0;
      mnLoading = 0;
      if (!Dib2Root.app.bAllowDummyPass) {
        TcvCodec.instance.settleHexPhrase("");
      }
      UiPres.INSTANCE.zUiKeypadInx = 2;
      UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard] = ColorDistinct.APPLE_GREEN.nmz.rgb0;
      final QToken token = QToken.createTask(QOpUi.zzSET, UiValTag.UI_PANE_SPLIT_GAP_X);
      token.parX = 0;
      UiPres.INSTANCE.wxGateIn4Feed.push(token);
    }

    @Override
    public FeederRf start() {
      super.start();
      reset();
      if (null == mPathDataFile) {
        mPathDataFile = IoRunner.check4Load();
      }
      return me;
    }

    public void setPath(String pathDataFile) {
      mPathDataFile = pathDataFile;
      reset();
    }

    public void requestPhrase() {
      mbAccessCodeFirst = false;
      //lastUpdateNanos = 0;
    }

    @Override
    protected int prepareTextLines() {
      final long nanos = DateFunc.currentTimeNanobisLinearized(false);
      if (1 < (nSlide30Supp & 0x3fffffff)) {
        nSlide30Supp = ((nSlide30Supp >>> 30) << 30) | 1;
      }
      int page = (int) (nSlide30Supp >>> 30);
      if (page > cSlides) {
        nSlide30Supp = ((long) cSlides) << 30;
      }
      if ((0 < mnLoading) && (mnLoading > (nanos + (3L << 30)))) {
        zFeedTxt = Dib2Lang.kUiStepAcLoad_x;
        return zFeedTxt.length;
      } else if (0 < mnLoading) {
        mnLoading = 0;
      }
      if (null == mPathDataFile) {
        mPathDataFile = IoRunner.check4Load();
      }
      boolean emailFirst = (!Dib2Root.app.bAllowDummyPass
          && ((null == Dib2Root.ccmSto.mUserAddr) || (0 >= Dib2Root.ccmSto.mUserAddr.indexOf('@'))
              || (null == Dib2Root.ccmSto.hidden_get("smtp_user")))
          && ((null == mPathDataFile) || !(new File(mPathDataFile)).exists()));
      mbEmailfirstIntro = emailFirst ? ((0 == mbEmailfirstIntro) ? 3 : mbEmailfirstIntro) : 0;
      cSlides = 1;
      if (0 != (2 & mbEmailfirstIntro)) {
        if (null == zTxt) {
          UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard] = ColorDistinct.PURE_RED.nmz.rgb0;
        }
        cSlides = Dib2Lang.kUiIntroChat.length;
        page = (page > cSlides) ? cSlides : page;
        zTxt = (0 == (nSlide30Supp & 0xffL)) ? Dib2Lang.kUiIntroChat[page - 1] : Dib2Lang.kUiIntroChat_Supp;
      } else if (0 != mbEmailfirstIntro) {
        zTxt = ((null == Dib2Root.ccmSto.mUserAddr) || (0 >= Dib2Root.ccmSto.mUserAddr.indexOf('@')))
            ? Dib2Lang.kUiNeedEMailAddress
            : Dib2Lang.kUiNeedEMailHost;
        zTxt[0] = "\t@"; // + UiPres.INSTANCE.getEntry(true);
      } else {
        final String[] info = // TcvCodec.instance.setAccessCode(null) ? Dib2Lang.kUiStepPw :
            // Dib2Lang.kUiStepAc;
            //            Dib2Lang.pickTransl(
            (Dib2Root.app.bAllowDummyPass && mbAccessCodeFirst) ? Dib2Lang.kUiStepAcOpt
                : (mbAccessCodeFirst ? Dib2Lang.kUiStepAc : Dib2Lang.kUiStepPw);
        final String[] optionalPaths = IoRunner.listPaths(Dib2Constants.MAGIC_BYTES_STR, false, "main", "external");
        zTxt = Arrays.copyOf(info, 3 + info.length + optionalPaths.length);
        int i1 = info.length;
        zTxt[i1++] = "==> " + ((null == mPathDataFile) ? Dib2Root.app.dbFileName
            : (mPathDataFile.substring(1 + mPathDataFile.lastIndexOf('/'))) + '\t' + mPathDataFile);
        zTxt[i1++] = "";
        for (int i0 = 0; i0 < optionalPaths.length; ++i0) {
          zTxt[i1++] = optionalPaths[i0].substring(1 + optionalPaths[i0].lastIndexOf('/')) + '\t' + optionalPaths[i0];
        }
      }
      //}
      zFeedTxt = zTxt;
      return zFeedTxt.length;
    }

    @Override
    public QToken tryOrFilter4Ui(QToken cmd) {
      //lastUpdateNanos = 0;
      String entry = cmd.parS0;
      cmd.parS0 = null;

      if (0 != mbEmailfirstIntro) {
        boolean go = false;
        if ((QOpFeed.zzAPPLY == cmd.op) || (QOpFeed.zzPUSH == cmd.op)) {
          go = true;
        } else if (!(cmd.op instanceof QOpMain)) {
          if ((QOpUi.zzKEY == cmd.op) || (QOpUi.zzKEY_REP == cmd.op)) {
            final char key = (char) cmd.parX;
            switch (key) {
              case CR:
              case LF:
              case PUSH:
                go = true;
                break;
              default:
                return cmd;
            }
          }
        }
        if (go) {
          mbEmailfirstIntro = 1;
          UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard] = ColorDistinct.APPLE_GREEN.nmz.rgb0;
        }
        go = go && ((null != cmd.argX) || (null != entry));
        if (!go) {
          return null;
        }
        if (null != entry) {
          cmd.argX = QSeq.createQSeq(entry);
        }
        cmd.op = ((null == Dib2Root.ccmSto.mUserAddr) || (0 >= Dib2Root.ccmSto.mUserAddr.indexOf('@'))) ? QOpMain.QME
            : QOpMain.QHOST;
        if (QOpMain.QME == cmd.op) {
          if (null == entry) {
            entry = ((QSeq) cmd.argX).toStringFull();
          }
          int iSep = entry.indexOf('@');
          if (0 >= iSep) {
            return null;
          }
          // Show the hostname, ports etc. as suggestion:
          String entry2 = entry.substring(iSep + 1) + " " + entry + " 993 587";//.swapped
          QToken extra = QToken.createTask(QOpUi.zzENTRY, QSeq.createQSeq(entry2));
          Dib2Root.schedulerTrigger.trigger(extra);
          //MainThreads.push(extra);
          // First chat:
          extra = QToken.createTask(QOpWk.zzQSTO, QSeq.createQSeq(entry));
          extra.op = QOpWk.zzQSTO;
          extra.argX = QSeq.createQSeq(":TOPIC: " + entry.substring(0, iSep) + "\n:AT: " + entry);
          extra.parN0 = Cats.CHAT.flag;
          extra.parS0 = entry;
          Dib2Root.schedulerTrigger.trigger(extra);
          //MainThreads.push(extra);
        }
        return cmd;
      }

      if ((QOpFeed.zzAPPLY == cmd.op) || (QOpFeed.zzPUSH == cmd.op)) {
        cmd.op = QOpMain.NOP;
      }
      if (cmd.op instanceof QOpMain) {
        if (null == mPathDataFile) {
          mPathDataFile = IoRunner.check4Load();
        }
      } else {
        if (QOpUi.zzKEY == cmd.op) {
          final char key = (char) cmd.parX;
          switch (key) {
            case CR:
            case LF:
            case PUSH:
              cmd.op = QOpMain.NOP;
              return cmd;
            case StringFunc.XCOPY:
              IoRunner.backupFiles("main", "external");
              return null;
            case StringFunc.XPASTE:
              File dirFrom = Dib2Root.platform.getFilesDir("external");
              File dirTo = Dib2Root.platform.getFilesDir("main");
              if ((null == dirFrom)
                  || (null == dirTo)
                  || (dirFrom.equals(dirTo)
                      || !dirFrom.exists()
                      || !dirTo.exists()
                      || !dirFrom.isDirectory())
                  || !dirTo.isDirectory()) {
                return null;
              }
              File from = new File(dirFrom, Dib2Root.app.dbFileName);
              if (!from.exists()) {
                return null;
              }
              File to = new File(dirTo, Dib2Root.app.dbFileName);
              if (to.exists()) {
                File to2 = new File(dirTo, Dib2Root.app.dbFileName + ".x.bak");
                if (to2.exists()) {
                  to2.delete();
                }
                to.renameTo(to2);
              }
              IoRunner.copyFile(from, to);
              mPathDataFile = IoRunner.check4Load();
              return null;
            default:
              return cmd;
          }
        }
        return (cmd.op instanceof QOpUi) ? cmd : null;
      }
      cmd.argX = null;
      mnLoading = 0;
      switch ((QOpMain) cmd.op) {
        case ESCAPE:
          File path;
          if ((null != mPathDataFile) && (path = new File(mPathDataFile)).exists()) {
            String name = DateFunc.dateShort4Millis().substring(4, 6);
            name = mPathDataFile.replace(".dm", "." + name + ".bak");
            File path2 = new File(name);
            if (path2.exists()) {
              path2.delete();
            }
            path.renameTo(path2);
            mPathDataFile = null;
          }
          if (Dib2Root.app.bAllowDummyPass) {
            TcvCodec.instance.setDummyPhrase(true);
            Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
            Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
          } else if (TcvCodec.instance.setAccessCode(null)
              && (4 < TcvCodec.instance.getPassFull().length)) {
            if (null == mPathDataFile) {
              Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
              Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
              mnLoading = 0;
            } else {
              final long nanos = DateFunc.currentTimeNanobisLinearized(false);
              mnLoading = nanos;
            }
          } else {
            // Clear it:
            TcvCodec.instance.setAccessCode(new byte[0]);
            reset();
          }
          if (0 >= mnLoading) {
            return null;
          }
          break;
          // case zzEXEC:
        case NOP:
          ///// Considering the special case of AC == PW ...
          if ((null != entry) && (0 < entry.length())) {
            if (mbAccessCodeFirst) {
              TcvCodec.instance.settleHexPhrase("");
              //boolean pwFromFile = !TcvCodec.instance.settleHexPhrase(null);
              if (TcvCodec.instance.setAccessCode(StringFunc.bytesUtf8(entry))) {
                if ((2 <= entry.length())
                ){ //&& ((null == TcvCodec.instance.getPassPhraseHex())
                    //    || (4 >= TcvCodec.instance.getPassPhraseHex().length()))) {
                  if (TcvCodec.instance.settleHexPhrase(null)) {
                    //if (pwFromFile) {
                      final long nanos = DateFunc.currentTimeNanobisLinearized(false);
                      mnLoading = nanos;
                    //}
                  }
                }
              }
            } else {
              if (TcvCodec.instance.settleHexPhrase(StringFunc.hexUtf8(entry, false))) {
                final long nanos = DateFunc.currentTimeNanobisLinearized(false);
                mnLoading = nanos;
              }
            }
            mbAccessCodeFirst = !mbAccessCodeFirst;
            //lastUpdateNanos = 0;
          } else if (Dib2Root.app.bAllowDummyPass) {
            TcvCodec.instance.setDummyPhrase(true);
            reset();
            Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
            return null;
          }
          // final byte[] temp = TcvCodec.instance.getPassFull();
          if (0 >= mnLoading) {
            return null;
          }
          break;
        default:
          ;
      }
      if (0 < mnLoading) {
        if (null == mPathDataFile) {
          File dir = Dib2Root.platform.getFilesDir("main");
          if ((null == dir) || !(new File(dir, Dib2Root.app.dbFileName).exists())) {
            mPathDataFile = IoRunner.findLatest(Dib2Root.platform.getFilesDir("main"), false);
          } else {
            mPathDataFile = Dib2Root.app.dbFileName;
          }
        }
        if ((null == mPathDataFile) && TcvCodec.instance.settleHexPhrase(null)) {
          Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
          Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
        } else if (null != mPathDataFile) {
          cmd.op = QOpIo.zzLOAD_INITIAL;
          cmd.argX = QWord.createQWord(mPathDataFile, true);
          QOpFeed.zStack.insert(0, cmd.pushWip4Seq(null));
          Dib2Root.schedulerTrigger.trigger(cmd);
        } else {
          mnLoading = 0;
        }
      }
      return null;
    }
  }

  // =====

  public static FeederRf findFeeder(String name) {
    name = name.toUpperCase(Locale.ROOT);
    FeederRf feeder;
    try {
      feeder = FeederRf.valueOf(name);
    } catch (Exception e) {
      feeder = null;
    }
    if (null == feeder) {
      for (FeederRf fx : FeederRf.values()) {
        for (String opt : fx.mOptionalNames) {
          if (opt.equals(name)) {
            feeder = fx;
            break;
          }
        }
      }
      if (null == feeder) {
        return null;
      }
    }
    return feeder;
  }

  // =====
}
