// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_ui;

import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Lang.AppState;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.generic.QToken.QScript;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_feed.*;

// =====

public final class UiFrame {

  // =====

  /** Canvas width = frame width + 1 if using border lines, defaults to page width. */
  public static volatile int qWidthCanvasPx = Dib2Constants.UI_PAGE_INIT;

  /** Canvas height = frame height + 1 if using border lines, defaults to page width. */
  public static volatile int qHeightCanvasPx = Dib2Constants.UI_PAGE_INIT;

  /** 2*72 dpi as base value. */
  private static int zShift2PxMajor = Dib2Constants.UI_PT10_SHIFT - 1;

  private static boolean zbShift2PxMinor = false;
  // private static int zShift2Px_0 = Dib2Constants.UI_PT10_SHIFT - 1;

  private static int zShift2Px4Frame = zShift2PxMajor;
  private static boolean zShiftX4Frame = zbShift2PxMinor;
  private static int zColorOverride = 0;

  // =====

  private static int shift2Px(int val) {
    final int shift = zShift2PxMajor;
    if (zbShift2PxMinor) {
      return (val >> shift) - (val >> (shift + 2));
    }
    return (val >> shift);
  }

  static int shift4Px(int val) {
    final int shift = zShift2PxMajor;
    if (zbShift2PxMinor) {
      return (val << (shift + 2)) / 3 + ((0 < val) ? 1 : 0);
    }
    return (val << shift);
  }

  private static void pushShift4Canvas() {
    final int zoom = UiValTag.UI_ZOOMLVL_BOARD.i32Fut() - (zbShift2PxMinor ? 1 : 0);
    zShift2Px4Frame = zShift2PxMajor;
    zShiftX4Frame = zbShift2PxMinor;
    zShift2PxMajor = zShift2PxMajor - ((zoom + 1) >> 1);
    zShift2PxMajor = (0 > zShift2PxMajor) ? 0 : zShift2PxMajor;
    zbShift2PxMinor = (0 != (zoom & 1));
  }

  private static void popShift4Canvas() {
    zShift2PxMajor = zShift2Px4Frame;
    zbShift2PxMinor = zShiftX4Frame;
  }

  /** For platform's UI thread, exclusively! */
  public int getTextSize4Ui() {
    final int fontSizePt10 = UiValTag.UI_FONT_SIZE_WIN_PT10.i32Fut();
    return shift4Px(fontSizePt10);
  }

  /** Canvas width = frame width + 1 if using border lines. Size as virtual paper size. */
  public static void checkSizeNZoom(int xWidthPx, int xHeightPx, int xSizeMinPt10) {
    xWidthPx =
        (Dib2Constants.UI_FRAME_SIZE_MIN_PX > xWidthPx)
            ? Dib2Constants.UI_FRAME_SIZE_MIN_PX
            : xWidthPx;
    xHeightPx =
        (Dib2Constants.UI_FRAME_SIZE_MIN_PX > xHeightPx)
            ? Dib2Constants.UI_FRAME_SIZE_MIN_PX
            : xHeightPx;
    if ((qWidthCanvasPx == xWidthPx) && (qHeightCanvasPx == xHeightPx)) {
      return;
    }
    qWidthCanvasPx = xWidthPx;
    qHeightCanvasPx = xHeightPx;
    // Check virtual display size.
    if ((xSizeMinPt10 < Dib2Constants.UI_WIN_SIZE_MIN_PT10)
        || (Dib2Constants.UI_WIN_SIZE_MAX_PT10 < xSizeMinPt10)) {
      xSizeMinPt10 =
          (Dib2Constants.UI_WIN_SIZE_MAX_PT10 < xSizeMinPt10)
              ? Dib2Constants.UI_WIN_SIZE_MAX_PT10
              : Dib2Constants.UI_WIN_SIZE_MIN_PT10;
    }
    final int min = (xWidthPx < xHeightPx) ? xWidthPx : xHeightPx;
    int dpi = min * Dib2Constants.UI_PT10_P_INCH / xSizeMinPt10;
    if ((min / dpi) < Dib2Constants.UI_DISPLAY_SIZE_MIN_INCH) {
      // Display too small even for short text lines => pretend ...
      dpi = min / Dib2Constants.UI_DISPLAY_SIZE_MIN_INCH;
    } else if ((min / dpi) > Dib2Constants.UI_DISPLAY_SIZE_MAX_INCH) {
      dpi = min / Dib2Constants.UI_DISPLAY_SIZE_MAX_INCH;
    }
    int level = -1;
    zbShift2PxMinor = (dpi < (2 * Dib2Constants.UI_PT_P_INCH / 3));
    // Calculate with dpp, rounded up:
    for (int dpp = (dpi * 2 + 10) / Dib2Constants.UI_PT_P_INCH; dpp != 0; dpp /= 2) {
      ++level;
      zbShift2PxMinor = zbShift2PxMinor || (2 == dpp);
    }
    int shifts = (Dib2Constants.UI_PT10_SHIFT <= level) ? 0 : (Dib2Constants.UI_PT10_SHIFT - level);
    zShift2PxMajor = shifts;
    if (shift4Px(min) < (5 * Dib2Constants.UI_WIN_SIZE_MIN_PT10 / 4)) {
      if (zbShift2PxMinor) {
        zbShift2PxMinor = false;
        ++shifts;
      } else {
        zbShift2PxMinor = true;
      }
    }
    // zShift2Px_0 = shifts;
    zShift2PxMajor = shifts;
    final int widPt10 = shift4Px(xWidthPx);
    final int heightPt10 = shift4Px(xHeightPx);
    UiValTag.UI_WIN_WIDTH.setInitial(widPt10);
    UiValTag.UI_WIN_WIDTH.setFut(widPt10);
    UiValTag.UI_WIN_HEIGHT.setInitial(heightPt10);
    UiValTag.UI_WIN_HEIGHT.setFut(heightPt10);
    int zoomLvl = (zbShift2PxMinor) ? -1 : 0;
    if (widPt10 < (3 * Dib2Constants.UI_WIN_SIZE_MIN_PT10 / 2)) {
      zoomLvl = -1;
    } else {
      for (int rel = ((widPt10 < heightPt10) ? widPt10 : heightPt10) / Dib2Constants.UI_PAGE_INIT;
          rel != 0;
          rel /= 2) {
        ++zoomLvl;
      }
    }
    UiValTag.UI_ZOOMLVL_BOARD.setInitial(zoomLvl);
    UiValTag.UI_ZOOMLVL_BOARD.setFut(zoomLvl);
  }

  private boolean setContextVals(GraphicsIf g, QOpGraph.GraphContext ctx) {
    g.setColor((null == ctx) ? ColorNmz.ColorDistinct.FG__BLACK.argbQ(1) : ctx.color);
    g.setMatching128FontHeight(
        1
            + shift2Px(
                (null == ctx) ? UiValTag.UI_FONT_SIZE_WIN_PT10.getInitial() : ctx.heightPt10));
    //  g.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING,
    // RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
    return true;
  }

  private boolean paintScript(
      GraphicsIf g,
      int xjXPx,
      int xjYPx,
      long bFramePointer,
      QOpGraph.GraphContext previousVals,
      int cScripts,
      QScript... xScripts) {
    // Atomic!
    QScript[] script = xScripts;
    cScripts = (0 <= cScripts) ? cScripts : xScripts.length;
    if (null == script) {
      // Race condition.
      return false;
    }
    QOpGraph.GraphContext ctx =
        QOpGraph.makeScriptContext(previousVals, (0 != (1 & bFramePointer)));
    int jXLine = ctx.xPt10;
    if (null == previousVals) {
      g.setColor(ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
      ctx.zHeightPx = g.setMatching128FontHeight(1 + shift2Px(ctx.heightPt10));
    }
    int nx = 0;
    String sText = null;
    for (int iS = 0; iS < cScripts; ++iS) {
      final QScript scrEl = script[iS];
      if (null == scrEl) {
        break;
      } else if (null != scrEl.script) {
        // TODO without recursion
        if (script != scrEl.script) {
          paintScript(g, xjXPx, xjYPx, bFramePointer, ctx, scrEl.cScript, (QScript[]) scrEl.script);
        }
        continue;
      } else if (null == scrEl.op) {
        continue;
      }
      switch ((QOpGraph) scrEl.op) {
        case ARC:
          break;
        case CURVE:
          break;
        case DASH:
          break;
        case ENTRY:
          sText = UiPres.INSTANCE.getEntry(true);
          nx = sText.length();
          if (35 < nx) {
            sText = sText.substring(0, 10) + "..." + sText.substring(nx - 20, nx);
          }
          g.drawText(sText, xjXPx + shift2Px(ctx.xPt10), xjYPx + 1 + shift2Px((ctx.yPt10 + ctx.eBasePt10)));
          break;
        case FACE:
          nx = 1 + shift2Px(ctx.heightPt10);
          ctx.zHeightPx = g.setMatching128Font(nx, scrEl.parS0, scrEl.parX % 3);
          break;
        case HEIGHT:
          ctx.heightPt10 = scrEl.parX;
          ctx.zHeightPx = g.setMatching128FontHeight(1 + shift2Px(ctx.heightPt10));
          break;
        case IMAGE:
          break;
          //    case LEADING:
          //      break;
        case LINE:
          g.drawLine(
              xjXPx + shift2Px(ctx.xPt10),
              xjYPx + shift2Px(ctx.yPt10),
              xjXPx + shift2Px(scrEl.parX),
              xjYPx + shift2Px(scrEl.parY));
          //ClickRepeater.markPointedElement(scrEl.hScriptElement, ctx.xPt10, ctx.yPt10, scrEl.parX, scrEl.parY);
          ctx.xPt10 = scrEl.parX;
          ctx.yPt10 = scrEl.parY;
          break;
        case LNCAP:
          break;
        case LNJOIN:
          break;
        case LNWIDTH:
          break;
        case POINTER:
          int plen = (3 << Dib2Constants.UI_PT10_SHIFT);
          nx = ctx.color;
          ctx.color = (int) scrEl.parN0;
          g.setColor(ctx.color);
          g.drawLine(xjXPx + shift2Px(scrEl.parX), xjYPx + shift2Px(scrEl.parY - plen),
              xjXPx + shift2Px(scrEl.parX), xjYPx + shift2Px(scrEl.parY + plen) + 1);
          g.drawLine(xjXPx + shift2Px(scrEl.parX - plen), xjYPx + shift2Px(scrEl.parY),
              xjXPx + shift2Px(scrEl.parX + plen) + 1, xjYPx + shift2Px(scrEl.parY));
          g.drawLine(xjXPx + shift2Px(scrEl.parX) + 1, xjYPx + shift2Px(scrEl.parY - plen),
              xjXPx + shift2Px(scrEl.parX) + 1, xjYPx + shift2Px(scrEl.parY + plen) + 1);
          g.drawLine(xjXPx + shift2Px(scrEl.parX - plen), xjYPx + shift2Px(scrEl.parY) + 1,
              xjXPx + shift2Px(scrEl.parX + plen) + 1, xjYPx + shift2Px(scrEl.parY) + 1);
          ctx.color = nx;
          g.setColor(ctx.color);
          break;
        case POS:
          ctx.xPt10 = scrEl.parX;
          ctx.yPt10 = scrEl.parY;
          break;
        case POSX:
          ctx.xPt10 = scrEl.parX;
          break;
        case POSY:
          ctx.yPt10 = scrEl.parX;
          break;
        case RBASE:
          ctx.eBasePt10 = scrEl.parX;
          break;
        case RGBCOLOR:
          ctx.color = (0 == zColorOverride) ? scrEl.parX : zColorOverride;
          g.setColor(ctx.color);
          break;
        case RMOVE:
          break;
        case STYLE:
          break;
        case TEXT:
          sText = scrEl.parS0;
          nx = 0;
          if ((0 < sText.length()) && (' ' >= sText.charAt(0))) {
            for (; nx < sText.length() && ' ' >= sText.charAt(nx); ++nx) {}
            sText = sText.substring(nx);
            nx = -nx * (shift2Px(UiFunc.boundWidthNmz(" ", ctx.heightPt10)));
          }
          nx += g.getBoundsLeft(sText);
          g.drawText(
              sText,
              xjXPx - nx + shift2Px(ctx.xPt10),
              xjYPx + 1 + shift2Px((ctx.yPt10 + ctx.eBasePt10)));
          nx = UiFunc.boundWidthNmz(scrEl.parS0, ctx.heightPt10);
          // TODO monospaced ...
          ctx.xPt10 += nx;
          break;
        case TXBOX:
          nx = g.getBoundsRight(scrEl.parS0);
          g.drawText(
              scrEl.parS0,
              // TODO stretch
              xjXPx + shift2Px(ctx.xPt10) + ((shift2Px(scrEl.parX) - nx) >> 1),
              xjYPx + 1 + shift2Px(ctx.yPt10 + ctx.eBasePt10));
          ctx.xPt10 += scrEl.parX;
          break;
        case TXCTR: // ,
          nx = g.getBoundsMiddle(scrEl.parS0);
          g.drawText(
              scrEl.parS0,
              xjXPx + shift2Px(ctx.xPt10) - nx,
              xjYPx + 1 + shift2Px(ctx.yPt10 + ctx.eBasePt10));
          nx = UiFunc.boundWidthNmz(scrEl.parS0, ctx.heightPt10) >> 1;
          ctx.xPt10 += nx;
          break;
        case TXSHLEFT: // ,
          nx = g.getBoundsRight(scrEl.parS0); // g.getBoundsMiddle(scrEl.parS0)<<1;
          g.drawText(
              scrEl.parS0,
              xjXPx + shift2Px(ctx.xPt10) - nx,
              xjYPx + 1 + shift2Px(ctx.yPt10 + ctx.eBasePt10));
          nx = UiFunc.boundWidthNmz(scrEl.parS0, ctx.heightPt10);
          ctx.xPt10 -= nx;
          break;
        case TXLF:
          ctx.xPt10 = jXLine;
          ctx.yPt10 += ctx.eLine;
          break;
        case WEIGHT:
          break;
        default:
          break;
      }
    }
    setContextVals(g, previousVals);
    ctx.recycleMe();
    return true;
  }

  static void shiftPointerCanvas4Page(int xX, int xY) {
    final int bar = UiValTag.UI_BAR_PANE_HEIGHT.i32Fut();
    xY -= shift2Px((Dib2Constants.UI_FRAME_BARS - 1) * bar);
    ClickRepeater.uiCPointerMoving = false;
    ClickRepeater.uiCPointerX0 = shift4Px(xX);
    ClickRepeater.uiCPointerY0 = shift4Px(xY);
    if ((0 > xX) && (0 > xY)) {
      ClickRepeater.qPointerX0 = -1;
      ClickRepeater.qPointerY0 = -1;
      return;
    }
    final int splitX = shift2Px(
    UiValTag.UI_PANE_SPLIT_X.i32Fut());
    final int splitY = shift2Px(
    UiValTag.UI_PANE_SPLIT_Y.i32Fut());

    pushShift4Canvas();

    final int offsX = shift2Px(
        UiValTag.UI_PANE_OFFS_X.i32Fut());
    final int offsY = shift2Px(
        UiValTag.UI_PANE_OFFS_Y.i32Fut());
    final int eSplitX = shift2Px(
    UiValTag.UI_PANE_SPLIT_GAP_X.i32Fut());
    final int eSplitY = shift2Px(
    UiValTag.UI_PANE_SPLIT_GAP_Y.i32Fut());

    xX += (xX >= splitX) ? eSplitX : 0;
    xY += (xY >= splitY) ? eSplitY : 0;
    xX += offsX;
    xY += offsY;

    xX = shift4Px(xX);
    xY = shift4Px(xY);

    popShift4Canvas();

    if ((0 <= xX) && (0 <= xY)) {
      ClickRepeater.qUiPointerTick = DateFunc.currentTimeNanobisLinearized(true);
    }
    ClickRepeater.qPointerX0 = xX;
    ClickRepeater.qPointerY0 = xY;
  }

  private void fillDataImage(int index, int heightC, GraphicsIf xGr) {

    final int heightF = qHeightCanvasPx;
    final int widthF = qWidthCanvasPx;

    // Right border line not visible (+1), bottom yes, but status bar pushed (+1):
    xGr.setCanvasImage(
        index,
        widthF + 1,
        heightC + 1,
        ColorNmz.ColorDistinct.BG__WHITE.argbQ(0),
        ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
    QScript scr = ((FeederRf) (Dib2Root.app.feederCurrent)).get().getLastFeed();
    if //((Dib2Root.app.appState.ordinal() > Dib2Lang.AppState.DISCLAIMER.ordinal()) &&
    (ColorNmz.ColorDistinct.PURE_RED.nmz.rgb0 != UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard]) {
      paintScript(xGr, 0, 0, 0L, null, 1, UiPres.qUiKeypad);
    }

    if ((null != scr)
        && (ColorNmz.ColorDistinct.SEA.nmz.rgb0 != UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard])) {
      final int splitX = shift2Px(UiValTag.UI_PANE_SPLIT_X.i32Fut());
      final int splitY = shift2Px(UiValTag.UI_PANE_SPLIT_Y.i32Fut());
      final int zoom = UiValTag.UI_ZOOMLVL_BOARD.i32Fut() - (zbShift2PxMinor ? 1 : 0);

      pushShift4Canvas();

      final int hBoard = shift2Px(Dib2Constants.UI_PAGE_HEIGHT);
      final int wBoard = shift2Px(Dib2Constants.UI_PAGE_WIDTH);
      final int offsX = shift2Px(UiValTag.UI_PANE_OFFS_X.i32Fut());
      final int offsY = shift2Px(UiValTag.UI_PANE_OFFS_Y.i32Fut());
      final int eSplitX = shift2Px(UiValTag.UI_PANE_SPLIT_GAP_X.i32Fut());
      final int eSplitY = shift2Px(UiValTag.UI_PANE_SPLIT_GAP_Y.i32Fut());
      final boolean doX = (10 < splitX) && ((splitX + 10) < widthF) && (10 < eSplitX);
      final boolean doY = (10 < splitY) && ((splitY + 10) < heightC) && (10 < eSplitY);
      if (!doX && !doY) {
        paintScript(xGr, -offsX, -offsY, 0x2L, null, 1, scr);
      } else {
        xGr.setClip(0, 0, doX ? splitX : widthF, doY ? splitY : heightC);
        paintScript(xGr, -offsX, -offsY, 0x2L, null, 1, scr);
        if (doX) {
          xGr.setClip(splitX, 0, widthF, (doY ? splitY : heightC));
          paintScript(xGr, -offsX - eSplitX, -offsY, 0x2L, null, 1, scr);
        }
        if (doY) {
          xGr.setClip(0, splitY, (doX ? splitX : widthF), heightC);
          paintScript(xGr, -offsX, -offsY - eSplitY, 0x2L, null, 1, scr);
        }
        if (doX && doY) {
          xGr.setClip(splitX, splitY, widthF, heightC);
          paintScript(xGr, -offsX - eSplitX, -offsY - eSplitY, 0x2L, null, 1, scr);
        }
        xGr.setClip(0, 0, widthF, heightC);
        xGr.setColorTool(ColorNmz.ColorDistinct.ULTRAMARINE.argbQ(1));
        if (doX) {
          xGr.drawLine(splitX - 1, 0, splitX - 1, heightC);
        }
        if (doY) {
          xGr.drawLine(0, splitY - 1, widthF, splitY - 1);
        }
        xGr.setColorTool(ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
      }

      popShift4Canvas();

      int scrollBarX =
          ((widthF * widthF / wBoard)
                  >> ((0 < zoom) ? (zoom >> 1) : 0)
                  << ((0 > zoom) ? ((-zoom) >> 1) : 0))
              - (zoom << (zoom & 1));
      scrollBarX = (10 > scrollBarX) ? 10 : ((scrollBarX > widthF) ? widthF : scrollBarX);
      int scrollBarY =
          ((heightC * heightC / hBoard) // ,
                  >> ((0 < zoom) ? (zoom >> 1) : 0)
                  << ((0 > zoom) ? ((-zoom) >> 1) : 0))
              - (zoom << (zoom & 1));
      scrollBarY = (10 > scrollBarY) ? 10 : ((scrollBarY > heightC) ? heightC : scrollBarY);
      int jX = shift2Px(UiValTag.UI_PANE_OFFS_X.i32Fut()) * widthF / wBoard;
      int jY = shift2Px(UiValTag.UI_PANE_OFFS_Y.i32Fut()) * heightC / hBoard;
      jX = (jX > (widthF - scrollBarX)) ? (widthF - scrollBarX) : jX;
      jY = (jY > (heightF - scrollBarY)) ? (heightF - scrollBarY) : jY;
      xGr.setColorTool(ColorNmz.ColorDistinct.BG__WHITE.argbQ(0));
      xGr.drawLine(0, 0, widthF, 0);
      xGr.drawLine(0, 0, 0, heightC);
      xGr.drawLine(widthF - 1, 0, widthF - 1, heightC);
      xGr.drawLine(0, heightC - 1, widthF, heightC - 1);
      xGr.setColorTool(ColorNmz.ColorDistinct.ULTRAMARINE.argbQ(1));
      xGr.drawLine(jX, 0, jX + scrollBarX, 0);
      xGr.drawLine(jX, 1, jX + scrollBarX, 1);
      xGr.drawLine(jX, 2, jX + scrollBarX, 2);
      xGr.drawLine(0, jY, 0, jY + scrollBarY);
      xGr.drawLine(1, jY, 1, jY + scrollBarY);
      xGr.drawLine(2, jY, 2, jY + scrollBarY);
      if (doX) {
        jX =
            (shift2Px(UiValTag.UI_PANE_OFFS_X.i32Fut())
                    + shift2Px(UiValTag.UI_PANE_SPLIT_GAP_X.i32Fut()))
                * widthF
                / wBoard;
        jX = (jX > (widthF - scrollBarX)) ? (widthF - scrollBarX) : jX;
      }
      if (doY) {
        jY =
            (shift2Px(UiValTag.UI_PANE_OFFS_Y.i32Fut())
                    + shift2Px(UiValTag.UI_PANE_SPLIT_GAP_Y.i32Fut()))
                * heightC
                / hBoard;
        jY = (jY > (heightC - scrollBarY)) ? (heightC - scrollBarY) : jY;
      }
      xGr.drawLine(widthF - 1, jY, widthF - 1, jY + scrollBarY);
      xGr.drawLine(widthF - 2, jY, widthF - 2, jY + scrollBarY);
      xGr.drawLine(widthF - 3, jY, widthF - 3, jY + scrollBarY);
      xGr.drawLine(jX, heightC - 1, jX + scrollBarX, heightC - 1);
      xGr.drawLine(jX, heightC - 2, jX + scrollBarX, heightC - 2);
      xGr.drawLine(jX, heightC - 3, jX + scrollBarX, heightC - 3);
      xGr.setColorTool(ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
    }

    if (ColorNmz.ColorDistinct.APPLE_GREEN.nmz.rgb0 == UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard]) {
      if (Dib2Root.app.feederCurrent == Dib2Root.app.mainFeeder) {
        zColorOverride = ColorNmz.ColorDistinct.BG__WHITE.argbQ(1);
        paintScript(xGr, -1, -1, 0L, null, 1, UiPres.qUiKeypad);
        paintScript(xGr, 1, 0, 0L, null, 1, UiPres.qUiKeypad);
        paintScript(xGr, 0, 1, 0L, null, 1, UiPres.qUiKeypad);
        zColorOverride = 0;
        paintScript(xGr, 0, 0, 0L, null, 1, UiPres.qUiKeypad);
      }
    }
  }

  private void paintBar(int index, int barHeightPx, QScript script, int backColor, GraphicsIf xGr) {
    if (0 != (paint4Frame_bRefreshNeeded & (1 << index))) {
      // Right border line not visible (+1), bottom yes (+0):
      xGr.setCanvasImage(
          index,
          qWidthCanvasPx + 1,
          barHeightPx,
          backColor,
          ColorNmz.ColorDistinct.FG__BLACK.argbQ(0));
      paintScript(xGr, 0, 0, 0x1L, null, -1, script);
    }
  }

  /** Currently, full refresh is fast enough. */
  static long paint4Frame_bRefreshNeeded = -1;

  public boolean paint4Frame(GraphicsIf xGr) {
    if (Dib2Root.app.appState.ordinal() >= AppState.EXIT_REQUEST.ordinal()) {
      if (Dib2Root.app.appState.ordinal() == AppState.EXIT_REQUEST.ordinal()) {
        Dib2Root.triggerExitProcess();
      }
      return false;
      // paint4Frame_bRefreshNeeded = 0;
    }
    if (MainThreads.isIdle() || (0 != paint4Frame_bRefreshNeeded)) {
      final long tick = DateFunc.currentTimeNanobisLinearized(true);
      UiValTag.tick(tick);
      UiPres.prepareUiFrameData();
    }

    final int heightF = qHeightCanvasPx;
    final int barHeightPx = shift2Px(UiValTag.UI_BAR_PANE_HEIGHT.i32Fut());
    final int heightB = heightF - Dib2Constants.UI_FRAME_BARS * barHeightPx;
    if (0 != (paint4Frame_bRefreshNeeded & (1 << 0))) {
      //ClickRepeater.resetPointedElements();
      fillDataImage(0, heightB, xGr);
    }
    final int backColor = ColorNmz.findDistinct(UiValTag.UI_BAR_BACKGROUND_COLOR.i32Fut()).argbQ(0);
    paintBar(1, barHeightPx, UiPres.qUiBarTitle, backColor, xGr);
    paintBar(2, barHeightPx, UiPres.qUiBarTools, backColor, xGr);
    paintBar(3, barHeightPx, UiPres.qUiBarEntry, ColorNmz.ColorDistinct.BG__WHITE.argbQ(0), xGr);
    paintBar(4, barHeightPx, UiPres.qUiBarStatus, backColor, xGr);
    xGr.drawImage(0, 0, (Dib2Constants.UI_FRAME_BARS - 1) * barHeightPx);
    xGr.drawImage(1, 0, 0);
    xGr.drawImage(2, 0, barHeightPx);
    xGr.drawImage(3, 0, 2 * barHeightPx);
    // Bottom border line not needed => + 1.
    xGr.drawImage(4, 0, (heightF - barHeightPx) + 1);

    xGr.setColorTool(ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
    xGr.show();
    return (Dib2Root.app.appState.ordinal() < AppState.EXIT_REQUEST.ordinal());
  }

  // =====
}
