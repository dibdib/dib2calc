// Copyright (C) 2021, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.config;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

// =====

public class Dib2State implements Dib2Constants {

  // =====

  public static final class Threaded {

    ///// Main

    /** Specific service thread if needed. */
    // public volatile Object activeService = null;

    /** 'View model' ... */
    public /*FeederRf*/ Object mainFeeder = null;

    public /*FeederRf*/ Object feederCurrent = null;
    public volatile /*FeederRf*/ Object feederNext = null;

    ///// Basic

    public final AtomicLong qTick = new AtomicLong(0);

    public long qTickMin = 0;

    public volatile Dib2Lang.AppState appState = Dib2Lang.AppState.CREATE;
    public volatile /*ExceptionAdapter*/ Object error = null;

    ///// Saved, handled by active window

    /** 'Dib2Calc', 'Dib2Qm', ... */
    public String appName = "Dib2";
    /** To match mainFeeder: 'calc', ... */
    public String appShort = "x";

    public String mainClassName = null;
    public boolean bPermitted = false;
    public boolean bAllowDummyPass = false;

    public boolean bStorage = true;
    public boolean bWakeUp = false;
    public boolean bAutostart = false;
    public boolean bInternet = false;
    public boolean bVibrate = true;
    public boolean bNotificationSound = true;
    public boolean bNotificationToast = false;
    public boolean bAutoRefresh = false;

    ///// Saved, handled by Frame

    public boolean bServiceThreadsHalted = false;

    public long alarmTime_msec = 0;
    public int jSound0_msec = 10 * 1000;
    public int jSound1_msec = 1 * 60 * 1000;
    public int soundLength_msec = 1200;

    ///// Secondary

    public String dbFileName = null;
    public String dbFileOptionalPath = null;
    public int autosaveInterval_msec = 10 * 60 * 1000;
    //    public volatile long currentSyncTime = 0;
    public volatile String msg4Notification = null;
    public int serviceInterval_msec = 0; // 7000; // or 12*60*1000 or ...
    //      public long bSound_InitS0S1 = 0;
    public int minLargeSeq = 42;
    public int minTimeVerySlowProcess_msec = 5000;

    /////

    /** Locale: null for multilingual texts with CLDR (UCA DUCET) sorting. */
    public Locale locale = null;
  }

  public static final class Ui {
    public boolean bExitConfirmation = false;
    public boolean bTerminalMode = false;
    public int iLang = 0; // --> Dib2Local.kLanguages
    public volatile String progress = null;
    //    public int cSlides = 1;
    //    public int iSlide = 1;
    //    public int iSlideSupplement = 0;
  }

  // =====
}
