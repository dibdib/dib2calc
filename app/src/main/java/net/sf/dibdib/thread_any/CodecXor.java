// Copyright (C) 2016, 2018  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import java.util.Arrays;
import net.sf.dibdib.config.Dib2Constants;

/** Simple XOR codec for debugging */
public class CodecXor extends CodecFunc {
  // =====

  public static final CodecXor instance = new CodecXor();

  @Override
  public byte[] encode(
      byte[] compressedData,
      int from,
      int to,
      byte[] key,
      byte[] iv16,
      int keyInfo,
      byte[] keyData,
      byte[] signatureKey)
      throws Exception {
    byte[] salt =
        new byte
            [16]; // TcvCodec.createHeaderSaltIv16OLD( Dib2Constants.MAGIC_BYTES, new byte[] { 1, 0,
    // 0 }, this);
    for (int i0 = key.length - 1; i0 >= 4; --i0) {
      key[i0] ^= salt[i0 % 16];
    }
    byte[] out = Arrays.copyOf(salt, to - from + salt.length);
    for (int i0 = to - from - 1; i0 >= 0; --i0) {
      out[salt.length + i0] = (byte) (key[i0 % 32] ^ compressedData[from + i0]);
    }
    return MiscFunc.packet4880X(Dib2Constants.RFC4880_EXP2, null, out, 0, out.length);
  }

  @Override
  public byte[] decode(byte[] data, int offset, int len, byte[] keyOrPass, byte[] signatureKey)
      throws Exception {
    int hdlen = MiscFunc.getPacketHeaderLen(data, offset);
    if (data[hdlen + 3] != getMethodTag()) {
      return null;
    }
    byte[] key = Arrays.copyOf(keyOrPass, 32);
    for (int i0 = key.length - 1; i0 >= 4; --i0) {
      key[i0] ^= data[hdlen + (i0 % 16)];
    }
    byte[] out = new byte[len - 16];
    for (int i0 = len - 1; i0 >= 16; --i0) {
      out[i0] = (byte) (key[i0 % 32] ^ data[offset + hdlen + i0]);
    }
    return Arrays.copyOfRange(out, offset + hdlen + 16, offset + len);
  }

  @Override
  public byte getMethodTag() {
    return '1'; // dummy - xor
  }

  @Override
  public byte[] getInitialValue(int len) {
    return Arrays.copyOf(
        ("" + (DateFunc.currentTimeMillisLinearized() & 0xfffff) + "0123456789")
            .getBytes(StringFunc.CHAR8),
        len);
  }

  // =====
}
