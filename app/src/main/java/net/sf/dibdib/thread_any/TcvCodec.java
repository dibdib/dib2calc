// Copyright (C) 2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import java.io.File;
import java.util.Arrays;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.TsvCodecIf;

public class TcvCodec {
  // =====

  public static final TcvCodec instance = new TcvCodec();

  // =====
  public static class HeaderInfo {
    // =====

    public int version;
    public char encoding;
    public char algo;
    public byte[] address;
    public String caau;
    public byte[] keyDataPublic; // public keys or hashes or salt

    public HeaderInfo(byte[] keySalt) {
      version = Dib2Constants.FILE_STRUC_VERSION;
      encoding = 'C';
      algo = 'A';
      address = new byte[0];
      final int len = keySalt.length & 0x1f;
      keyDataPublic = Arrays.copyOf(keySalt, len + 2);
      keyDataPublic[keyDataPublic.length - 2] = (byte) (SerFunc.TAG_BYTES_L0 + len);
      keyDataPublic[keyDataPublic.length - 1] = SerFunc.TAG_nil;
    }

    public HeaderInfo(
        int version, char encoding, char algo, byte[] address, String caau, byte[] keyDataPublic) {
      this.version = version;
      this.encoding = encoding;
      this.algo = algo;
      this.address = address;
      this.caau = caau;
      this.keyDataPublic = keyDataPublic;
    }
  }

  // =====

  /** In case zAccessCodeHex == "" && zPassPhraseHex == "" */
  // private static final byte[] kAccessCodePassDummy = "42dibdib.sourceforge.net".getBytes(
  // StringFunc.STR16X );
  private String zAccessCodeHex = null;

  private String zPassPhraseHex = null;
  private String zAdditionalCodes = null;
  private byte[] zAPKey = null;
  private byte[] zAPSalt = null;
  private boolean zAPUsed4Reading = false;

  public byte[] create(char platform, Object... parameters) {
    for (TsvCodecIf codec : Dib2Root.codecs) {
      codec.create(platform, parameters);
    }
    return null;
  }

  public boolean setAccessCode(byte[] accessCode) {
    if ((null != accessCode)
        && (1 == accessCode.length)
        && ('=' == accessCode[0])
        && (null != zPassPhraseHex)
        && (2 < zPassPhraseHex.length())) {
      zAccessCodeHex = zPassPhraseHex;
    }
    zAccessCodeHex =
        (null == accessCode) ? zAccessCodeHex : StringFunc.hex4Bytes(accessCode, false);
    zAccessCodeHex =
        ((null == zAccessCodeHex) || (0 >= zAccessCodeHex.length()) ? null : zAccessCodeHex);
    zAPKey = null;
    return (null != zAccessCodeHex) && (2 < zAccessCodeHex.length());
  }

  public boolean checkAccessCode(byte[] accessCode) {
    zAPKey = null;
    zAPSalt = null;
    zAPUsed4Reading = false;
    if (null == accessCode) {
      File file = new File(Dib2Root.platform.getFilesDir("safe"), Dib2Root.PASS_FILENAME);
      return !(file.isFile());
    }
    if (null == zAccessCodeHex) {
      return false;
    }
    return StringFunc.hex4Bytes(accessCode, false).equals(zAccessCodeHex);
  }

  public void unpackPhrase(byte[] acPhraseEncodedHex) throws Exception { // acPhraseEncoded

    if (((Dib2Constants.MAGIC_BYTES[0] == acPhraseEncodedHex[0])
            && (Dib2Constants.MAGIC_BYTES[1] == acPhraseEncodedHex[1]))
        || ('F' < (acPhraseEncodedHex[0] & 0xff))) {
      ///// Old format.
      zAdditionalCodes = null;
      return;
    } else {
      byte[] acPhraseEncoded = StringFunc.bytes4Hex(acPhraseEncodedHex);
      acPhraseEncoded =
          Dib2Root.codecs[0].decodePhrase(acPhraseEncoded, StringFunc.bytes4Hex(zAccessCodeHex));
      zPassPhraseHex = StringFunc.hex4Bytes(acPhraseEncoded, false);
      zAdditionalCodes = null;
    }
  }

  private byte[] setHexPhrase_dat = null;

  /**
   * Read or set hex phrase for en-/ decoding.
   *
   * @param xmPhrase Hex string of pass phrase, "" for decoding from protected dir, "=" for using
   *     access code.
   * @return true iff proper phrase available.
   */
  public boolean settleHexPhrase(String xmPhrase) {
    String old = zPassPhraseHex;
    if // ((null != zPassPhraseHex) &&
    (null != xmPhrase) {
      zPassPhraseHex =
          (0 >= xmPhrase.length())
              ? null
              : ((1 >= xmPhrase.length())
                  ? zAccessCodeHex
                  : xmPhrase); // ((2 >= xmPhrase.length()) ? zPassPhraseHex : xmPhrase));
    }
    //  zLastSave = (1000 > zLastSave) ? DateFunc.currentTimeMillisLinearized() : zLastSave;
    if ((null == xmPhrase)
        && (null == zPassPhraseHex)
        && (null != zAccessCodeHex)
        && (2 < zAccessCodeHex.length())) {
      File dir = Dib2Root.platform.getFilesDir("safe");
      if (null == dir) {
        return false;
      }
      File file = new File(dir, Dib2Root.PASS_FILENAME);
      try {
        boolean erase = false;
        if (file.isFile()) {
          if (null != setHexPhrase_dat) {
            // Limit number of retries.
            file.delete();
          } else {
            setHexPhrase_dat = MiscFunc.readFile(file.getAbsolutePath(), 0);
            if (20 > setHexPhrase_dat.length) {
              // Not intended, but maybe somebody wants to use a very good AC, then
              // the pass phrase should not be encoded by itself.
              zPassPhraseHex = zAccessCodeHex;
            } else {
              // Limit number of retries.
              file.delete();
            }
          }
        } else {
          erase = true;
        }
        if (null != setHexPhrase_dat) {
          byte[] dat = setHexPhrase_dat;
          if (erase) {
            setHexPhrase_dat = null;
          }
          unpackPhrase(dat);
          file = new File(dir, Dib2Root.PASS_FILENAME_X);
          if (file.exists()) {
            try {
              dat = readPacked(file.getAbsolutePath(), null, null, null);
              char[] utf16 = StringFunc.hex4Bytes(dat, false).toCharArray();
              zAdditionalCodes = StringFunc.string4HexUtf16(utf16, 0, utf16.length);
            } catch (Exception e) {
              // Not usable any more ...
              file.delete();
            }
            if (0 >= zAdditionalCodes.length()) {
              zAdditionalCodes = null;
            }
          }
        }
      } catch (Exception e) {
        if (file.isFile()) {
          // Limit number of retries.
          file.delete();
        }
      }
    }
    if ((null == zPassPhraseHex) || (2 >= zPassPhraseHex.length())) {
      return false;
    }
    if (!zPassPhraseHex.equals(old) && (null != old) && (2 < old.length())) {
      zAPKey = null;
      File file = new File(Dib2Root.platform.getFilesDir("safe"), Dib2Root.PASS_FILENAME);
      if (file.isFile()) {
        file.delete();
      }
    }
    return true;
  }

  /** Depending on the context, it might be okay to use a dummy phrase ... */
  public boolean setDummyPhrase(boolean force) {
    if (!force) {
      File file = new File(Dib2Root.platform.getFilesDir("safe"), Dib2Root.PASS_FILENAME);
      if (file.isFile()) {
        return false;
      }
    }
    zAccessCodeHex = "30";
    zPassPhraseHex = "42";
    zAPKey = null;
    return true;
  }

  /**
   * Calculate number of salt iterations.
   *
   * @param saltIterMag -1..(SALT_ITERATION_MAG+x): 0 for default value.
   * @return Matching number or 1174 as old value.
   */
  public int getSaltIterations4Mag(int saltIterMag) {
    final int stdd = (1 << Dib2Constants.SALT_ITERATION_MAG) + 3;
    int saltIterations =
        (0 > saltIterMag)
            ? 1174 // old value 1174
            : ((0 == saltIterMag) ? stdd : ((1 << saltIterMag) + 3));
    // Prevent deadlock:
    saltIterations =
        ((Dib2Constants.SALT_ITERATION_MAG + 3) >= saltIterMag) ? saltIterations : stdd;
    return saltIterations;
  }

  public boolean writePhrase() {
    if ((zAccessCodeHex == null)
        || (zPassPhraseHex == null)
        || (2 >= zAccessCodeHex.length()) && (2 >= zPassPhraseHex.length())) {
      return false;
    }
    File dir = Dib2Root.platform.getFilesDir("safe");
    if (null == dir) {
      return false;
    }
    try {
      File file = new File(dir, Dib2Constants.PASS_FILENAME);
      byte[] dat =
          Dib2Root.codecs[0].encodePhrase(
              StringFunc.bytes4Hex(zPassPhraseHex), StringFunc.bytes4Hex(zAccessCodeHex));
      // Not intended:
      if (zAccessCodeHex.equals(zPassPhraseHex)) {
        // But if so, then we should not save the phrase encoded by itself.
        dat = new byte[] {'0', '0'};
      } else {
        dat = StringFunc.hexAscii4Bytes(dat);
      }
      MiscFunc.writeFile(file.getAbsolutePath(), dat, 0, dat.length, null);
      if (null != zAdditionalCodes) {
        dat = StringFunc.bytes4Hex(StringFunc.hexUtf16(zAdditionalCodes));
        file = new File(dir, Dib2Constants.PASS_FILENAME_X);
        if (0 >= writePacked(dat, 0, dat.length, file.getAbsolutePath())) {
          return false;
        }
      }
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  public String getPassPhraseHex() {
    if (zAccessCodeHex == null || zPassPhraseHex == null) {
      return null;
    }
    return zPassPhraseHex;
  }

  public byte[] getPassFull() {
    if (zAccessCodeHex == null || zPassPhraseHex == null) {
      return null;
    }
    if (zAccessCodeHex == "" && zPassPhraseHex == "") {
      setDummyPhrase(true);
    }
    return StringFunc.bytes4Hex(zAccessCodeHex + zPassPhraseHex);
  }

  public void setAdditionalCodes(String add) {
    zAdditionalCodes = add;
  }

  public String getAdditionalCodes() {
    return zAdditionalCodes;
  }

  public byte[] getKey(byte[] xSalt16, int saltIterMag) {
    xSalt16 = (null == xSalt16) ? zAPSalt : xSalt16;
    if (null == xSalt16) {
      xSalt16 = Dib2Root.codecs[0].getInitialValue(16);
    }
    final int stdd = ((1 << Dib2Constants.SALT_ITERATION_MAG) + 3);
    int saltIterations = getSaltIterations4Mag(saltIterMag);
    if ((stdd == saltIterations) && (null != zAPKey)) {
      if (Arrays.equals(xSalt16, zAPSalt)) {
        return zAPKey;
      }
    }
    byte[] out =
        Dib2Root.codecs[0].getKey(
            StringFunc.bytes4Hex(zPassPhraseHex),
            StringFunc.bytes4Hex(zAccessCodeHex),
            xSalt16,
            saltIterations);
    if (stdd == saltIterations) {
      zAPKey = out;
      zAPSalt = xSalt16;
    }
    return out;
  }

  public long unpackAddrPk(JResult out, byte[] dat, TsvCodecIf decoder) {
    String addr = "";
    long offsPk0 = -1;
    byte[] newKey = new byte[0];
    int offsPkTag = -1;
    int offs = 0;
    int len = dat.length;
    try {
      if (dat[0] == (byte) Dib2Root.RFC4880_EXP2) {
        offs = MiscFunc.getPacketHeaderLen(dat, 0);
        len = offs + MiscFunc.getPacketBodyLen(dat, 1);
        if ((len > dat.length) || (16 >= len)) {
          Dib2Root.log("unpack", "unexpected length " + len);
          return -1;
        }
      }
      if ((dat[offs] != Dib2Root.MAGIC_BYTES[0]) || (dat[offs + 1] != Dib2Root.MAGIC_BYTES[1])) {
        Dib2Root.log("unpack", "unexpected format " + offs);
        return -1;
      }
      if (decoder.getMethodTag() != dat[offs + 3]) { // 'A'
        Dib2Root.log("unpack", "unexpected format " + offs);
        return -1;
      }
      if ('0' > dat[offs + 2]) {
        // Old format.
        ExceptionAdapter.throwAdapted(new IllegalStateException(), getClass(), "Old file format.");
        return 0;
      } else if ('C' != dat[offs + 2]) {
        Dib2Root.log("unpack", "unexpected format " + offs);
        return -1;
      }
      if ((7 != (0xf & dat[len - 1]))
          || (dat[offs + 7] != (byte) (SerFunc.TAG_BYTES_L0 + 7))
          || (Dib2Constants.FILE_STRUC_VERSION < dat[offs + 4])) {
        Dib2Root.log("unpack", "unexpected format L " + len);
      }
      // Skip SIV, ENC, IV.
      long offsLen = SerFunc.getTcvOffsetLength(dat, 2, len - 2, 3);
      offsPkTag = -1 + (int) offsLen;
      // Go to public key value/hash/salt:
      offsLen = SerFunc.getTcvOffsetLength(dat, 2, offsPkTag, 2);
      offsPk0 = offsLen;
      newKey = Arrays.copyOfRange(dat, (int) offsLen, (int) (offsLen >>> 32) + (int) offsLen);
      if ((4 >= (offsLen >>> 32)) || (2 > (int) offsLen)) {
        newKey = new byte[0];
      }
      offsLen = SerFunc.getTcvOffsetLength(dat, 2, -1 + (int) offsLen, 1);
      if (2 < (int) offsLen) {
        addr =
            StringFunc.string4Utf8(
                Arrays.copyOfRange(dat, (int) offsLen, (int) (offsLen >>> 32) + (int) offsLen));
      }
      if ((offs + 8) != (int) offsLen) {
        Dib2Root.log("unpack", "unexpected format E " + offsLen);
        return -1;
      }
    } catch (Exception e) {
      return -9;
    }
    if (null != out) {
      out.object0 = addr;
      out.object1 = newKey;
      out.long0 = offsPkTag;
    }
    return offsPk0;
  }

  public byte[] unpack(byte[] xData, byte[] yHeader, String optionalPhrase, String optionalAc)
      throws Exception {
    byte[] dat = xData;
    if ((null == dat) || (1 >= dat.length) || (0 <= dat[0])) {
      if (null != yHeader) {
        yHeader[0] = 1;
      }
      return null;
    }
    TsvCodecIf decoder = null;
    zAPUsed4Reading = true;
    int offs = 0;
    if (dat[offs] == (byte) Dib2Constants.RFC4880_EXP2) {
      offs += MiscFunc.getPacketHeaderLen(dat, offs);
    }
    if (dat[offs] != Dib2Constants.MAGIC_BYTES[0]) {
      return null;
    }
    for (TsvCodecIf c0 : Dib2Root.codecs) {
      int hdlen = 16;
      //      int hdlen = MiscFunc.getPacketHeaderLen( dat, 0 ) + Dib2Constants.MAGIC_BYTES.length +
      // 2;
      if (dat[offs + 3] == c0.getMethodTag()) {
        if (null != yHeader) {
          hdlen = (hdlen > yHeader.length) ? yHeader.length : hdlen;
          System.arraycopy(dat, offs, yHeader, 0, hdlen);
        }
        decoder = c0;
        break;
      }
    }
    if (null == decoder) {
      if (null != yHeader) {
        yHeader[0] = 2;
      }
      return null;
    }
    long offsPk0 = unpackAddrPk(null, dat, decoder);
    byte[] key;
    if (0 >= offsPk0) {
      if (0 > offsPk0) {
        return null;
      }
      // Old format ...
      return null;
    } else {
      if (16 != (offsPk0 >>> 32)) {
        Dib2Root.log("read", "unexpected format OL " + offsPk0);
      }
      byte[] salt = Arrays.copyOfRange(dat, (int) offsPk0, 16 + (int) offsPk0);
      key =
          (null == optionalAc)
              ? getKey(salt, dat[6 + offs])
              : decoder.getKey(
                  optionalPhrase.getBytes(StringFunc.CHAR16UTF8),
                  optionalAc.getBytes(StringFunc.CHAR16UTF8),
                  salt,
                  getSaltIterations4Mag(dat[6 + offs] & 0xff));
    }
    dat = decoder.decode(dat, offs, dat.length, key, null);
    if ((dat[0] != Dib2Constants.MAGIC_BYTES[0]) || (dat[1] != Dib2Constants.MAGIC_BYTES[1])) {
      return null;
    }
    dat = decoder.decompress(dat, dat.length);
    if (null != dat) {
      return dat;
    }
    return null;
  }

  /**
   * Read encoded file (cmp. RFC 4880).
   *
   * @param filePath
   * @param xyOptHeader
   * @param optionalPhrase
   * @param optionalAc
   * @return
   */
  public byte[] readPacked(
      String filePath, byte[] xyOptHeader, String optionalPhrase, String optionalAc) {
    //  QResult addrPk = QResult.get8Pool();
    byte[] magicBytes = new byte[2];
    if (null != xyOptHeader) {
      magicBytes[0] = (0 != xyOptHeader[0]) ? xyOptHeader[0] : Dib2Constants.MAGIC_BYTES[0];
      magicBytes[1] = (0 != xyOptHeader[0]) ? xyOptHeader[1] : Dib2Constants.MAGIC_BYTES[1];
    }
    byte[] dat = null;
    //  if (null != xyOptHeader) {
    //    xyOptHeader[ 0 ] = 3; //0;
    //  }
    byte[] fdat;
    try {
      fdat = MiscFunc.readFile(filePath, Dib2Constants.FILE_STRUC_VERSION_CMPAT);
    } catch (Exception e) {
      Dib2Root.log("codec", "Error: " + e + e.getMessage() + "/ " + filePath);
      return null;
    }
    for (short iVariant = 0; iVariant < 3; ++iVariant) {
      try {
        if (0 < iVariant) {
          // Workaround for the sake of portability ...
          Dib2Root.codecs[0].create((char) 0, "VAR", new Short(iVariant));
        }
        dat = unpack(fdat, xyOptHeader, optionalPhrase, optionalAc);
        if ((null != dat)
            && ((0 == magicBytes[0]) || ((dat[0] == magicBytes[0]) && (dat[1] == magicBytes[1])))) {
          if (0 != iVariant) {
            zAPKey = null;
          }
          return dat;
        }
        zAPKey = null;
      } catch (Exception e) {
        Dib2Root.log("codec", "Error: " + e + e.getMessage() + "/ " + filePath);
      }
    }
    if (null != xyOptHeader) {
      xyOptHeader[0] = 0;
    }
    return null;
  }

  public byte[] pack(
      byte[] dat, int from, int to, byte[] key, byte[] iv16, HeaderInfo hi, byte[] sigKey) {
    try {
      byte[] compr = Dib2Root.codecs[0].compress(dat, from, to);
      final int coFrom = compr[0] + MiscFunc.getPacketHeaderLen(compr, compr[0]);
      final int coTo = coFrom + MiscFunc.getPacketBodyLen(compr, 1 + compr[0]);
      //    if ((null == sigKey) && (Dib2Constants.FILE_STRUC_VERSION_CMPAT > hi.version)) {
      //      byte[] enc = Dib2Root.csvCodecs[ 0 ].encode( compr, coFrom, coTo, key, iv16, 0, null,
      // //hi.keyDataPublic,
      //        null );
      //      return enc;
      //    }
      String headerWithKeyDataHex =
          StringFunc.hex4Bytes(hi.address, false); // prefs.getHex( "email_address", null );
      int oldCaauInfo = 0;
      byte[] header;
      if (Dib2Constants.FILE_STRUC_VERSION_CMPAT <= hi.version) {
        if (250 < headerWithKeyDataHex.length()) {
          Dib2Root.log("codec pack", "error addr len " + headerWithKeyDataHex.length());
          return null;
        }
        byte[] keyP = hi.keyDataPublic;
        if (null == keyP) {
          keyP = new byte[] {SerFunc.TAG_nil, SerFunc.TAG_nil};
        } else if (256 <= keyP.length) {
          return null;
        }
        int offs = 8;
        header =
            new byte
                [1
                    + offs
                    + hi.address.length
                    + 2
                    + keyP.length]; // + ((2 >= keyP.length) ? 1 : 2) ];
        header[0] = Dib2Constants.MAGIC_BYTES[0];
        header[1] = Dib2Constants.MAGIC_BYTES[1];
        header[2] = (byte) hi.encoding; // 'C'; // CTR
        header[3] = (byte) hi.algo; // 'A'; // AES
        header[4] = Dib2Constants.FILE_STRUC_VERSION_CMPAT;
        header[5] = Dib2Constants.FILE_STRUC_VERSION;
        header[6] = Dib2Constants.SALT_ITERATION_MAG; // (0 == old value 1174)
        header[7] = (byte) (SerFunc.TAG_BYTES_L0 + 7);
        System.arraycopy(hi.address, 0, header, offs, hi.address.length);
        header[offs = offs + hi.address.length] = (byte) hi.address.length;
        header[++offs] = SerFunc.TAG_BYTES_L1;
        System.arraycopy(keyP, 0, header, ++offs, keyP.length);
        offs += keyP.length;
        header[offs++] = (byte) (SerFunc.TAG_ARR_C0 + 4);
      } else {
        if (160 < headerWithKeyDataHex.length()) {
          Dib2Root.log("codec pack", "error addr len " + headerWithKeyDataHex.length());
          return null;
        }
        if ('0' != hi.caau.charAt(1)) {
          oldCaauInfo = (hi.caau.charAt(1) & 0x3) << 4;
          headerWithKeyDataHex += " " + StringFunc.hex4Bytes(hi.keyDataPublic, false);
        }
        headerWithKeyDataHex +=
            "                ".substring(0, 16).substring(15 - headerWithKeyDataHex.length() % 16);
        headerWithKeyDataHex += ":";
        oldCaauInfo |= ((hi.caau.charAt(2) & 0x3) << 2) | (hi.caau.charAt(3) & 0x3);
        // caauInfo == keyInfo (!= 0 !!!).
        oldCaauInfo |= 0x40;
        header = headerWithKeyDataHex.getBytes(StringFunc.CHAR8);
      }
      return Dib2Root.codecs[0].encode(compr, coFrom, coTo, key, iv16, oldCaauInfo, header, sigKey);
    } catch (Exception e) {
    }
    return null;
  }

  public byte[] pack(byte[] dat, String phrase, String accessCode) {
    byte[] salt = Dib2Root.codecs[0].getInitialValue(16);
    byte[] key =
        Dib2Root.codecs[0].getKey(
            phrase.getBytes(StringFunc.CHAR16UTF8),
            accessCode.getBytes(StringFunc.CHAR16UTF8),
            salt,
            getSaltIterations4Mag(0));
    TcvCodec.HeaderInfo hi = new TcvCodec.HeaderInfo(salt);
    return TcvCodec.instance.pack(dat, 0, dat.length, key, null, hi, null);
  }

  public int writePacked(byte[] txt, int from, int to, String path, int... optVersion) {
    if (null == getPassFull()) {
      return -9;
    }
    if (zAPUsed4Reading || (null == zAPKey)) {
      zAPUsed4Reading = false;
      zAPKey = getKey(null, 0);
      if (null == zAPSalt) {
        return -99;
      }
    }
    int rc = -1;
    try {
      rc = -2;
      TcvCodec.HeaderInfo hi = new TcvCodec.HeaderInfo(zAPSalt);
      if ((null != optVersion) && (0 < optVersion.length)) {
        hi.version = optVersion[0];
      }
      byte[] iv = Dib2Root.codecs[0].getInitialValue(16);
      rc = -3;
      byte[] enc = pack(txt, from, to, zAPKey, iv, hi, null);
      if (null == enc) {
        return -4;
      }
      rc = -5;
      MiscFunc.writeFile(path, enc, 0, enc.length, null);
      rc = enc.length;
    } catch (Exception e) {
      Dib2Root.log("codec", "error " + e + e.getMessage());
      return rc;
    }
    return rc;
  }

  // =====
}
