// Copyright (C) 2016, 2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.QVal;

public final class QValPool {

  /** TODO shash as val ... */
  public static QMMap qvals;

  public static /*o*/ QVal asQValR(long handle) {
    if (0 == (handle & 1L)) {
      // TODO
    }
    return (QVal) qvals.search(handle);
  }

  public static /*o*/ QVal asQValR(/*o*/ QVal val) {
    return val;
  }

  //  private /*o*/ QVal(long handle) {
  //    this.handle = handle;
  //  }

  public static QVal asQVal(long handle) {
    return asQValR(handle);
  }

  public static QVal asQVal(/*o*/ QVal val) {
    return val;
  }

  public static long asHandle(long handle) {
    return handle;
  }

  public static long asHandle(/*o*/ QVal val) {
    return val.shash;
  }

  // Transitional:
  public static String string4QVal(QWord word) {
    return word.toStringFull();
  }

  public static String string4QVal(QVal val) {
    return asQValR(val).toString();
  }

  public static QVal qval4AtomicLiteral(String str) {
    final QWord out = QWord.createQWord(str, true);
    if (0 != (out.shash & 1L)) {
      final long h0 = qvals.add4Handle(out);
      out.shash = h0;
    }
    return asQVal(out);
  }

  public static QVal qval4AtomicValue(String str) {
    final QWord out = QWord.createQWord(str, false);
    if (0 != (out.shash & 1L)) {
      final long h0 = qvals.add4Handle(out);
      out.shash = h0;
    }
    return asQVal(out);
  }

  public static QSeq qval4String(String string) {
    return (null == string) ? null : QSeq.createQSeq(string);
  }
}
