// Copyright (C) 2021, 2022  Roland Horsch and others:
// Original data by Adrian Mariano, as provided by GNU:
// http://ftp.gnu.org/gnu/units/units-2.21.tar.gz
// For the presented form: Copyright (C) 2021, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.picked.common;

public enum Units {
  // !locale en_US
  // !  set UNITS_ENGLISH US
  // !endlocale
  // !locale en_GB
  // !  set UNITS_ENGLISH GB
  // !endlocale
  // !set UNITS_ENGLISH US   # Default setting for English units
  // !set UNITS_SYSTEM default   # Set a default value
  // !varnot UNITS_SYSTEM si emu esu gaussian gauss hlu natural natural-gauss hartree planck
  // planck-red default
  // !message Unknown unit system given with -u or UNITS_SYSTEM environment variable
  // !message Valid systems: si, emu, esu, gauss[ian], hlu, natural, natural-gauss
  // !message                planck, planck-red, hartree
  // !message Using SI
  // !prompt (SI)
  // !endvar
  // !var UNITS_SYSTEM si
  // !message SI units selected
  // !prompt (SI)
  // !endvar
  // ? s         !      # The second, symbol s, is the SI unit of time.  It is defined
  // ? second    s      # by taking the fixed numerical value of the unperturbed
  c_SI(299792458, ""),
  c(299792458, "m/s"), // speed of light in vacuum (exact
  // ? m         !      # The metre, symbol m, is the SI unit of length.  It is
  // ? meter     m      # defined by taking the fixed numerical value of the speed
  // ? metre     m      # of light in vacuum, c, to be 299 792 458 when expressed in
  h_SI(6.62607015e-34, ""),
  h(6.62607015e-34, "J s"), // Planck constant (exact
  // ? kg        !      # The kilogram, symbol kg, is the SI unit of mass.  It is
  // ? kilogram  kg     # defined by taking the fixed numerical value of the Planck
  k_SI(1.380649e-23, ""),
  boltzmann(1.380649e-23, "J/K"), // Boltzmann constant (exact
  // ? k         boltzmann
  // ? K         !      # The kelvin, symbol K, is the SI unit of thermodynamic
  // ? kelvin    K      # temperature.  It is defined by taking the fixed numerical
  e_SI(1.602176634e-19, ""),
  e(1.602176634e-19, "C"), // electron charge (exact
  // ? A         !      # The ampere, symbol A, is the SI unit of electric current.
  // ? ampere    A      # It is defined by taking the fixed numerical value of the
  // ? amp       ampere # elementary charge, e, to be 1.602 176 634 * 10^-19 when
  avogadro(6.02214076e23, "/ mol"), // Size of a mole (exact
  // ^ N_A       avogadro
  // ? mol       !      # The mole, symbol mol, is the SI unit of amount of
  // ? mole      mol    # substance.  One mole contains exactly 6.022 140 76 * 10^23
  // ? cd        !      # The candela, symbol cd, is the SI unit of luminous intensity
  // ? candela   cd     # in a given direction.  It is defined by taking the fixed
  // ? radian    !dimensionless   # The angle subtended at the center of a circle by
  // ? sr        !dimensionless   # Solid angle which cuts off an area of the surface
  // ? steradian sr               #   of the sphere equal to that of a square with
  // ## A primitive non-SI unit
  // ? bit       !      # Basic unit of information (entropy).  The entropy in bits
  // -yotta-                  1e24     # Greek or Latin octo, "eight"
  // -zetta-                  1e21     # Latin septem, "seven"
  // -exa-                    1e18     # Greek hex, "six"
  // -peta-                   1e15     # Greek pente, "five"
  // -tera-                   1e12     # Greek teras, "monster"
  // -giga-                   1e9      # Greek gigas, "giant"
  // -mega-                   1e6      # Greek megas, "large"
  // -myria-                  1e4      # Not an official SI prefix
  // -kilo-                   1e3      # Greek chilioi, "thousand"
  // -hecto-                  1e2      # Greek hekaton, "hundred"
  // -deca-                   1e1      # Greek deka, "ten"
  // -deka-                   deca
  // -deci-                   1e-1     # Latin decimus, "tenth"
  // -centi-                  1e-2     # Latin centum, "hundred"
  // -milli-                  1e-3     # Latin mille, "thousand"
  // -micro-                  1e-6     # Latin micro or Greek mikros, "small"
  // -nano-                   1e-9     # Latin nanus or Greek nanos, "dwarf"
  // -pico-                   1e-12    # Spanish pico, "a bit"
  // -femto-                  1e-15    # Danish-Norwegian femten, "fifteen"
  // -atto-                   1e-18    # Danish-Norwegian atten, "eighteen"
  // -zepto-                  1e-21    # Latin septem, "seven"
  // -yocto-                  1e-24    # Greek or Latin octo, "eight"
  // -quarter-                1|4
  // -semi-                   0.5
  // -demi-                   0.5
  // -hemi-                   0.5
  // -half-                   0.5
  // -double-                 2
  // -triple-                 3
  // -treble-                 3
  // -kibi-                   2^10     # In response to the convention of illegally
  // -mebi-                   2^20     # and confusingly using metric prefixes for
  // -gibi-                   2^30     # powers of two, the International
  // -tebi-                   2^40     # Electrotechnical Commission aproved these
  // -pebi-                   2^50     # binary prefixes for use in 1998.  If you
  // -exbi-                   2^60     # want to refer to "megabytes" using the
  // -Ki-                     kibi     # binary definition, use these prefixes.
  // -Mi-                     mebi
  // -Gi-                     gibi
  // -Ti-                     tebi
  // -Pi-                     pebi
  // -Ei-                     exbi
  // -Y-                      yotta
  // -Z-                      zetta
  // -E-                      exa
  // -P-                      peta
  // -T-                      tera
  // -G-                      giga
  // -M-                      mega
  // -k-                      kilo
  // -h-                      hecto
  // -da-                     deka
  // -d-                      deci
  // -c-                      centi
  // -m-                      milli
  // -u-                      micro   # it should be a mu but u is easy to type
  // -n-                      nano
  // -p-                      pico
  // -f-                      femto
  // -a-                      atto
  // -z-                      zepto
  // -y-                      yocto
  // ## Names of some numbers
  one(1, ""),
  two(2, ""),
  // . double(2, ""),
  couple(2, ""),
  three(3, ""),
  triple(3, ""),
  four(4, ""),
  quadruple(4, ""),
  five(5, ""),
  quintuple(5, ""),
  six(6, ""),
  seven(7, ""),
  eight(8, ""),
  nine(9, ""),
  ten(10, ""),
  eleven(11, ""),
  twelve(12, ""),
  thirteen(13, ""),
  fourteen(14, ""),
  fifteen(15, ""),
  sixteen(16, ""),
  seventeen(17, ""),
  eighteen(18, ""),
  nineteen(19, ""),
  twenty(20, ""),
  thirty(30, ""),
  forty(40, ""),
  fifty(50, ""),
  sixty(60, ""),
  seventy(70, ""),
  eighty(80, ""),
  ninety(90, ""),
  hundred(100, ""),
  thousand(1000, ""),
  million(1e6, ""),
  // ? twoscore                two score
  // ? threescore              three score
  // ? fourscore               four score
  // ? fivescore               five score
  // ? sixscore                six score
  // ? sevenscore              seven score
  // ? eightscore              eight score
  // ? ninescore               nine score
  // ? tenscore                ten score
  // ? twelvescore             twelve score
  shortbillion(1e9, ""),
  shorttrillion(1e12, ""),
  shortquadrillion(1e15, ""),
  shortquintillion(1e18, ""),
  shortsextillion(1e21, ""),
  shortseptillion(1e24, ""),
  shortoctillion(1e27, ""),
  shortnonillion(1e30, ""),
  // ? shortnoventillion          shortnonillion
  shortdecillion(1e33, ""),
  shortundecillion(1e36, ""),
  shortduodecillion(1e39, ""),
  shorttredecillion(1e42, ""),
  shortquattuordecillion(1e45, ""),
  shortquindecillion(1e48, ""),
  shortsexdecillion(1e51, ""),
  shortseptendecillion(1e54, ""),
  shortoctodecillion(1e57, ""),
  shortnovemdecillion(1e60, ""),
  shortvigintillion(1e63, ""),
  centillion(1e303, ""),
  googol(1e100, ""),
  // ? longbillion               million^2
  // ? longtrillion              million^3
  // ? longquadrillion           million^4
  // ? longquintillion           million^5
  // ? longsextillion            million^6
  // ? longseptillion            million^7
  // ? longoctillion             million^8
  // ? longnonillion             million^9
  // ? longnoventillion          longnonillion
  // ? longdecillion             million^10
  // ? longundecillion           million^11
  // ? longduodecillion          million^12
  // ? longtredecillion          million^13
  // ? longquattuordecillion     million^14
  // ? longquindecillion         million^15
  // ? longsexdecillion          million^16
  // ? longseptdecillion         million^17
  // ? longoctodecillion         million^18
  // ? longnovemdecillion        million^19
  // ? longvigintillion          million^20
  // ## These numbers fill the gaps left by the long system above.
  milliard(1000, "million"),
  billiard(1000, "million^2"),
  trilliard(1000, "million^3"),
  quadrilliard(1000, "million^4"),
  quintilliard(1000, "million^5"),
  sextilliard(1000, "million^6"),
  septilliard(1000, "million^7"),
  octilliard(1000, "million^8"),
  nonilliard(1000, "million^9"),
  // ? noventilliard           nonilliard
  decilliard(1000, "million^10"),
  // ## For consistency
  // ? longmilliard              milliard
  // ? longbilliard              billiard
  // ? longtrilliard             trilliard
  // ? longquadrilliard          quadrilliard
  // ? longquintilliard          quintilliard
  // ? longsextilliard           sextilliard
  // ? longseptilliard           septilliard
  // ? longoctilliard            octilliard
  // ? longnonilliard            nonilliard
  // ? longnoventilliard         noventilliard
  // ? longdecilliard            decilliard
  // ## The short system prevails in English speaking countries
  // ? billion                 shortbillion
  // ? trillion                shorttrillion
  // ? quadrillion             shortquadrillion
  // ? quintillion             shortquintillion
  // ? sextillion              shortsextillion
  // ? septillion              shortseptillion
  // ? octillion               shortoctillion
  // ? nonillion               shortnonillion
  // ? noventillion            shortnoventillion
  // ? decillion               shortdecillion
  // ? undecillion             shortundecillion
  // ? duodecillion            shortduodecillion
  // ? tredecillion            shorttredecillion
  // ? quattuordecillion       shortquattuordecillion
  // ? quindecillion           shortquindecillion
  // ? sexdecillion            shortsexdecillion
  // ? septendecillion         shortseptendecillion
  // ? octodecillion           shortoctodecillion
  // ? novemdecillion          shortnovemdecillion
  // ? vigintillion            shortvigintillion
  // ## Numbers used in India
  lakh(1e5, ""),
  crore(1e7, ""),
  arab(1e9, ""),
  kharab(1e11, ""),
  neel(1e13, ""),
  padm(1e15, ""),
  shankh(1e17, ""),
  // ## Named SI derived units (officially accepted)
  // ? newton                  kg m / s^2   # force
  // ? N                       newton
  // ? pascal                  N/m^2        # pressure or stress
  // ? Pa                      pascal
  // ? joule                   N m          # energy
  // ? J                       joule
  // ? watt                    J/s          # power
  // ? W                       watt
  // ? coulomb                 A s          # charge
  // ? C                       coulomb
  // ? volt                    W/A          # potential difference
  // ? V                       volt
  // ? ohm                     V/A          # electrical resistance
  // ? siemens                 A/V          # electrical conductance
  // ? S                       siemens
  // ? farad                   C/V          # capacitance
  // ? F                       farad
  // ? weber                   V s          # magnetic flux
  // ? Wb                      weber
  // ? henry                   V s / A      # inductance
  // ? H                       henry
  // ? tesla                   Wb/m^2       # magnetic flux density
  // ? T                       tesla
  // ? hertz                   /s           # frequency
  // ? Hz                      hertz
  // ^ LENGTH                  meter
  // ^ AREA                    LENGTH^2
  // ^ VOLUME                  LENGTH^3
  // ^ MASS                    kilogram
  // ^ AMOUNT                  mole
  // ^ ANGLE                   radian
  // ^ SOLID_ANGLE             steradian
  // ^ MONEY                   US$
  // ^ FORCE                   newton
  // ^ PRESSURE                FORCE / AREA
  // ^ STRESS                  FORCE / AREA
  // ^ FREQUENCY               hertz
  // ^ VELOCITY                LENGTH / TIME
  // ^ ACCELERATION            VELOCITY / TIME
  // ^ DENSITY                 MASS / VOLUME
  // ^ LINEAR_DENSITY          MASS / LENGTH
  // ^ VISCOSITY               FORCE TIME / AREA
  // ^ KINEMATIC_VISCOSITY     VISCOSITY / DENSITY
  // ^ CURRENT                 ampere
  // ^ CHARGE                  coulomb
  // ^ CAPACITANCE             farad
  // ^ RESISTANCE              ohm
  // ^ CONDUCTANCE             siemens
  // ^ RESISTIVITY             RESISTANCE AREA / LENGTH
  // ^ CONDUCTIVITY            CONDUCTANCE LENGTH / AREA
  // ^ INDUCTANCE              henry
  // ^ E_FIELD                 ELECTRIC_POTENTIAL / LENGTH
  // ^ B_FIELD                 tesla
  // ^ D_FIELD                 E_FIELD epsilon0 / epsilon0_SI  #   mu0_SI c^2 F / m
  // ^ H_FIELD                 B_FIELD / (mu0/mu0_SI)
  // ^ ELECTRIC_DIPOLE_MOMENT  C m
  // ^ MAGNETIC_DIPOLE_MOMENT  J / T
  // ^ POLARIZATION            ELECTRIC_DIPOLE_MOMENT / VOLUME
  // ^ MAGNETIZATION           MAGNETIC_DIPOLE_MOMENT / VOLUME
  // ^ ELECTRIC_POTENTIAL      ENERGY / CHARGE #volt
  // ^ VOLTAGE                 ELECTRIC_POTENTIAL
  // ^ E_FLUX                  E_FIELD AREA
  // ^ D_FLUX                  D_FIELD AREA
  // ^ B_FLUX                  B_FIELD AREA
  // ^ H_FLUX                  H_FIELD AREA
  // ## units derived easily from SI units
  // ? gram                    millikg
  // ? gm                      gram
  // ? g                       gram
  tonne(1000, "kg"),
  // ? t                       tonne
  // ? metricton               tonne
  // ? sthene                  tonne m / s^2
  // ? funal                   sthene
  // ? pieze                   sthene / m^2
  quintal(100, "kg"),
  bar(1e5, "Pa"), // About 1 at
  // ? b                       bar
  // ? vac                     millibar
  // ? micron                  micrometer # One millionth of a meter
  // ? bicron                  picometer  # One brbillionth of a meter
  // ? cc                      cm^3
  are(100, "m^2"),
  // ? a                       are
  liter(1000, "cc"), // The liter was defined in 1901 as th
  oldliter(1.000028, "dm^3"), // space occupied by 1 kg of pure water a
  // ? L                       liter         # the temperature of its maximum density
  // ? l                       liter         # under a pressure of 1 atm.  This was
  // ? mho                     siemens    # Inverse of ohm, hence ohm spelled backward
  // ? galvat                  ampere     # Named after Luigi Galvani
  angstrom(1e-10, "m"), // Convenient for describing molecular size
  // ? xunit                   xunit_cu      # Used for measuring x-ray wavelengths.
  // ? siegbahn                xunit         # Originally defined to be 1|3029.45 of
  xunit_cu(1.00207697e-13, "m"), // the spacing of calcite planes at 1
  xunit_mo(1.00209952e-13, "m"), // degC. It was intended to be exactl
  angstromstar(1.00001495, "angstrom"), // Defined by JA Bearden in 1965 to replac
  silicon_d220(1.920155716e-10, "m"), // Silicon lattice spacin
  // ? siliconlattice sqrt(8) silicon_d220# Silicon lattice parameter, (a), the side
  fermi(1e-15, "m"), // Convenient for describing nuclear size
  barn(1e-28, "m^2"), // Used to measure cross section fo
  shed(1e-24, "barn"), // Defined to be a smaller companion to th
  // ? brewster                micron^2/N # measures stress-optical coef
  // ? diopter                 /m         # measures reciprocal of lens focal length
  fresnel(1e12, "Hz"), // occasionally used in spectroscop
  shake(1e-8, "sec"),
  svedberg(1e-13, "s"), // Used for measuring the sedimentatio
  // ? gamma                   microgram  # Also used for 1e-9 tesla
  // ? lambda                  microliter
  spat(1e12, "m"), // Rarely used for astronomical measurement
  preece(1e13, "ohm m"), // resistivit
  // ? planck                  J s        # action of one joule over one second
  // ? sturgeon                /henry     # magnetic reluctance
  // ? daraf                   1/farad    # elastance (farad spelled backwards)
  leo(10, "m/s^2"),
  // ? poiseuille              N s / m^2  # viscosity
  // ? mayer                   J/g K      # specific heat
  // ? mired                   / microK   # reciprocal color temperature.  The name
  // ? crocodile               megavolt   # used informally in UK physics labs
  metricounce(25, "g"),
  // ? mounce                  metricounce
  finsenunit(1e5, "W/m^2"), // Measures intensity of ultraviolet ligh
  fluxunit(1e-26, "W/m^2 Hz"), // Used in radio astronomy to measur
  // ? jansky                  fluxunit   # K. G. Jansky identified radio waves coming
  // ? Jy                      jansky     # from outer space in 1931.
  // ? flick       W / cm^2 sr micrometer # Spectral radiance or irradiance
  // ? pfu                    / cm^2 sr s # particle flux unit -- Used to measure
  // ? pyron            cal_IT / cm^2 min # Measures heat flow from solar radiation,
  // ? katal                   mol/sec    # Measure of the amount of a catalyst.  One
  // ? kat                     katal      #   katal of catalyst enables the reaction
  solarluminosity(382.8e24, "W"), // A common yardstick for comparing th
  // ? solarirradiance		solarluminosity / (4 pi sundist^2)
  // ? solarconstant		solarirradiance
  // ^ TSI			solarirradiance		# total solar irradiance
  // ## time
  // ? sec                     s
  minute(60, "s"),
  // ? min                     minute
  hour(60, "min"),
  // ? hr                      hour
  day(24, "hr"),
  // ? d                       day
  // ? da                      day
  week(7, "day"),
  // ? wk                      week
  sennight(7, "day"),
  fortnight(14, "day"),
  blink(1e-5, "day"), // Actual human blink takes 1|3 secon
  ce(1e-2, "day"),
  cron(1e6, "years"),
  watch(4, "hours"), // time a sentry stands watch or a ship'
  // : bell                    1|8 watch  # Bell would be sounded every 30 minutes.
  // : decimalhour             1|10 day
  // : decimalminute           1|100 decimalhour
  // : decimalsecond           1|100 decimalminute
  // ? beat                    decimalminute          # Swatch Internet Time
  // ## angular measure
  circle(2, "pi radian"),
  // : degree                  1|360 circle
  // ? deg                     degree
  // ? arcdeg                  degree
  // : arcmin                  1|60 degree
  // ? arcminute               arcmin
  // ? '                       arcmin
  // : arcsec                  1|60 arcmin
  // ? arcsecond               arcsec
  // ? "                       arcsec
  // ? ''                      "
  rightangle(90, "degrees"),
  // : quadrant                1|4 circle
  // : quintant                1|5 circle
  // : sextant                 1|6 circle
  // : sign                    1|12 circle # Angular extent of one sign of the zodiac
  // ? turn                    circle
  // ? revolution              turn
  // ? rev                     turn
  // ? pulsatance              radian / sec
  // : gon                     1|100 rightangle  # measure of grade
  // ? grade                   gon
  // : centesimalminute        1|100 grade
  // : centesimalsecond        1|100 centesimalminute
  // : milangle                1|6400 circle     # Official NIST definition.
  // : pointangle              1|32 circle  # Used for reporting compass readings
  centrad(0.01, "radian"), // Used for angular deviation of ligh
  // ? mas                     milli arcsec # Used by astronomers
  // ? seclongitude            circle (seconds/day) # Astronomers measure longitude
  // ## Some geometric formulas
  // () circlearea(r)   units=[m;m^2] range=[0,) pi r^2 ; sqrt(circlearea/pi)
  // () spherevolume(r) units=[m;m^3] range=[0,) 4|3 pi r^3 ;  <> cuberoot(spherevolume/4|3 pi)
  // () spherevol()     spherevolume
  // () square(x)       range=[0,)          x^2 ; sqrt(square)
  // ## Solid angle measure
  sphere(4, "pi sr"),
  // : squaredegree            1|180^2 pi^2 sr
  // : squareminute            1|60^2 squaredegree
  // : squaresecond            1|60^2 squareminute
  // ? squarearcmin            squareminute
  // ? squarearcsec            squaresecond
  sphericalrightangle(0.5, "pi sr"),
  octant(0.5, "pi sr"),
  // ## Concentration measures
  percent(0.01, ""),
  // ? %                       percent
  mill(0.001, ""), // Originally established by Congress in 179
  // : proof                   1|200     # Alcohol content measured by volume at
  ppm(1e-6, ""),
  // ? partspermillion         ppm
  ppb(1e-9, ""),
  // ? partsperbillion         ppb       # USA billion
  ppt(1e-12, ""),
  // ? partspertrillion        ppt       # USA trillion
  // : karat                   1|24      # measure of gold purity
  // ? caratgold               karat
  // ? gammil                  mg/l
  basispoint(0.01, "%"), // Used in financ
  // : fine                    1|1000    # Measure of gold purity
  // () pH(x) units=[1;mol/liter] range=(0,) 10^(-x) mol/liter ; (-log(pH liters/mol))
  // ^ TEMPERATURE             kelvin
  // ^ TEMPERATURE_DIFFERENCE  kelvin
  // () tempC(x) units=[1;K] domain=[-273.15,) range=[0,)  <> x K + stdtemp ; (tempC +(-stdtemp))/K
  // () tempcelsius() tempC
  // ? degcelsius              K
  // ? degC                    K
  // () tempF(x) units=[1;K] domain=[-459.67,) range=[0,)  <> (x+(-32)) degF + stdtemp ;
  // (tempF+(-stdtemp))/degF + 32
  // () tempfahrenheit() tempF
  // : degfahrenheit           5|9 degC
  // : degF                    5|9 degC
  // ? degreesrankine          degF              # The Rankine scale has the
  // ? degrankine              degreesrankine    # Fahrenheit degree, but its zero
  // ? degreerankine           degF              # is at absolute zero.
  // ? degR                    degrankine
  // ? tempR                   degrankine
  // ? temprankine             degrankine
  // () tempreaumur(x)    units=[1;K] domain=[-218.52,) range=[0,)  <> x degreaumur+stdtemp ;
  // (tempreaumur+(-stdtemp))/degreaumur
  // : degreaumur              10|8 degC # The Reaumur scale was used in Europe and
  // ? degK                    K         # "Degrees Kelvin" is forbidden usage.
  // ? tempK                   K         # For consistency
  // [] gasmark[degR]  <> .0625    634.67  <> .125     659.67  <> .25      684.67  <> .5
  // 709.67  <> 1        734.67  <> 2        759.67  <> 3        784.67  <> 4        809.67  <> 5
  //     834.67  <> 6        859.67  <> 7        884.67  <> 8        909.67  <> 9        934.67  <>
  // 10       959.67
  // () beaufort_WMO1100(B) units=[1;m/s] domain=[0,17] range=[0,)  <> 0.836 B^3|2 m/s;
  // (beaufort_WMO1100 s / 0.836 m)^2|3
  // () beaufort(B) units=[1;m/s] domain=[0,17] range=[0,)  <> beaufort_WMO1100(B);
  // ~beaufort_WMO1100(beaufort)
  // ## Physical constants
  // ## Basic constants
  pi(3.14159265358979323846, ""),
  // ? light                   c
  mu0_SI(2, "alpha h_SI / e_SI^2 c_SI"), // Vacuum magnetic permeabilit
  mu0(2, "alpha h / e^2 c"), // Gets overridden in CGS mode
  // ? epsilon0_SI             1/mu0_SI c_SI^2  # Vacuum electric permittivity
  // ? epsilon0                1/mu0 c^2        #   Also overridden in CGS modes
  // ? Z0                      mu0 c            # Free space impedance
  // ? energy                  c^2              # Convert mass to energy
  // ? hbar                    h / 2 pi
  // ? hbar_SI                 h_SI / 2 pi
  // ? spin                    hbar
  // ^ G_SI            6.67430e-11
  G(6.67430e-11, "N m^2 / kg^2"), // Newtonian gravitational constan
  // ? coulombconst            1/4 pi epsilon0  # Listed as k or k_C sometimes
  // ? k_C                     coulombconst
  // ## Physico-chemical constants
  atomicmassunit_SI(1.66053906660e-27, ""), // Unified atomic mass unit, defined a
  atomicmassunit(1.66053906660e-27, "kg"), // Unified atomic mass unit, defined a
  // ? u                       atomicmassunit   #   1|12 of the mass of carbon 12.
  // ? amu                     atomicmassunit   #   The relationship N_A u = 1 g/mol
  // ? dalton                  u                #   is approximately, but not exactly
  // ? Da                      dalton           #   true (with the 2019 SI).
  amu_chem(1.66026e-27, "kg"), // 1|16 of the weighted average mass o
  amu_phys(1.65981e-27, "kg"), // 1|16 of the mass of a neutra
  // ? gasconstant             k N_A            # Molar gas constant (exact)
  // ? R                       gasconstant
  // ? kboltzmann              boltzmann
  // ? molarvolume         mol R stdtemp / atm  # Volume occupied by one mole of an
  // ? loschmidt     avogadro mol / molarvolume # Molecules per cubic meter of an
  // ? molarvolume_si  N_A siliconlattice^3 / 8 # Volume of a mole of crystalline
  // ? stefanboltzmann pi^2 k^4 / 60 hbar^3 c^2 # The power per area radiated by a
  // ? sigma                   stefanboltzmann  #   blackbody at temperature T is
  // ? wiendisplacement     (h c/k)/4.9651142317442763  # Wien's Displacement Law gives
  K_J90(483597.9, "GHz/V"), // Direct measurement of the volt is difficult. Unti
  // ^ K_J   2e/h              #   recently, laboratories kept Weston cadmium cells as
  R_K90(25812.807, "ohm"), // Measurement of the ohm also presents difficulties
  // ^ R_K   h/e^2             #   The old approach involved maintaining resistances
  // ? ampere90 (K_J90 R_K90 / K_J R_K) A
  // ? coulomb90 (K_J90 R_K90 / K_J R_K) C
  // ? farad90 (R_K90/R_K) F
  // ? henry90 (R_K/R_K90) H
  // ? ohm90 (R_K/R_K90) ohm
  // ? volt90 (K_J90/K_J) V
  // ? watt90 (K_J90^2 R_K90 / K_J^2 R_K) W
  // ## Various conventional values
  gravity(9.80665, "m/s^2"), // std acceleration of gravity (exact
  // ? force                   gravity          # use to turn masses into forces
  atm(101325, "Pa"), // Standard atmospheric pressur
  // ? atmosphere              atm
  Hg(13.5951, "gram force / cm^3"), // Standard weight of mercury (exact
  // ? water                   gram force/cm^3  # Standard weight of water (exact)
  // ? waterdensity            gram / cm^3      # Density of water
  // ? H2O                     water
  // ? wc                      water            # water column
  mach(331.46, "m/s"), // speed of sound in dry air at ST
  standardtemp(273.15, "K"), // standard temperatur
  // ? stdtemp                 standardtemp
  // ? normaltemp              tempF(70)        # for gas density, from NIST
  // ? normtemp                normaltemp       # Handbook 44
  Hg10C(13.5708, "force gram / cm^3"), // These units, when used to for
  Hg20C(13.5462, "force gram / cm^3"), // pressure measures, are not accurat
  Hg23C(13.5386, "force gram / cm^3"), // because of considerations of th
  Hg30C(13.5217, "force gram / cm^3"), // revised practical temperature scale
  Hg40C(13.4973, "force gram / cm^3"),
  Hg60F(13.5574, "force gram / cm^3"),
  H2O0C(0.99987, "force gram / cm^3"),
  H2O5C(0.99999, "force gram / cm^3"),
  H2O10C(0.99973, "force gram / cm^3"),
  H2O15C(0.99913, "force gram / cm^3"),
  H2O18C(0.99862, "force gram / cm^3"),
  H2O20C(0.99823, "force gram / cm^3"),
  H2O25C(0.99707, "force gram / cm^3"),
  H2O50C(0.98807, "force gram / cm^3"),
  H2O100C(0.95838, "force gram / cm^3"),
  // ## Atomic constants
  // ? Rinfinity            m_e c alpha^2 / 2 h # The wavelengths of a spectral series
  // ^ R_H                     10967760 /m      #   can be expressed as
  alpha(7.2973525693e-3, ""), // The fine structure constant wa
  // ? bohrradius              alpha / 4 pi Rinfinity
  prout(185.5, "keV"), // nuclear binding energy equal to 1|1
  conductancequantum(2, "e^2 / h"),
  // ## Particle radius
  // ? electronradius    coulombconst e^2 / electronmass c^2   # Classical
  deuteronchargeradius(2.12799e-15, "m"),
  protonchargeradius(0.8751e-15, "m"),
  // ## Masses of elementary particles
  // ? electronmass_SI         electronmass_u atomicmassunit_SI
  electronmass_u(5.48579909065e-4, ""),
  electronmass(5.48579909065e-4, "u"),
  // ? m_e                     electronmass
  muonmass(0.1134289259, "u"),
  // ? m_mu                    muonmass
  taumass(1.90754, "u"),
  // ? m_tau                   taumass
  protonmass(1.007276466621, "u"),
  // ? m_p                     protonmass
  neutronmass(1.00866491595, "u"),
  // ? m_n                     neutronmass
  deuteronmass(2.013553212745, "u"), // Nucleus of deuterium, on
  // ? m_d                     deuteronmass        #   proton and one neutron
  alphaparticlemass(4.001506179127, "u"), // Nucleus of He, two proton
  // ? m_alpha                 alphaparticlemass   #   and two neutrons
  tritonmass(3.01550071621, "u"), // Nucleius of H3, one proto
  // ? m_t                     tritonmass          #   and two neutrons
  helionmass(3.014932247175, "u"), // Nucleus of He3, two proton
  // ? m_h                     helionmass          #   and one neutron
  // ? electronwavelength      h / m_e c
  // ? lambda_C                electronwavelength
  // ? protonwavelength        h / m_p c
  // ? lambda_C,p              protonwavelength
  // ? neutronwavelength       h / m_n c
  // ? lambda_C,n              neutronwavelength
  // ? muonwavelength          h / m_mu c
  // ? lambda_C,mu             muonwavelength
  g_d(0.8574382338, ""), // Deuteron g-facto
  // ? g_e                    -2.00231930436256   # Electron g-factor
  // ? g_h                    -4.255250615        # Helion g-factor
  // ? g_mu                   -2.0023318418       # Muon g-factor
  // ? g_n                    -3.82608545         # Neutron g-factor
  g_p(5.5856946893, ""), // Proton g-facto
  g_t(5.957924931, ""), // Triton g-facto
  fermicoupling(1.1663787e-5, "/ GeV^2"),
  // ? bohrmagneton            e hbar / 2 electronmass  # Reference magnetic moment for
  // ? mu_B                    bohrmagneton             #   the electron
  // ? nuclearmagneton         e hbar /  2 protonmass   # Convenient reference magnetic
  // ? mu_N                    nuclearmagneton          #   moment for heavy particles
  // ? mu_e                    g_e mu_B / 2             # Electron spin magnet moment
  // ? mu_mu                   g_mu e hbar / 4 muonmass # Muon spin magnetic moment
  // ? mu_p                    g_p mu_N / 2             # Proton magnetic moment
  // ? mu_n                    g_n mu_N / 2             # Neutron magnetic moment
  // ? mu_t                    g_t mu_N / 2             # Triton magnetic moment
  // ? mu_d                    g_d mu_N            # Deuteron magnetic moment, spin 1
  // ? mu_h                    g_h mu_N / 2             # Helion magnetic moment
  // ## Units derived from physical constants
  // ? kgf                     kg force
  // ? technicalatmosphere     kgf / cm^2
  // ? at                      technicalatmosphere
  // ? hyl                     kgf s^2 / m   # Also gram-force s^2/m according to [15]
  // ? mmHg                    mm Hg
  // ? torr                    atm / 760  # The torr, named after Evangelista
  // ? tor                     Pa         # Suggested in 1913 but seldom used [24].
  // ? inHg                    inch Hg
  // ? inH2O                   inch water
  // ? mmH2O                   mm water
  // ? eV                      e V      # Energy acquired by a particle with charge e
  // ? electronvolt            eV       #   when it is accelerated through 1 V
  // ? lightyear               c julianyear # The 365.25 day year is specified in
  // ? ly                      lightyear    # NIST publication 811
  // ? lightsecond             c s
  // ? lightminute             c min
  // ? parsec                  au / tan(arcsec)    # Unit of length equal to distance
  // ? pc                      parsec              #   from the sun to a point having
  // ? rydberg                 h c Rinfinity       # Rydberg energy
  crith(0.089885, "gram"), // The crith is the mass of on
  // ? amagatvolume            molarvolume
  // ? amagat                  mol/amagatvolume    # Used to measure gas densities
  // ? lorentz                 bohrmagneton / h c  # Used to measure the extent
  // ? cminv                   h c / cm            # Unit of energy used in infrared
  // ? invcm                   cminv               #   spectroscopy.
  // ? wavenumber              cminv
  // ? kcal_mol                kcal_th / mol N_A   # kcal/mol is used as a unit of
  // ## CGS system based on centimeter, gram and second
  // ? dyne                    cm gram / s^2   # force
  // ? dyn                     dyne
  // ? erg                     cm dyne         # energy
  // ? poise                   gram / cm s     # viscosity, honors Jean Poiseuille
  // ? P                       poise
  // ? rhe                     /poise          # reciprocal viscosity
  // ? stokes                  cm^2 / s        # kinematic viscosity
  // ? St                      stokes
  // ? stoke                   stokes
  // ? lentor                  stokes          # old name
  // ? Gal                     cm / s^2        # acceleration, used in geophysics
  // ? galileo                 Gal             # for earth's gravitational field
  // ? barye                   dyne/cm^2       # pressure
  // ? barad                   barye           # old name
  // ? kayser                  1/cm            # Proposed as a unit for wavenumber
  // ? balmer                  kayser          # Even less common name than "kayser"
  // ? kine                    cm/s            # velocity
  // ? bole                    g cm / s        # momentum
  // ? pond                    gram force
  // ? glug                gram force s^2 / cm # Mass which is accelerated at
  // ? darcy           centipoise cm^2 / s atm # Measures permeability to fluid flow.
  // ? mobileohm               cm / dyn s      # mobile ohm, measure of mechanical
  // ? mechanicalohm           dyn s / cm      # mechanical resistance
  // ? acousticalohm           dyn s / cm^5    # ratio of the sound pressure of
  // ? ray                     acousticalohm
  // ? rayl                    dyn s / cm^3    # Specific acoustical resistance
  eotvos(1e-9, "Gal/cm"), // Change in gravitational acceleratio
  // !var UNITS_SYSTEM esu emu gaussian gauss hlu
  // ? sqrt_cm                 !
  // ? sqrt_centimeter         sqrt_cm
  // +m                      100 sqrt_cm^2
  // ? sqrt_g                  !
  // ? sqrt_gram               sqrt_g
  // +kg                     kilo sqrt_g^2
  // !endvar
  statcoulomb(10, "coulomb cm / s c"), // Charge such that two charge
  // ? esu                     statcoulomb           # of 1 statC separated by 1 cm
  // ? statcoul                statcoulomb           # exert a force of 1 dyne
  // ? statC                   statcoulomb
  // ? stC                     statcoulomb
  // ? franklin                statcoulomb
  // ? Fr                      franklin
  // !var UNITS_SYSTEM esu
  // !message CGS-ESU units selected
  // !prompt (ESU)
  // +statcoulomb            sqrt(dyne) cm
  // +A                      10 c_SI statamp
  // +mu0                    1/c^2
  // +coulombconst           1
  // !endvar
  // ? statampere              statcoulomb / s
  // ? statamp                 statampere
  // ? statA                   statampere
  // ? stA                     statampere
  // ? statvolt                dyne cm / statamp sec
  // ? statV                   statvolt
  // ? stV                     statvolt
  // ? statfarad               statamp sec / statvolt
  // ? statF                   statfarad
  // ? stF                     statfarad
  // ? cmcapacitance           statfarad
  // ? stathenry               statvolt sec / statamp
  // ? statH                   stathenry
  // ? stH                     stathenry
  // ? statohm                 statvolt / statamp
  // ? stohm                   statohm
  // ? statmho                 /statohm
  // ? stmho                   statmho
  // ? statweber               statvolt sec
  // ? statWb                  statweber
  // ? stWb                    statweber
  // ? stattesla               statWb/cm^2   # Defined by analogy with SI; rarely
  // ? statT                   stattesla     #   if ever used
  // ? stT                     stattesla
  debye(1e-10, "statC angstrom"), // unit of electrical dipole momen
  // ? helmholtz               debye/angstrom^2     # Dipole moment per area
  jar(1000, "statfarad"), // approx capacitance of Leyden ja
  abampere(10, "A"), // Current which produces a force o
  // ? abamp                   abampere        #   2 dyne/cm between two infinitely
  // ? aA                      abampere        #   long wires that are 1 cm apart
  // ? abA                     abampere
  // ? biot                    abampere
  // ? Bi                      biot
  // !var UNITS_SYSTEM emu
  // !message CGS-EMU units selected
  // !prompt (EMU)
  // +abampere               sqrt(dyne)
  // +A                      0.1 abamp
  // +mu0                    1
  // +coulombconst           c^2
  // !endvar
  // ? abcoulomb               abamp sec
  // ? abcoul                  abcoulomb
  // ? abC                     abcoulomb
  // ? abfarad                 abampere sec / abvolt
  // ? abF                     abfarad
  // ? abhenry                 abvolt sec / abamp
  // ? abH                     abhenry
  // ? abvolt                  dyne cm  / abamp sec
  // ? abV                     abvolt
  // ? abohm                   abvolt / abamp
  // ? abmho                   /abohm
  // ? gauss                   abvolt sec / cm^2 # The magnetic field 2 cm from a wire
  // ? Gs                      gauss             # carrying a current of 1 abampere
  // ? maxwell                 gauss cm^2        # Also called the "line"
  // ? Mx                      maxwell
  // ? oersted                 gauss / mu0   # From the relation H = B / mu
  // ? Oe                      oersted
  // ? gilbert                 gauss cm / mu0
  // ? Gb                      gilbert
  // ? Gi                      gilbert
  unitpole(4, "pi maxwell"), // unit magnetic pol
  // ? emu                     erg/gauss  # "electro-magnetic unit", a measure of
  // !var UNITS_SYSTEM gaussian gauss
  // !message CGS-Gaussian units selected
  // !prompt (Gaussian)
  // !endvar
  // !var UNITS_SYSTEM gaussian gauss natural-gauss
  // +statcoulomb            sqrt(dyne) cm
  // +A                      10 c_SI statamp
  // +mu0                    1
  // +epsilon0               1
  // +coulombconst           1                  # The gauss is the B field produced
  // +gauss                  statcoulomb / cm^2 # 1 cm from a wire carrying a current
  // +weber                  1e8 maxwell        # of 0.5*(c/(cm/s)) stA = 1.5e10 stA
  // +bohrmagneton           e hbar / 2 electronmass c
  // +nuclearmagneton        e hbar / 2 protonmass c
  // !endvar
  // ## Electromagnetic CGS (Heaviside-Lorentz)
  // ? hlu_charge    statcoulomb / sqrt(4 pi)
  // ? hlu_current   hlu_charge / sec
  // ? hlu_volt      erg / hlu_charge
  // ? hlu_efield    hlu_volt / cm
  // ? hlu_bfield    sqrt(4 pi) gauss
  // !var UNITS_SYSTEM hlu
  // !message CGS-Heaviside-Lorentz Units selected
  // !prompt (HLU)
  // !endvar
  // !var UNITS_SYSTEM hlu natural planck planck-red
  // +statcoulomb            sqrt(dyne) cm sqrt(4 pi)
  // +A                      10 c_SI statamp
  // +mu0                    1
  // +epsilon0               1
  // +gauss                  (1/2 pi c) (0.5 c/(cm/s)) statamp / cm
  // +weber                  1e8 maxwell
  // +bohrmagneton           e hbar / 2 electronmass c
  // +nuclearmagneton        e hbar / 2 protonmass c
  // !endvar
  // ## These are the Heaviside-Lorentz natural units
  // ? natural_length          hbar c / eV
  // ? natural_mass            eV / c^2
  // ? natural_time            hbar / eV
  // ? natural_temp            eV / boltzmann
  // ? natural_charge          e / sqrt(4 pi alpha)
  // ? natural_current         natural_charge / natural_time
  // ? natural_force           natural_mass natural_length / natural_time^2
  // ? natural_energy          natural_force natural_length
  // ? natural_power           natural_energy / natural_time
  // ? natural_volt            natural_energy / natural_charge
  // ? natural_Efield          natural_volt / natural_length
  // ? natural_Bfield          natural_volt natural_time / natural_length^2
  // !var UNITS_SYSTEM natural
  // !message Natural units selected (Heaviside-Lorentz based)
  // !prompt (natural)
  // +eV                     !
  // +h                      2 pi
  // +c                      1
  // +boltzmann              1
  // +m                      e_SI / hbar_SI c_SI eV
  // +kg                     (c_SI^2 / e_SI) eV
  // +s                      e_SI / hbar_SI eV
  // +K                      (k_SI / e_SI) eV
  // !endvar
  // !var UNITS_SYSTEM natural-gauss
  // !message Natural units selected (Gaussian based)
  // !prompt (natgauss)
  // +eV                     !
  // +h                      2 pi
  // +c                      1
  // +boltzmann              1
  // +m                      e_SI / (h_SI / 2 pi) c_SI eV
  // +kg                     (c_SI^2 / e_SI) eV
  // +s                      e_SI / (h_SI / 2 pi) eV
  // +K                      (k_SI / e_SI) eV
  // !endvar
  // ## Rationalized, unreduced planck units
  // ? planckmass              sqrt(hbar c / G)
  // ? m_P                     planckmass
  // ? plancktime              hbar / planckmass c^2
  // ? t_P                     plancktime
  // ? plancklength            plancktime c
  // ? l_P                     plancklength
  // ? plancktemperature       hbar / k plancktime
  // ^ T_P                     plancktemperature
  // ? planckenergy            planckmass plancklength^2 / plancktime^2
  // ^ E_P                     planckenergy
  // ? planckcharge            sqrt(epsilon0 hbar c)
  // ? planckcurrent           planckcharge / plancktime
  // ? planckvolt              planckenergy / planckcharge
  // ? planckEfield            planckvolt / plancklength
  // ? planckBfield            planckvolt plancktime / plancklength^2
  // ## Rationalized, reduced planck units
  // ? planckmass_red          sqrt(hbar c / 8 pi G)
  // ? plancktime_red          hbar / planckmass_red c^2
  // ? plancklength_red        plancktime_red c
  // ? plancktemperature_red   hbar / k plancktime_red
  // ? planckenergy_red        planckmass_red plancklength_red^2 / plancktime_red^2
  // ? planckcharge_red        sqrt(epsilon0 hbar c)
  // ? planckcurrent_red       planckcharge_red / plancktime_red
  // ? planckvolt_red          planckenergy_red / planckcharge_red
  // ? planckEfield_red        planckvolt_red / plancklength_red
  // ? planckBfield_red        planckvolt_red plancktime_red / plancklength_red^2
  // !var UNITS_SYSTEM planck
  // !message Planck units selected
  // !prompt (planck)
  // +c 1
  // +h 2 pi
  // +G 1
  // +boltzmann 1
  // +kg sqrt(G_SI / hbar_SI c_SI)
  // +s  c_SI^2 / hbar_SI kg
  // +m  s / c_SI
  // +K  k_SI / hbar_SI s
  // !endvar
  // !var UNITS_SYSTEM planck-red
  // !message Reduced planck units selected
  // !prompt (planck reduced)
  // +c 1
  // +h 2 pi
  // +G 1/8 pi
  // +boltzmann 1
  // +kg sqrt(8 pi G_SI / hbar_SI c_SI)
  // +s  c_SI^2 / hbar_SI kg
  // +m  s / c_SI
  // +K  k_SI / hbar_SI s
  // !endvar
  // ## Some historical electromagnetic units
  intampere(0.999835, "A"), // Defined as the current which in on
  // ? intamp                  intampere     #   second deposits .001118 gram of
  intfarad(0.999505, "F"),
  intvolt(1.00033, "V"),
  intohm(1.000495, "ohm"), // Defined as the resistance of
  daniell(1.042, "V"), // Meant to be electromotive force of
  // ? faraday                 N_A e mol     # Charge that must flow to deposit or
  faraday_phys(96521.9, "C"), // liberate one gram equivalent of an
  faraday_chem(96495.7, "C"), // element. (The chemical and physica
  kappline(6000, "maxwell"), // Named by and for Gisbert Kap
  siemensunit(0.9534, "ohm"), // Resistance of a meter long column o
  copperconductivity(58, "siemens m / mm^2"), // A wire a meter long wit
  // ^ IACS                    copperconductivity      #   a 1 mm^2 cross section
  copperdensity(8.89, "g/cm^3"), // The "ounce" measures th
  // ? ouncecopper             oz / ft^2 copperdensity #   thickness of copper used
  // ? ozcu                    ouncecopper             #   in circuitboard fabrication
  // ## Photometric units
  // ^ LUMINOUS_INTENSITY      candela
  // ^ LUMINOUS_FLUX           lumen
  // ^ LUMINOUS_ENERGY         talbot
  // ^ ILLUMINANCE             lux
  // ^ EXITANCE                lux
  candle(1.02, "candela"), // Standard unit for luminous intensit
  hefnerunit(0.9, "candle"), // in use before candel
  // ? hefnercandle            hefnerunit    #
  violle(20.17, "cd"), // luminous intensity of 1 cm^2 o
  // ? lumen                   cd sr         # Luminous flux (luminous energy per
  // ? lm                      lumen         #    time unit)
  // ? talbot                  lumen s       # Luminous energy
  // ? lumberg                 talbot        # References give these values for
  // ? lumerg                  talbot        #    lumerg and lumberg both.  Note that
  // ? lux                     lm/m^2        # Illuminance or exitance (luminous
  // ? lx                      lux           #   flux incident on or coming from
  // ? phot                    lumen / cm^2  #   a surface)
  // ? ph                      phot          #
  // ? footcandle              lumen/ft^2    # Illuminance from a 1 candela source
  // ? metercandle             lumen/m^2     # Illuminance from a 1 candela source
  // ? mcs                     metercandle s # luminous energy per area, used to
  nox(1e-3, "lux"), // These two units were proposed fo
  skot(1e-3, "apostilb"), // measurements relating to dark adapte
  // ## Luminance measures
  // ^ LUMINANCE               nit
  // ? nit                     cd/m^2        # Luminance: the intensity per projected
  // ? stilb                   cd / cm^2     # area of an extended luminous source.
  // ? sb                      stilb         # (nit is from latin nitere = to shine.)
  // ? apostilb                cd/pi m^2
  // ? asb                     apostilb
  // ? blondel                 apostilb      # Named after a French scientist.
  // ? equivalentlux           cd / pi m^2   # luminance of a 1 lux surface
  // ? equivalentphot          cd / pi cm^2  # luminance of a 1 phot surface
  // ? lambert                 cd / pi cm^2
  // ? footlambert             cd / pi ft^2
  // () bril(x) units=[1;lambert]  2^(x+-100) lamberts ;log2(bril/lambert)+100
  // ## Some luminance data from the IES Lighting Handbook, 8th ed, 1993
  sunlum(1.6e9, "cd/m^2"), // at zenit
  sunillum(100e3, "lux"), // clear sk
  sunillum_o(10e3, "lux"), // overcast sk
  sunlum_h(6e6, "cd/m^2"), // value at horizo
  skylum(8000, "cd/m^2"), // average, clear sk
  skylum_o(2000, "cd/m^2"), // average, overcast sk
  moonlum(2500, "cd/m^2"),
  s100(100, "/ lx s"), // ISO 100 spee
  // ? iso100                  s100
  // ## Reflected-light meter calibration constant with ISO 100 speed
  k1250(12.5, "(cd/m2) / lx s"), // For Canon, Nikon, and Sekoni
  k1400(14, "(cd/m2) / lx s"), // For Kenko (Minolta) and Penta
  // ## Incident-light meter calibration constant with ISO 100 film
  c250(250, "lx / lx s"), // flat-disc recepto
  // ## Exposure value to scene luminance with ISO 100 imaging media
  // () ev100(x) units=[1;cd/m^2] range=(0,) 2^x k1250 / s100; log2(ev100 s100/k1250)
  // () EV100()  ev100
  // ## Exposure value to scene illuminance with ISO 100 imaging media
  // () iv100(x) units=[1;lx] range=(0,) 2^x c250 / s100; log2(iv100 s100 / c250)
  // : N_exif          1|3.125 lx s    # value in Exif 2.3 (2010), making Sv(5) = 100
  K_apex1961(11.4, "(cd/m2) / lx s"), // value in ASA PH2.12-196
  K_apex1971(12.5, "(cd/m2) / lx s"), // value in ANSI PH3.49-1971; more commo
  C_apex1961(224, "lx / lx s"), // value in PH2.12-1961 (20.83 for I i
  C_apex1971(322, "lx / lx s"), // mean value in PH3.49-1971 (30 +/- 5 for I i
  // ? N_speed         N_exif
  // ? K_lum           K_apex1971
  // ? C_illum         C_apex1961
  // () Av(A)           units=[1;1] domain=[-2,) range=[0.5,)  2^(A/2); 2 log2(Av)
  // () Tv(t)           units=[1;s] range=(0,)  2^(-t) s; log2(s / Tv)
  // () Sval(S)   units=[1;1] range=(0,) 2^S / (N_speed/lx s); log2((N_speed/lx s) Sval)
  // () Bv(x)           units=[1;cd/m^2] range=(0,)  <> 2^x K_lum N_speed ; log2(Bv / (K_lum
  // N_speed))
  // () Iv(x)           units=[1;lx] range=(0,)  <> 2^x C_illum N_speed ; log2(Iv / (C_illum
  // N_speed))
  // () Sx(S)           units=[1;1] domain=(0,)  <> log2((N_speed/lx s) S); 2^Sx / (N_speed/lx s)
  // () Sdeg(S)         units=[1;1] range=(0,) 10^((S - 1) / 10) ; (1 + 10 log(Sdeg))
  // () Sdin()          Sdeg
  // () numericalaperture(x) units=[1;1] domain=(0,1] range=[0.5,)  <> 0.5 / x ; 0.5 /
  // numericalaperture
  // () NA()            numericalaperture
  // () fnumber(x)      units=[1;1] domain=[0.5,) range=[0.5,) x ; fnumber
  // ^ TIME                    second
  anomalisticyear(365.2596, "days"), // The time between successiv
  siderealyear(365.256360417, "day"), // The time for the earth to mak
  tropicalyear(365.242198781, "day"), // The time needed for the mean su
  eclipseyear(346.62, "days"), // The line of nodes is th
  saros(223, "synodicmonth"), // The earth, moon and sun appear i
  siderealday(86164.09054, "s"), // The sidereal day is the interva
  // : siderealhour            1|24 siderealday    #   between two successive transits
  // : siderealminute          1|60 siderealhour   #   of a star over the meridian,
  // : siderealsecond          1|60 siderealminute #   or the time required  for the
  anomalisticmonth(27.55454977, "day"), // Time for the moon to travel fro
  nodicalmonth(27.2122199, "day"), // The nodes are the points wher
  // ? draconicmonth           nodicalmonth        #   an orbit crosses the ecliptic.
  // ? draconiticmonth         nodicalmonth        #   This is the time required to
  siderealmonth(27.321661, "day"), // Time required for the moon t
  lunarmonth(29, "days + 12 hours + 44 minutes + 2.8 seconds"),
  // ? synodicmonth            lunarmonth          #   Full moons occur when the sun
  // ? lunation                synodicmonth        #   and moon are on opposite sides
  // : lune                    1|30 lunation       #   of the earth.  Since the earth
  // : lunour                  1|24 lune           #   moves around the sun, the moon
  // ? year                    tropicalyear
  // ? yr                      year
  // : month                   1|12 year
  // ? mo                      month
  lustrum(5, "years"), // The Lustrum was a Roma
  decade(10, "years"),
  century(100, "years"),
  millennium(1000, "years"),
  // ? millennia               millennium
  // ? solaryear               year
  lunaryear(12, "lunarmonth"),
  calendaryear(365, "day"),
  commonyear(365, "day"),
  leapyear(366, "day"),
  julianyear(365.25, "day"),
  gregorianyear(365.2425, "day"),
  islamicyear(354, "day"), // A year of 12 lunar months. The
  islamicleapyear(355, "day"), // began counting on July 16, AD 62
  // : islamicmonth            1|12 islamicyear # They have 29 day and 30 day months.
  // ## Sidereal days
  mercuryday(58.6462, "day"),
  venusday(243.01, "day"), // retrograd
  // ? earthday                siderealday
  marsday(1.02595675, "day"),
  jupiterday(0.41354, "day"),
  saturnday(0.4375, "day"),
  uranusday(0.65, "day"), // retrograd
  neptuneday(0.768, "day"),
  plutoday(6.3867, "day"),
  mercuryyear(0.2408467, "julianyear"),
  venusyear(0.61519726, "julianyear"),
  // ? earthyear               siderealyear
  marsyear(1.8808476, "julianyear"),
  jupiteryear(11.862615, "julianyear"),
  saturnyear(29.447498, "julianyear"),
  uranusyear(84.016846, "julianyear"),
  neptuneyear(164.79132, "julianyear"),
  plutoyear(247.92065, "julianyear"),
  // : earthflattening         1|298.25642
  earthradius_equatorial(6378136.49, "m"),
  // ? earthradius_polar       (-earthflattening+1) earthradius_equatorial
  landarea(148.847e6, "km^2"),
  oceanarea(361.254e6, "km^2"),
  moonradius(1738, "km"), // mean valu
  sunradius(6.96e8, "m"),
  gauss_k(0.01720209895, ""), // This beast has dimensions o
  // ? gaussianyear      (2 pi / gauss_k) days # Year that corresponds to the Gaussian
  astronomicalunit(149597870700.0, "m"), // IAU definition from 2012, exac
  // ? au                     astronomicalunit # ephemeris for the above described
  GMsun(1.32712440018e20, "m^3 / s^2"), // heliocentric gravitational constan
  // ? solarmass                       GMsun/G # with uncertainty 8e9 is known more
  // ? sunmass                       solarmass # accurately than G.
  sundist(1.0000010178, "au"), // mean earth-sun distanc
  moondist(3.844e8, "m"), // mean earth-moon distanc
  sundist_near(1.471e11, "m"), // earth-sun distance at perihelio
  sundist_far(1.521e11, "m"), // earth-sun distance at aphelio
  moondist_min(3.564e8, "m"), // approximate least distance a
  moondist_max(4.067e8, "m"), // approximate greatest distance a
  // ? mercurymass             solarmass / 6023600   # 250
  // ? venusmass               solarmass / 408523.71 # 0.06
  // ? earthmoonmass           solarmass / 328900.56 # 0.02
  // ? marsmass                solarmass / 3098708   # 9
  // ? jupitermass             solarmass / 1047.3486 # 0.0008
  // ? saturnmass              solarmass / 3497.898  # 0.018
  // ? uranusmass              solarmass / 22902.98  # 0.03
  // ? neptunemass             solarmass / 19412.24  # 0.04
  // ? plutomass               solarmass / 1.35e8    # 0.07e8
  moonearthmassratio(0.012300034, ""), // uncertainty 3e-
  // ? earthmass               earthmoonmass / ( 1 + moonearthmassratio)
  // ? moonmass                moonearthmassratio earthmass
  oldmercurymass(0.33022e24, "kg"),
  oldvenusmass(4.8690e24, "kg"),
  oldmarsmass(0.64191e24, "kg"),
  oldjupitermass(1898.8e24, "kg"),
  oldsaturnmass(568.5e24, "kg"),
  olduranusmass(86.625e24, "kg"),
  oldneptunemass(102.78e24, "kg"),
  oldplutomass(0.015e24, "kg"),
  mercuryradius(2440, "km"),
  venusradius(6051.84, "km"),
  earthradius(6371.01, "km"),
  marsradius(3389.92, "km"),
  jupiterradius(69911, "km"),
  saturnradius(58232, "km"),
  uranusradius(25362, "km"),
  neptuneradius(24624, "km"),
  plutoradius(1151, "km"),
  moongravity(1.62, "m/s^2"),
  hubble(70, "km/s/Mpc"), // approximat
  // ? H0                      hubble
  // ? lunarparallax  asin(earthradius_equatorial / moondist) # Moon equatorial
  // ? moonhp         lunarparallax                           # horizontal parallax
  // () airmass(alt) units=[degree;1] domain=[0,90] noerror  <> 1 / (sin(alt) + 0.50572 (alt /
  // degree + 6.07995)^-1.6364)
  // () airmassz(zenith) units=[degree;1] domain=[0,90] noerror  <> 1 / (cos(zenith) + 0.50572
  // (96.07995 - zenith / degree)^-1.6364)
  extinction_coeff(0.21, ""),
  // () atm_transmission(alt) units=[degree;1] domain=[0,90] noerror  <> exp(-extinction_coeff
  // airmass(alt))
  // () atm_transmissionz(zenith) units=[degree;1] domain=[0,90] noerror  <> exp(-extinction_coeff
  // airmassz(zenith))
  // ? moonvmag	-12.74	# Moon apparent visual magnitude at mean distance
  // ? sunvmag		-26.74	# Sun apparent visual magnitude at mean distance
  // ? moonsd	asin(moonradius / moondist) # Moon angular semidiameter at mean distance
  // ? sunsd	asin(sunradius / sundist)   # Sun angular semidiameter at mean distance
  // () vmag(mag) units=[1;lx] domain=[,]  range=(0,]  <> 2.54e-6 lx 10^(-0.4 mag); -2.5 log(vmag /
  // (2.54e-6 lx))
  // () SB_degree(sb) units=[1;cd/m^2] domain=[,] range=(0,]  <> vmag(sb) / squaredegree ;  <>
  // ~vmag(SB_degree squaredegree)
  // () SB_minute(sb) units=[1;cd/m^2] domain=[,] range=(0,]  <> vmag(sb) / squareminute ;  <>
  // ~vmag(SB_minute squareminute)
  // () SB_second(sb) units=[1;cd/m^2] domain=[,] range=(0,]  <> vmag(sb) / squaresecond ;  <>
  // ~vmag(SB_second squaresecond)
  // () SB_sr(sb) units=[1;cd/m^2] domain=[,] range=(0,]  <> vmag(sb) / sr ;  <> ~vmag(SB_sr sr)
  // () SB()		SB_second
  // () SB_sec()	SB_second
  // () SB_min()	SB_minute
  // () SB_deg()	SB_degree
  // ? S10	SB_degree(10)
  hartree(2, "rydberg"), // Approximate electric potential energy o
  // ## Fundamental units
  // ? atomicmass              electronmass
  // ? atomiccharge            e
  // ? atomicaction            hbar
  // ? atomicenergy            hartree
  // ## Derived units
  // ? atomicvelocity          sqrt(atomicenergy / atomicmass)
  // ? atomictime              atomicaction / atomicenergy
  // ? atomiclength            atomicvelocity atomictime
  // ? atomicforce             atomicenergy / atomiclength
  // ? atomicmomentum          atomicenergy / atomicvelocity
  // ? atomiccurrent           atomiccharge / atomictime
  // ? atomicpotential         atomicenergy / atomiccharge   # electrical potential
  // ? atomicvolt              atomicpotential
  // ? atomicEfield            atomicpotential / atomiclength
  // ? atomicBfield            atomicvolt atomictime / atomiclength^2
  // ? atomictemperature       atomicenergy / boltzmann
  // !var UNITS_SYSTEM hartree
  // !message Hartree units selected
  // !prompt (hartree)
  // +kg           1/electronmass_SI
  // +K            k_SI / hbar_SI s
  // +m            alpha c_SI electronmass_SI / hbar_SI
  // +s            alpha c_SI m
  // +A            1 / s e_SI
  // !endvar
  // ## These thermal units treat entropy as charge, from [5]
  // ? thermalcoulomb          J/K        # entropy
  // ? thermalampere           W/K        # entropy flow
  // ? thermalfarad            J/K^2
  // ? thermalohm              K^2/W      # thermal resistance
  // ? fourier                 thermalohm
  // ? thermalhenry            J K^2/W^2  # thermal inductance
  // ? thermalvolt             K          # thermal potential difference
  // ## United States units
  // ## linear measure
  // ^ US                      1200|3937 m/ft   # These four values will convert
  // -US-                     US               #   international measures to
  // -survey-                 US               #   US Survey measures
  // -geodetic-               US
  // : int                     3937|1200 ft/m   # Convert US Survey measures to
  // -int-                    int              #   international measures
  inch(2.54, "cm"),
  // ? in                      inch
  foot(12, "inch"),
  // ? feet                    foot
  // ? ft                      foot
  yard(3, "ft"),
  // ? yd                      yard
  mile(5280, "ft"), // The mile was enlarged from 5000 f
  // : line                    1|12 inch  # Also defined as '.1 in' or as '1e-8 Wb'
  rod(5.5, "yard"),
  // ? perch                   rod
  furlong(40, "rod"), // From "furrow long
  // ? statutemile             mile
  league(3, "mile"), // Intended to be an an hour's wal
  // ## surveyor's measure
  surveyorschain(66, "surveyft"),
  // ? surveychain             surveyorschain
  // : surveyorspole           1|4 surveyorschain
  // : surveyorslink           1|100 surveyorschain
  chain(66, "ft"),
  // : link                    1|100 chain
  // ? ch                      chain
  USacre(10, "surveychain^2"),
  intacre(10, "chain^2"), // Acre based on international f
  // ? intacrefoot             acre foot
  // ? USacrefoot              USacre surveyfoot
  // ? acrefoot                intacrefoot
  // ? acre                    intacre
  // ? section                 mile^2
  township(36, "section"),
  homestead(160, "acre"), // Area of land granted by the 1862 Homestea
  // ? gunterschain            surveyorschain
  engineerschain(100, "ft"),
  // : engineerslink           1|100 engineerschain
  // ? ramsdenschain           engineerschain
  // ? ramsdenslink            engineerslink
  gurleychain(33, "feet"), // Andrew Ellicott chain is th
  // : gurleylink              1|50 gurleychain  # same length
  wingchain(66, "feet"), // Chain from 1664, introduced b
  // : winglink                1|80 wingchain    # Vincent Wing, also found in a
  // ## early US length standards
  troughtonyard(914.42190, "mm"),
  bronzeyard11(914.39980, "mm"),
  // ? mendenhallyard          surveyyard
  // ? internationalyard       yard
  // ## nautical measure
  fathom(6, "ft"), // Originally defined as the distance fro
  nauticalmile(1852, "m"), // Supposed to be one minute of latitude a
  // : cable                   1|10 nauticalmile
  // ? intcable                cable              # international cable
  // ? cablelength             cable
  UScable(100, "USfathom"),
  navycablelength(720, "USft"), // used for depth in wate
  marineleague(3, "nauticalmile"),
  // ? geographicalmile        brnauticalmile
  // ? knot                    nauticalmile / hr
  // ? click                   km       # US military slang
  // ? klick                   click
  // ## Avoirdupois weight
  pound(0.45359237, "kg"), // The one normally use
  // ? lb                      pound           # From the latin libra
  // : grain                   1|7000 pound    # The grain is the same in all three
  // : ounce                   1|16 pound
  // ? oz                      ounce
  // : dram                    1|16 ounce
  // ? dr                      dram
  ushundredweight(100, "pounds"),
  // ? cwt                     hundredweight
  // ? shorthundredweight      ushundredweight
  // ? uston                   shortton
  shortton(2000, "lb"),
  // : quarterweight           1|4 uston
  // : shortquarterweight      1|4 shortton
  // ? shortquarter            shortquarterweight
  troypound(5760, "grain"),
  // : troyounce               1|12 troypound
  // ? ozt                     troyounce
  // : pennyweight             1|20 troyounce  # Abbreviated "d" in reference to a
  // ? dwt                     pennyweight     #   Frankish coin called the "denier"
  // ? assayton                mg ton / troyounce  # mg / assayton = troyounce / ton
  // ? usassayton              mg uston / troyounce
  // ? brassayton              mg brton / troyounce
  // ? fineounce               troyounce       # A troy ounce of 99.5% pure gold
  // ## Some other jewelers units
  metriccarat(0.2, "gram"), // Defined in 190
  metricgrain(50, "mg"),
  // ? carat                   metriccarat
  // ? ct                      carat
  // : jewelerspoint           1|100 carat
  // : silversmithpoint        1|4000 inch
  momme(3.75, "grams"), // Traditional Japanese unit base
  // ## Apothecaries' weight
  // ? appound                 troypound
  // ? apounce                 troyounce
  // : apdram                  1|8 apounce
  // : apscruple               1|3 apdram
  // ## Liquid measure
  usgallon(231, "in^3"), // US liquid measure is derived fro
  // ? gal                     gallon          # the British wine gallon of 1707.
  // : quart                   1|4 gallon      # See the "winegallon" entry below
  // : pint                    1|2 quart       # more historical information.
  // : gill                    1|4 pint
  // : usquart                 1|4 usgallon
  // : uspint                  1|2 usquart
  // : usgill                  1|4 uspint
  // : usfluidounce            1|16 uspint
  // : fluiddram               1|8 usfloz
  // : minimvolume             1|60 fluiddram
  // ? qt                      quart
  // ? pt                      pint
  // ? floz                    fluidounce
  // ? usfloz                  usfluidounce
  // ? fldr                    fluiddram
  liquidbarrel(31.5, "usgallon"),
  usbeerbarrel(2, "beerkegs"),
  beerkeg(15.5, "usgallon"), // Various among brewer
  // : ponykeg                 1|2 beerkeg
  winekeg(12, "usgallon"),
  petroleumbarrel(42, "usgallon"), // Originated in Pennsylvania oi
  // ? barrel                  petroleumbarrel # fields, from the winetierce
  // ? bbl                     barrel
  ushogshead(2, "liquidbarrel"),
  usfirkin(9, "usgallon"),
  usbushel(2150.42, "in^3"), // Volume of 8 inch cylinder with 18.
  // ? bu                      bushel        # inch diameter (rounded)
  // : peck                    1|4 bushel
  // : uspeck                  1|4 usbushel
  // : brpeck                  1|4 brbushel
  // ? pk                      peck
  // : drygallon               1|2 uspeck
  // : dryquart                1|4 drygallon
  // : drypint                 1|2 dryquart
  drybarrel(7056, "in^3"), // Used in US for fruits, vegetables
  cranberrybarrel(5826, "in^3"), // US cranberry barre
  heapedbushel(1.278, "usbushel"), // The following explanation for thi
  wheatbushel(60, "lb"),
  soybeanbushel(60, "lb"),
  cornbushel(56, "lb"),
  ryebushel(56, "lb"),
  barleybushel(48, "lb"),
  oatbushel(32, "lb"),
  ricebushel(45, "lb"),
  canada_oatbushel(34, "lb"),
  // ## Wine and Spirits measure
  ponyvolume(1, "usfloz"),
  jigger(1.5, "usfloz"), // Can vary between 1 and 2 usflo
  // ? shot                    jigger     # Sometimes 1 usfloz
  eushot(25, "ml"), // EU standard spirits measur
  // : fifth                   1|5 usgallon
  winebottle(750, "ml"), // US industry standard, 197
  // : winesplit               1|4 winebottle
  magnum(1.5, "liter"), // Standardized in 1979, but give
  metrictenth(375, "ml"),
  metricfifth(750, "ml"),
  metricquart(1, "liter"),
  // ## Old British bottle size
  // : reputedquart            1|6 brgallon
  // : reputedpint             1|2 reputedquart
  // ? brwinebottle            reputedquart       # Very close to 1|5 winegallon
  // ## French champagne bottle sizes
  split(200, "ml"),
  jeroboam(2, "magnum"),
  rehoboam(3, "magnum"),
  methuselah(4, "magnum"),
  imperialbottle(4, "magnum"),
  salmanazar(6, "magnum"),
  balthazar(8, "magnum"),
  nebuchadnezzar(10, "magnum"),
  solomon(12, "magnum"),
  melchior(12, "magnum"),
  sovereign(17.5, "magnum"),
  primat(18, "magnum"),
  goliath(18, "magnum"),
  melchizedek(20, "magnum"),
  midas(20, "magnum"),
  wineglass(150, "mL"), // the size of a "typical" servin
  alcoholunitus(14, "g / ethanoldensity"),
  alcoholunitca(13.6, "g / ethanoldensity"),
  alcoholunituk(8, "g / ethanoldensity"),
  alcoholunitau(10, "g / ethanoldensity"),
  /// *coffeeratio             55 g/L  #
  // ? clarkdegree     grains/brgallon # Content by weigh of calcium carbonate
  // ? gpg             grains/usgallon # Divide by water's density to convert to
  // ## Shoe measures
  // : shoeiron                1|48 inch    # Used to measure leather in soles
  // : shoeounce               1|64 inch    # Used to measure non-sole shoe leather
  // : shoesize_delta          1|3 inch     # USA shoe sizes differ by this amount
  shoe_men0(8.25, "inch"),
  // : shoe_women0             (7+11|12) inch
  // : shoe_boys0              (3+11|12) inch
  // : shoe_girls0             (3+7|12) inch
  // () shoesize_men(n) units=[1;inch]   shoe_men0 + n shoesize_delta ;  <>
  // (shoesize_men+(-shoe_men0))/shoesize_delta
  // () shoesize_women(n) units=[1;inch] shoe_women0 + n shoesize_delta ;  <>
  // (shoesize_women+(-shoe_women0))/shoesize_delta
  // () shoesize_boys(n) units=[1;inch]  shoe_boys0 + n shoesize_delta ;  <>
  // (shoesize_boys+(-shoe_boys0))/shoesize_delta
  // () shoesize_girls(n) units=[1;inch] shoe_girls0 + n shoesize_delta ;  <>
  // (shoesize_girls+(-shoe_girls0))/shoesize_delta
  // : europeshoesize          2|3 cm
  // ## USA slang units
  // ? buck                    US$
  fin(5, "US$"),
  sawbuck(10, "US$"),
  usgrand(1000, "US$"),
  // ? greenback               US$
  // ? key                     kg           # usually of marijuana, 60's
  lid(1, "oz"), // Another 60's weed uni
  // ? footballfield           usfootballfield
  usfootballfield(100, "yards"),
  canadafootballfield(110, "yards"), // And 65 yards wid
  marathon(26, "miles + 385 yards"),
  // ## British
  // ^ UK                      UKlength_SJJ
  // -UK-                     UK
  // -british-                UK
  UKlength_B(0.9143992, "meter / yard"), // Benoit found the yard to b
  UKlength_SJJ(0.91439841, "meter / yard"), // In 1922, Seers, Jolly an
  // ? UKlength_K              meter / 39.37079 inch # In 1816 Kater found this ratio
  // ? UKlength_C            meter / 1.09362311 yard # In 1866 Clarke found the meter
  brnauticalmile(6080, "ft"), // Used until 1970 when the U
  // ? brknot                  brnauticalmile / hr   #   switched to the international
  // : brcable                 1|10 brnauticalmile   #   nautical mile.
  // ? admiraltymile           brnauticalmile
  // ? admiraltyknot           brknot
  // ? admiraltycable          brcable
  seamile(6000, "ft"),
  shackle(15, "fathoms"), // Adopted 1949 by British nav
  clove(7, "lb"),
  stone(14, "lb"),
  tod(28, "lb"),
  // : brquarterweight         1|4 brhundredweight
  brhundredweight(8, "stone"),
  // ? longhundredweight       brhundredweight
  longton(20, "brhundredweight"),
  // ? brton                   longton
  // ## British Imperial volume measures
  // : brminim                 1|60 brdram
  // : brscruple               1|3 brdram
  // ? fluidscruple            brscruple
  // : brdram                  1|8 brfloz
  // : brfluidounce            1|20 brpint
  // ? brfloz                  brfluidounce
  // : brgill                  1|4 brpint
  // : brpint                  1|2 brquart
  // : brquart                 1|4 brgallon
  brgallon(4.54609, "l"), // The British Imperial gallon wa
  brbarrel(36, "brgallon"), // Used for bee
  brbushel(8, "brgallon"),
  brheapedbushel(1.278, "brbushel"),
  brquarter(8, "brbushel"),
  brchaldron(36, "brbushel"),
  bag(4, "brbushel"),
  bucket(4, "brgallon"),
  kilderkin(2, "brfirkin"),
  last(40, "brbushel"),
  // ? noggin                  brgill
  pottle(0.5, "brgallon"),
  pin(4.5, "brgallon"),
  puncheon(72, "brgallon"),
  seam(8, "brbushel"),
  coomb(4, "brbushel"),
  boll(6, "brbushel"),
  // : firlot                  1|4 boll
  brfirkin(9, "brgallon"), // Used for ale and bee
  cran(37.5, "brgallon"), // measures herring, about 750 fis
  brwinehogshead(52.5, "brgallon"), // This value is approximately equa
  // ? brhogshead              brwinehogshead #   to the old wine hogshead of 63
  brbeerhogshead(54, "brgallon"),
  brbeerbutt(2, "brbeerhogshead"),
  registerton(100, "ft^3"), // Used for internal capacity of ship
  shippington(40, "ft^3"), // Used for ship's cargo freight or timbe
  brshippington(42, "ft^3"), //
  // ? freightton            shippington # Both register ton and shipping ton derive
  displacementton(35, "ft^3"), // Approximate volume of a longton weight o
  waterton(224, "brgallon"),
  strike(70.5, "l"), // 16th century unit, sometime
  amber(4, "brbushel"), // Used for dry and liquid capacity [18
  // ## British volume measures with "imperial"
  // ? imperialminim           brminim
  // ? imperialscruple         brscruple
  // ? imperialdram            brdram
  // ? imperialfluidounce      brfluidounce
  // ? imperialfloz            brfloz
  // ? imperialgill            brgill
  // ? imperialpint            brpint
  // ? imperialquart           brquart
  // ? imperialgallon          brgallon
  // ? imperialbarrel          brbarrel
  // ? imperialbushel          brbushel
  // ? imperialheapedbushel    brheapedbushel
  // ? imperialquarter         brquarter
  // ? imperialchaldron        brchaldron
  // ? imperialwinehogshead    brwinehogshead
  // ? imperialhogshead        brhogshead
  // ? imperialbeerhogshead    brbeerhogshead
  // ? imperialbeerbutt        brbeerbutt
  // ? imperialfirkin          brfirkin
  // ## obscure British lengths
  // : barleycorn              1|3 UKinch   # Given in Realm of Measure as the
  // : nail                    1|16 UKyard  # Originally the width of the thumbnail,
  pole(16.5, "UKft"), // This was 15 Saxon feet, the Saxo
  rope(20, "UKft"), // foot (aka northern foot) being longe
  englishell(45, "UKinch"),
  flemishell(27, "UKinch"),
  // ? ell                     englishell   # supposed to be measure from elbow to
  span(9, "UKinch"), // supposed to be distance from thum
  goad(4.5, "UKft"), // used for cloth, possibly named after th
  // ## misc obscure British units
  hide(120, "acre"), // English unit of land area dating to the 7t
  // : virgate                 1|4 hide
  // : nook                    1|2 virgate
  // ? rood                    furlong rod  # Area of a strip a rod by a furlong
  // ? englishcarat            troyounce/151.5 # Originally intended to be 4 grain
  mancus(2, "oz"),
  mast(2.5, "lb"),
  nailkeg(100, "lbs"),
  basebox(31360, "in^2"), // Used in metal platin
  // ## alternate spellings
  // ? gramme                  gram
  // ? litre                   liter
  // ? dioptre                 diopter
  // ? aluminium               aluminum
  // ? sulphur                 sulfur
  // ## Units derived the human body (may not be very accurate)
  geometricpace(5, "ft"), // distance between points where the sam
  pace(2.5, "ft"), // distance between points where alternat
  USmilitarypace(30, "in"), // United States official military pac
  USdoubletimepace(36, "in"), // United States official doubletime pac
  // : fingerbreadth           7|8 in # The finger is defined as either the width
  fingerlength(4.5, "in"), // or length of the finge
  // ? finger                  fingerbreadth
  // ? palmwidth               hand   # The palm is a unit defined as either the width
  palmlength(8, "in"), // or the length of the han
  hand(4, "inch"), // width of han
  shaftment(6, "inch"), // Distance from tip of outstretched thumb to th
  smoot(5, "ft + 7 in"), // Created as part of an MIT fraternity prank
  tomcruise(5, "ft + 7.75 in"), // Height of Tom Cruis
  // ## Cooking measures
  // ## Common abbreviations
  // ? tbl                     tablespoon
  // ? tbsp                    tablespoon
  // ? tblsp                   tablespoon
  // ? Tb                      tablespoon
  // ? tsp                     teaspoon
  // : saltspoon               1|4 tsp
  // ## US measures
  uscup(8, "usfloz"),
  // : ustablespoon            1|16 uscup
  // : usteaspoon              1|3 ustablespoon
  // ? ustbl                   ustablespoon
  // ? ustbsp                  ustablespoon
  // ? ustblsp                 ustablespoon
  // ? ustsp                   usteaspoon
  metriccup(250, "ml"),
  // : stickbutter             1|4 lb            # Butter in the USA is sold in one
  legalcup(240, "ml"), // The cup used on nutrition labelin
  // : legaltablespoon         1|16 legalcup
  // ? legaltbsp               legaltablespoon
  // () scoop(n)  units=[1;cup] domain=[4,100] range=[0.04,1]  <> 32 usfloz / n ; 32 usfloz / scoop
  // ## US can sizes.
  number1can(10, "usfloz"),
  number2can(19, "usfloz"),
  // . number2.5can(3.5, "uscups"),
  number3can(4, "uscups"),
  number5can(7, "uscups"),
  number10can(105, "usfloz"),
  // ## British measures
  // : brcup                   1|2 brpint
  // : brteacup                1|3 brpint
  brtablespoon(15, "ml"), // Also 5|8 brfloz, approx 17.7 m
  // : brteaspoon              1|3 brtablespoon  # Also 1|4 brtablespoon
  brdessertspoon(2, "brteaspoon"),
  // ? dessertspoon            brdessertspoon
  // ? dsp                     dessertspoon
  // ? brtsp                   brteaspoon
  // ? brtbl                   brtablespoon
  // ? brtbsp                  brtablespoon
  // ? brtblsp                 brtablespoon
  // ## Australian
  australiatablespoon(20, "ml"),
  // ? austbl                  australiatablespoon
  // ? austbsp                 australiatablespoon
  // ? austblsp                australiatablespoon
  // : australiateaspoon       1|4 australiatablespoon
  // ? austsp                  australiateaspoon
  // ## Italian
  etto(100, "g"), // Used for buying items like meat an
  // ? etti                    etto           #   cheese.
  // ## Chinese
  catty(0.5, "kg"),
  // : oldcatty                4|3 lbs        # Before metric conversion.
  // : tael                    1|16 oldcatty  # Should the tael be defined both ways?
  mace(0.1, "tael"),
  oldpicul(100, "oldcatty"),
  picul(100, "catty"), // Chinese usag
  // ## Indian
  seer(14400, "grain"), // British Colonial standar
  // ? ser                     seer
  maund(40, "seer"),
  pakistanseer(1, "kg"),
  pakistanmaund(40, "pakistanseer"),
  // : chittak                 1|16 seer
  // : tola                    1|5 chittak
  // : ollock                  1|4 liter      # Is this right?
  // ## Japanese
  japancup(200, "ml"),
  butter(8, "oz/uscup"),
  butter_clarified(6.8, "oz/uscup"),
  cocoa_butter(9, "oz/uscup"),
  shortening(6.75, "oz/uscup"), // vegetable shortenin
  oil(7.5, "oz/uscup"),
  cakeflour_sifted(3.5, "oz/uscup"), // The density of flour depends on th
  cakeflour_spooned(4, "oz/uscup"), // measuring method. "Scooped", o
  cakeflour_scooped(4.5, "oz/uscup"), // "dip and sweep" refers to dipping
  flour_sifted(4, "oz/uscup"), // measure into a bin, and then sweepin
  flour_spooned(4.25, "oz/uscup"), // the excess off the top. "Spooned
  flour_scooped(5, "oz/uscup"), // means to lightly spoon into a measur
  breadflour_sifted(4.25, "oz/uscup"), // and then sweep the top. Sifted mean
  breadflour_spooned(4.5, "oz/uscup"), // sifting the flour directly into
  breadflour_scooped(5.5, "oz/uscup"), // measure and then sweeping the top
  cornstarch(120, "grams/uscup"),
  dutchcocoa_sifted(75, "g/uscup"), // These are for Dutch processed coco
  dutchcocoa_spooned(92, "g/uscup"),
  dutchcocoa_scooped(95, "g/uscup"),
  cocoa_sifted(75, "g/uscup"), // These are for nonalkalized coco
  cocoa_spooned(82, "g/uscup"),
  cocoa_scooped(95, "g/uscup"),
  heavycream(232, "g/uscup"),
  milk(242, "g/uscup"),
  sourcream(242, "g/uscup"),
  molasses(11.25, "oz/uscup"),
  cornsyrup(11.5, "oz/uscup"),
  honey(11.75, "oz/uscup"),
  sugar(200, "g/uscup"),
  powdered_sugar(4, "oz/uscup"),
  brownsugar_light(217, "g/uscup"), // packe
  brownsugar_dark(239, "g/uscup"),
  baking_powder(4.6, "grams / ustsp"),
  salt(6, "g / ustsp"),
  koshersalt(2.8, "g / ustsp"), // Diamond Crystal kosher sal
  koshersalt_morton(4.8, "g / ustsp"), // Morton kosher sal
  // ## Egg weights and volumes for a USA large egg
  egg(50, "grams"), // without shel
  eggwhite(30, "grams"),
  eggyolk(18.6, "grams"),
  eggvolume(3, "ustablespoons + 1|2 ustsp"),
  eggwhitevolume(2, "ustablespoons"),
  eggyolkvolume(3.5, "ustsp"),
  // ## Alcohol density
  ethanoldensity(0.7893, "g/cm^3"), // From CRC Handbook, 91st Editio
  // ? alcoholdensity          ethanoldensity
  // [] sugar_conc_bpe[K]  <> 0 0.0000   5 0.0788  10 0.1690  15 0.2729  20 0.3936  25 0.5351   <>
  // 30 0.7027  35 0.9036  40 1.1475  42 1.2599  44 1.3825  46 1.5165   <> 48 1.6634  50 1.8249  52
  // 2.0031  54 2.2005  56 2.4200  58 2.6651   <> 60 2.9400  61 3.0902  62 3.2499  63 3.4198  64
  // 3.6010  65 3.7944   <> 66 4.0012  67 4.2227  68 4.4603  69 4.7156  70 4.9905  71 5.2870   <> 72
  // 5.6075  73 5.9546  74 6.3316  75 6.7417  76 7.1892  77 7.6786   <> 78.0  8.2155  79.0  8.8061
  // 80.0  9.4578  80.5  9.8092  81.0 10.1793   <> 81.5 10.5693  82.0 10.9807  82.5 11.4152  83.0
  // 11.8743  83.5 12.3601   <> 84.0 12.8744  84.5 13.4197  85.0 13.9982  85.5 14.6128  86.0 15.2663
  //   <> 86.5 15.9620  87.0 16.7033  87.5 17.4943  88.0 18.3391  88.5 19.2424   <> 89.0 20.2092
  // 89.5 21.2452  90.0 22.3564  90.5 23.5493  91.0 24.8309   <> 91.5 26.2086  92.0 27.6903  92.5
  // 29.2839  93.0 30.9972  93.5 32.8374   <> 94.0 34.8104  94.5 36.9195  95.0 39.1636  95.5 41.5348
  //  96.0 44.0142   <> 96.5 46.5668  97.0 49.1350  97.5 51.6347  98.0 53.9681  98.1 54.4091   <>
  // 98.2 54.8423  98.3 55.2692  98.4 55.6928  98.5 56.1174  98.6 56.5497   <> 98.7 56.9999  98.8
  // 57.4828  98.9 58.0206  99.0 58.6455  99.1 59.4062   <> 99.2 60.3763  99.3 61.6706  99.4 63.4751
  //  99.5 66.1062  99.6 70.1448   <> 99.7 76.7867
  // () sugar_bpe(T) units=[K;g/cm^3] domain=[0,39.1636] range=[0.99717,1.5144619]  <>
  // brix(~sugar_conc_bpe(T)); sugar_conc_bpe(~brix(sugar_bpe))
  // () sugar_bp(T) units=[K;g/cm^3] domain=[373.15,412.3136]  <> range=[0.99717,1.5144619]  <>
  // brix(~sugar_conc_bpe(T-tempC(100))) ; <> sugar_conc_bpe(~brix(sugar_bp))+tempC(100)
  // () sugar_bpF(T) units=[1;g/cm^3] domain=[212,282.49448] range=[0.99717,1.5144619] <>
  // brix(~sugar_conc_bpe(tempF(T)+-tempC(100))) ; <>
  // ~tempF(sugar_conc_bpe(~brix(sugar_bpF))+tempC(100))
  // () sugar_bpC(T) units=[1;g/cm^3] domain=[100,139.1636] range=[0.99717,1.5144619] <>
  // brix(~sugar_conc_bpe(tempC(T)+-tempC(100))) ; <>
  // ~tempC(sugar_conc_bpe(~brix(sugar_bpC))+tempC(100))
  baumeconst(145, ""), // US valu
  // () baume(d) units=[1;g/cm^3] domain=[0,145) range=[1,)  <> (baumeconst/(baumeconst+-d)) g/cm^3
  // ;  <> (baume+((-g)/cm^3)) baumeconst / baume
  // () twaddell(x) units=[1;g/cm^3] domain=[-200,) range=[0,)  <> (1 + 0.005 x) g / cm^3 ;  <> 200
  // (twaddell / (g/cm^3) +- 1)
  // () quevenne(x) units=[1;g/cm^3] domain=[-1000,) range=[0,)  <> (1 + 0.001 x) g / cm^3 ;  <>
  // 1000 (quevenne / (g/cm^3) +- 1)
  // [] brix[0.99717g/cm^3] <> 0 1.00000  1 1.00390  2 1.00780  3 1.01173  4 1.01569  5 1.01968  <>
  // 6 1.02369  7 1.02773  8 1.03180  9 1.03590 10 1.04003 11 1.04418  <> 12 1.04837 13 1.05259 14
  // 1.05683 15 1.06111 16 1.06542 17 1.06976  <> 18 1.07413 19 1.07853 20 1.08297 21 1.08744 22
  // 1.09194 23 1.09647  <> 24 1.10104 25 1.10564 26 1.11027 27 1.11493 28 1.11963 29 1.12436  <> 30
  // 1.12913 31 1.13394 32 1.13877 33 1.14364 34 1.14855 35 1.15350  <> 36 1.15847 37 1.16349 38
  // 1.16853 39 1.17362 40 1.17874 41 1.18390  <> 42 1.18910 43 1.19434 44 1.19961 45 1.20491 46
  // 1.21026 47 1.21564  <> 48 1.22106 49 1.22652 50 1.23202 51 1.23756 52 1.24313 53 1.24874  <> 54
  // 1.25439 55 1.26007 56 1.26580 57 1.27156 58 1.27736 59 1.28320  <> 60 1.28909 61 1.29498 62
  // 1.30093 63 1.30694 64 1.31297 65 1.31905  <> 66 1.32516 67 1.33129 68 1.33748 69 1.34371 70
  // 1.34997 71 1.35627  <> 72 1.36261 73 1.36900 74 1.37541 75 1.38187 76 1.38835 77 1.39489  <> 78
  // 1.40146 79 1.40806 80 1.41471 81 1.42138 82 1.42810 83 1.43486  <> 84 1.44165 85 1.44848 86
  // 1.45535 87 1.46225 88 1.46919 89 1.47616  <> 90 1.48317 91 1.49022 92 1.49730 93 1.50442 94
  // 1.51157 95 1.51876
  // () apidegree(x) units=[1;g/cm^3] domain=[-131.5,) range=[0,)  <> 141.5 g/cm^3 / (x+131.5) ;  <>
  // 141.5 (g/cm^3) / apidegree + (-131.5)
  // ## North American Hardwoods
  wood_cherry(35, "lb/ft^3"),
  wood_redoak(44, "lb/ft^3"),
  wood_whiteoak(47, "lb/ft^3"),
  wood_blackwalnut(38, "lb/ft^3"),
  // ? wood_walnut             wood_blackwalnut
  wood_birch(43, "lb/ft^3"),
  wood_hardmaple(44, "lb/ft^3"),
  wood_bigleafmaple(34, "lb/ft^3"),
  wood_boxeldermaple(30, "lb/ft^3"),
  wood_redmaple(38, "lb/ft^3"),
  wood_silvermaple(33, "lb/ft^3"),
  wood_stripedmaple(32, "lb/ft^3"),
  // ? wood_softmaple         (wood_bigleafmaple  <> + wood_boxeldermaple  <> + wood_redmaple  <> +
  // wood_silvermaple  <> + wood_stripedmaple) / 5
  wood_poplar(29, "lb/ft^3"),
  wood_beech(45, "lb/ft^3"),
  // ## North American Softwoods
  wood_jeffreypine(28, "lb/ft^3"),
  wood_ocotepine(44, "lb/ft^3"),
  wood_ponderosapine(28, "lb/ft^3"),
  wood_loblollypine(35, "lb/ft^3"),
  wood_longleafpine(41, "lb/ft^3"),
  wood_shortleafpine(35, "lb/ft^3"),
  wood_slashpine(41, "lb/ft^3"),
  // ? wood_yellowpine        (wood_loblollypine  <> + wood_longleafpine  <> + wood_shortleafpine
  // <> + wood_slashpine) / 4
  wood_redpine(34, "lb/ft^3"),
  wood_easternwhitepine(25, "lb/ft^3"),
  wood_westernwhitepine(27, "lb/ft^3"),
  // ? wood_whitepine         (wood_easternwhitepine + wood_westernwhitepine) / 2
  wood_douglasfir(32, "lb/ft^3"),
  wood_blackspruce(28, "lb/ft^3"),
  wood_engelmannspruce(24, "lb/ft^3"),
  wood_redspruce(27, "lb/ft^3"),
  wood_sitkaspruce(27, "lb/ft^3"),
  wood_whitespruce(27, "lb/ft^3"),
  // ? wood_spruce            (wood_blackspruce  <> + wood_engelmannspruce  <> + wood_redspruce  <>
  // + wood_sitkaspruce  <> + wood_whitespruce) / 5
  // ## Other woods
  wood_basswood(26, "lb/ft^3"),
  wood_balsa(9, "lb/ft^3"),
  wood_ebony_gaboon(60, "lb/ft^3"),
  wood_ebony_macassar(70, "lb/ft^3"),
  wood_mahogany(37, "lb/ft^3"), // True (Honduran) mahogany
  wood_teak(41, "lb/ft^3"),
  wood_rosewood_brazilian(52, "lb/ft^3"),
  wood_rosewood_honduran(64, "lb/ft^3"),
  wood_rosewood_indian(52, "lb/ft^3"),
  wood_cocobolo(69, "lb/ft^3"),
  wood_bubinga(56, "lb/ft^3"),
  wood_zebrawood(50, "lb/ft^3"),
  wood_koa(38, "lb/ft^3"),
  wood_snakewood(75.7, "lb/ft^3"),
  wood_lignumvitae(78.5, "lb/ft^3"),
  wood_blackwood(79.3, "lb/ft^3"),
  wood_blackironwood(84.5, "lb/ft^3"), // Krugiodendron ferreum, liste
  // ## North American Hardwoods
  wood_mod_beech(1.720e6, "lbf/in^2"),
  wood_mod_birchyellow(2.010e6, "lbf/in^2"),
  // ? wood_mod_birch              wood_mod_birchyellow
  wood_mod_cherry(1.490e6, "lbf/in^2"),
  wood_mod_hardmaple(1.830e6, "lbf/in^2"),
  wood_mod_bigleafmaple(1.450e6, "lbf/in^2"),
  wood_mod_boxeldermaple(1.050e6, "lbf/in^2"),
  wood_mod_redmaple(1.640e6, "lbf/in^2"),
  wood_mod_silvermaple(1.140e6, "lbf/in^2"),
  // ? wood_mod_softmaple         (wood_mod_bigleafmaple  <> + wood_mod_boxeldermaple  <> +
  // wood_mod_redmaple  <> + wood_mod_silvermaple) / 4
  wood_mod_redoak(1.761e6, "lbf/in^2"),
  wood_mod_whiteoak(1.762e6, "lbf/in^2"),
  wood_mod_poplar(1.580e6, "lbf/in^2"),
  wood_mod_blackwalnut(1.680e6, "lbf/in^2"),
  // ? wood_mod_walnut             wood_mod_blackwalnut
  // ## North American Softwoods
  wood_mod_jeffreypine(1.240e6, "lbf/in^2"),
  wood_mod_ocotepine(2.209e6, "lbf/in^2"),
  wood_mod_ponderosapine(1.290e6, "lbf/in^2"),
  wood_mod_loblollypine(1.790e6, "lbf/in^2"),
  wood_mod_longleafpine(1.980e6, "lbf/in^2"),
  wood_mod_shortleafpine(1.750e6, "lbf/in^2"),
  wood_mod_slashpine(1.980e6, "lbf/in^2"),
  // ? wood_mod_yellowpine        (wood_mod_loblollypine  <> + wood_mod_longleafpine  <> +
  // wood_mod_shortleafpine  <> + wood_mod_slashpine) / 4
  wood_mod_redpine(1.630e6, "lbf/in^2"),
  wood_mod_easternwhitepine(1.240e6, "lbf/in^2"),
  wood_mod_westernwhitepine(1.460e6, "lbf/in^2"),
  // ? wood_mod_whitepine         (wood_mod_easternwhitepine +  <> wood_mod_westernwhitepine) / 2
  wood_mod_douglasfir(1.765e6, "lbf/in^2"),
  wood_mod_blackspruce(1.523e6, "lbf/in^2"),
  wood_mod_englemannspruce(1.369e6, "lbf/in^2"),
  wood_mod_redspruce(1.560e6, "lbf/in^2"),
  wood_mod_sitkaspruce(1.600e6, "lbf/in^2"),
  wood_mod_whitespruce(1.315e6, "lbf/in^2"),
  // ? wood_mod_spruce            (wood_mod_blackspruce  <> + wood_mod_englemannspruce  <> +
  // wood_mod_redspruce + wood_mod_sitkaspruce  <> + wood_mod_whitespruce) / 5
  // ## Other woods
  wood_mod_balsa(0.538e6, "lbf/in^2"),
  wood_mod_basswood(1.460e6, "lbf/in^2"),
  wood_mod_blackwood(2.603e6, "lbf/in^2"), // African, Dalbergia melanoxylo
  wood_mod_bubinga(2.670e6, "lbf/in^2"),
  wood_mod_cocobolo(2.712e6, "lbf/in^2"),
  wood_mod_ebony_gaboon(2.449e6, "lbf/in^2"),
  wood_mod_ebony_macassar(2.515e6, "lbf/in^2"),
  wood_mod_blackironwood(2.966e6, "lbf/in^2"), // Krugiodendron ferreu
  wood_mod_koa(1.503e6, "lbf/in^2"),
  wood_mod_lignumvitae(2.043e6, "lbf/in^2"),
  wood_mod_mahogany(1.458e6, "lbf/in^2"), // True (Honduran) mahogany
  wood_mod_rosewood_brazilian(2.020e6, "lbf/in^2"),
  wood_mod_rosewood_honduran(3.190e6, "lbf/in^2"),
  wood_mod_rosewood_indian(1.668e6, "lbf/in^2"),
  wood_mod_snakewood(3.364e6, "lbf/in^2"),
  wood_mod_teak(1.781e6, "lbf/in^2"),
  wood_mod_zebrawood(2.374e6, "lbf/in^2"),
  area_russia(17098246, "km^2"),
  area_antarctica(14000000, "km^2"),
  area_china(9596961, "km^2"),
  // ? area_us                   area_unitedstates
  area_brazil(8515767, "km^2"),
  area_australia(7692024, "km^2"),
  area_india(3287263, "km^2"),
  area_argentina(2780400, "km^2"),
  area_kazakhstan(2724900, "km^2"),
  area_algeria(2381741, "km^2"),
  area_drcongo(2344858, "km^2"),
  area_greenland(2166086, "km^2"),
  area_saudiarabia(2149690, "km^2"),
  area_mexico(1964375, "km^2"),
  area_indonesia(1910931, "km^2"),
  area_sudan(1861484, "km^2"),
  area_libya(1759540, "km^2"),
  area_iran(1648195, "km^2"),
  area_mongolia(1564110, "km^2"),
  area_peru(1285216, "km^2"),
  area_chad(1284000, "km^2"),
  area_niger(1267000, "km^2"),
  area_angola(1246700, "km^2"),
  area_mali(1240192, "km^2"),
  area_southafrica(1221037, "km^2"),
  area_colombia(1141748, "km^2"),
  area_ethiopia(1104300, "km^2"),
  area_bolivia(1098581, "km^2"),
  area_mauritania(1030700, "km^2"),
  area_egypt(1002450, "km^2"),
  area_tanzania(945087, "km^2"),
  area_nigeria(923768, "km^2"),
  area_venezuela(916445, "km^2"),
  area_pakistan(881912, "km^2"),
  area_namibia(825615, "km^2"),
  area_mozambique(801590, "km^2"),
  area_turkey(783562, "km^2"),
  area_chile(756102, "km^2"),
  area_zambia(752612, "km^2"),
  area_myanmar(676578, "km^2"),
  // ? area_burma                area_myanmar
  area_afghanistan(652230, "km^2"),
  area_southsudan(644329, "km^2"),
  area_france(640679, "km^2"),
  area_somalia(637657, "km^2"),
  area_centralafrica(622984, "km^2"),
  area_ukraine(603500, "km^2"),
  area_crimea(27000, "km^2"), // occupied by Russia; included i
  area_madagascar(587041, "km^2"),
  area_botswana(581730, "km^2"),
  area_kenya(580367, "km^2"),
  area_yemen(527968, "km^2"),
  area_thailand(513120, "km^2"),
  area_spain(505992, "km^2"),
  area_turkmenistan(488100, "km^2"),
  area_cameroon(475422, "km^2"),
  area_papuanewguinea(462840, "km^2"),
  area_sweden(450295, "km^2"),
  area_uzbekistan(447400, "km^2"),
  area_morocco(446550, "km^2"),
  area_iraq(438317, "km^2"),
  area_paraguay(406752, "km^2"),
  area_zimbabwe(390757, "km^2"),
  area_japan(377973, "km^2"),
  area_germany(357114, "km^2"),
  area_congorepublic(342000, "km^2"),
  area_finland(338424, "km^2"),
  area_vietnam(331212, "km^2"),
  area_malaysia(330803, "km^2"),
  area_norway(323802, "km^2"),
  area_ivorycoast(322463, "km^2"),
  area_poland(312696, "km^2"),
  area_oman(309500, "km^2"),
  area_italy(301339, "km^2"),
  area_philippines(300000, "km^2"),
  area_ecuador(276841, "km^2"),
  area_burkinafaso(274222, "km^2"),
  area_newzealand(270467, "km^2"),
  area_gabon(267668, "km^2"),
  area_westernsahara(266000, "km^2"),
  area_guinea(245857, "km^2"),
  area_uganda(241550, "km^2"),
  area_ghana(238533, "km^2"),
  area_romania(238397, "km^2"),
  area_laos(236800, "km^2"),
  area_guyana(214969, "km^2"),
  area_belarus(207600, "km^2"),
  area_kyrgyzstan(199951, "km^2"),
  area_senegal(196722, "km^2"),
  area_syria(185180, "km^2"),
  area_golanheights(1150, "km^2"), // occupied by Israel; included i
  area_cambodia(181035, "km^2"),
  area_uruguay(176215, "km^2"),
  area_somaliland(176120, "km^2"),
  area_suriname(163820, "km^2"),
  area_tunisia(163610, "km^2"),
  area_bangladesh(147570, "km^2"),
  area_nepal(147181, "km^2"),
  area_tajikistan(143100, "km^2"),
  area_greece(131990, "km^2"),
  area_nicaragua(130373, "km^2"),
  area_northkorea(120540, "km^2"),
  area_malawi(118484, "km^2"),
  area_eritrea(117600, "km^2"),
  area_benin(114763, "km^2"),
  area_honduras(112492, "km^2"),
  area_liberia(111369, "km^2"),
  area_bulgaria(110879, "km^2"),
  area_cuba(109884, "km^2"),
  area_guatemala(108889, "km^2"),
  area_iceland(103000, "km^2"),
  area_southkorea(100210, "km^2"),
  area_hungary(93028, "km^2"),
  area_portugal(92090, "km^2"),
  area_jordan(89342, "km^2"),
  area_serbia(88361, "km^2"),
  area_azerbaijan(86600, "km^2"),
  area_austria(83871, "km^2"),
  area_uae(83600, "km^2"),
  area_czechia(78865, "km^2"),
  // ? area_czechrepublic         area_czechia
  area_panama(75417, "km^2"),
  area_sierraleone(71740, "km^2"),
  area_ireland(70273, "km^2"),
  area_georgia(69700, "km^2"),
  area_srilanka(65610, "km^2"),
  area_lithuania(65300, "km^2"),
  area_latvia(64559, "km^2"),
  area_togo(56785, "km^2"),
  area_croatia(56594, "km^2"),
  area_bosnia(51209, "km^2"),
  area_costarica(51100, "km^2"),
  area_slovakia(49037, "km^2"),
  area_dominicanrepublic(48671, "km^2"),
  area_estonia(45227, "km^2"),
  area_denmark(43094, "km^2"),
  area_netherlands(41850, "km^2"),
  area_switzerland(41284, "km^2"),
  area_bhutan(38394, "km^2"),
  area_taiwan(36193, "km^2"),
  area_guineabissau(36125, "km^2"),
  area_moldova(33846, "km^2"),
  area_belgium(30528, "km^2"),
  area_lesotho(30355, "km^2"),
  area_armenia(29743, "km^2"),
  area_solomonislands(28896, "km^2"),
  area_albania(28748, "km^2"),
  area_equitorialguinea(28051, "km^2"),
  area_burundi(27834, "km^2"),
  area_haiti(27750, "km^2"),
  area_rwanda(26338, "km^2"),
  area_northmacedonia(25713, "km^2"),
  area_djibouti(23200, "km^2"),
  area_belize(22966, "km^2"),
  area_elsalvador(21041, "km^2"),
  area_israel(20770, "km^2"),
  area_slovenia(20273, "km^2"),
  area_fiji(18272, "km^2"),
  area_kuwait(17818, "km^2"),
  area_eswatini(17364, "km^2"),
  area_easttimor(14919, "km^2"),
  area_bahamas(13943, "km^2"),
  area_montenegro(13812, "km^2"),
  area_vanatu(12189, "km^2"),
  area_qatar(11586, "km^2"),
  area_gambia(11295, "km^2"),
  area_jamaica(10991, "km^2"),
  area_kosovo(10887, "km^2"),
  area_lebanon(10452, "km^2"),
  area_cyprus(9251, "km^2"),
  area_puertorico(9104, "km^2"), // United States territory; not include
  area_westbank(5860, "km^2"), // (CIA World Factbook
  area_hongkong(2755, "km^2"),
  area_luxembourg(2586, "km^2"),
  area_singapore(716, "km^2"),
  area_gazastrip(360, "km^2"), // (CIA World Factbook
  area_malta(316, "km^2"), // smallest EU countr
  area_liechtenstein(160, "km^2"),
  area_monaco(2.02, "km^2"),
  area_vaticancity(0.44, "km^2"),
  // ? area_europeanunion        area_austria + area_belgium + area_bulgaria  <> + area_croatia +
  // area_cyprus + area_czechia + area_denmark  <> + area_estonia + area_finland + area_france +
  // area_germany  <> + area_greece + area_hungary + area_ireland + area_italy  <> + area_latvia +
  // area_lithuania + area_luxembourg  <> + area_malta + area_netherlands + area_poland  <> +
  // area_portugal + area_romania + area_slovakia  <> + area_slovenia + area_spain + area_sweden
  // ? area_eu                   area_europeanunion
  area_alaska(1723336.8, "km^2"),
  area_texas(695661.6, "km^2"),
  area_california(423967.4, "km^2"),
  area_montana(380831.1, "km^2"),
  area_newmexico(314917.4, "km^2"),
  area_arizona(295233.5, "km^2"),
  area_nevada(286379.7, "km^2"),
  area_colorado(269601.4, "km^2"),
  area_oregon(254799.2, "km^2"),
  area_wyoming(253334.5, "km^2"),
  area_michigan(250486.8, "km^2"),
  area_minnesota(225162.8, "km^2"),
  area_utah(219881.9, "km^2"),
  area_idaho(216442.6, "km^2"),
  area_kansas(213100.0, "km^2"),
  area_nebraska(200329.9, "km^2"),
  area_southdakota(199728.7, "km^2"),
  area_washington(184660.8, "km^2"),
  area_northdakota(183107.8, "km^2"),
  area_oklahoma(181037.2, "km^2"),
  area_missouri(180540.3, "km^2"),
  area_florida(170311.7, "km^2"),
  area_wisconsin(169634.8, "km^2"),
  area_georgia_us(153910.4, "km^2"),
  area_illinois(149995.4, "km^2"),
  area_iowa(145745.9, "km^2"),
  area_newyork(141296.7, "km^2"),
  area_northcarolina(139391.0, "km^2"),
  area_arkansas(137731.8, "km^2"),
  area_alabama(135767.4, "km^2"),
  area_louisiana(135658.7, "km^2"),
  area_mississippi(125437.7, "km^2"),
  area_pennsylvania(119280.2, "km^2"),
  area_ohio(116097.7, "km^2"),
  area_virginia(110786.6, "km^2"),
  area_tennessee(109153.1, "km^2"),
  area_kentucky(104655.7, "km^2"),
  area_indiana(94326.2, "km^2"),
  area_maine(91633.1, "km^2"),
  area_southcarolina(82932.7, "km^2"),
  area_westvirginia(62755.5, "km^2"),
  area_maryland(32131.2, "km^2"),
  area_hawaii(28313.0, "km^2"),
  area_massachusetts(27335.7, "km^2"),
  area_vermont(24906.3, "km^2"),
  area_newhampshire(24214.2, "km^2"),
  area_newjersey(22591.4, "km^2"),
  area_connecticut(14357.4, "km^2"),
  area_delaware(6445.8, "km^2"),
  area_rhodeisland(4001.2, "km^2"),
  area_districtofcolumbia(177.0, "km^2"),
  // ? area_unitedstates          area_alabama + area_alaska + area_arizona  <> + area_arkansas +
  // area_california + area_colorado  <> + area_connecticut + area_delaware  <> +
  // area_districtofcolumbia + area_florida  <> + area_georgia_us + area_hawaii + area_idaho  <> +
  // area_illinois + area_indiana + area_iowa  <> + area_kansas + area_kentucky + area_louisiana  <>
  // + area_maine + area_maryland + area_massachusetts  <> + area_michigan + area_minnesota +
  // area_mississippi  <> + area_missouri + area_montana + area_nebraska  <> + area_nevada +
  // area_newhampshire + area_newjersey  <> + area_newmexico + area_newyork + area_northcarolina  <>
  // + area_northdakota + area_ohio + area_oklahoma  <> + area_oregon + area_pennsylvania +
  // area_rhodeisland  <> + area_southcarolina + area_southdakota  <> + area_tennessee + area_texas
  // + area_utah  <> + area_vermont + area_virginia + area_washington  <> + area_westvirginia +
  // area_wisconsin + area_wyoming
  area_ontario(1076395, "km^2"), // confederated 1867-Jul-0
  area_quebec(1542056, "km^2"), // confederated 1867-Jul-0
  area_novascotia(55284, "km^2"), // confederated 1867-Jul-0
  area_newbrunswick(72908, "km^2"), // confederated 1867-Jul-0
  // ? area_canada_original            area_ontario + area_quebec + area_novascotia  <> +
  // area_newbrunswick
  area_manitoba(647797, "km^2"), // confederated 1870-Jul-1
  area_britishcolumbia(944735, "km^2"), // confederated 1871-Jul-2
  area_princeedwardisland(5660, "km^2"), // confederated 1873-Jul-0
  // ? area_canada_additional          area_manitoba + area_britishcolumbia  <> +
  // area_princeedwardisland
  area_alberta(661848, "km^2"), // confederated 1905-Sep-0
  area_saskatchewan(651036, "km^2"), // confederated 1905-Sep-0
  area_newfoundlandandlabrador(405212, "km^2"), // confederated 1949-Mar-3
  // ? area_canada_recent              area_alberta + area_saskatchewan  <> +
  // area_newfoundlandandlabrador
  // ? area_canada_provinces           area_canada_original + area_canada_additional  <> +
  // area_canada_recent
  area_northwestterritories(1346106, "km^2"), // NT confederated 1870-Jul-1
  area_yukon(482443, "km^2"), // YT confederated 1898-Jun-1
  area_nunavut(2093190, "km^2"), // NU confederated 1999-Apr-0
  // ? area_canada_territories         area_northwestterritories + area_yukon  <> + area_nunavut
  // ? area_canada                     area_canada_provinces + area_canada_territories
  area_england(132947.76, "km^2"),
  area_wales(21224.48, "km^2"),
  // ? area_englandwales       area_england + area_wales
  area_scotland(80226.36, "km^2"),
  // ? area_greatbritain       area_england + area_wales + area_scotland
  // ? area_gb                 area_greatbritain
  area_northernireland(14133.38, "km^2"),
  // ? area_unitedkingdom      area_greatbritain + area_northernireland
  // ? area_uk                 area_unitedkingdom
  // ## Units derived from imperial system
  // ? ouncedal                oz ft / s^2     # force which accelerates an ounce
  // ? poundal                 lb ft / s^2     # same thing for a pound
  // ? tondal                  longton ft / s^2    # and for a ton
  // ? pdl                     poundal
  // ? osi                     ounce force / inch^2   # used in aviation
  // ? psi                     pound force / inch^2
  // ? psia                    psi             # absolute pressure
  // ? tsi                     ton force / inch^2
  // ? reyn                    psi sec
  // ? slug                    lbf s^2 / ft
  // ? slugf                   slug force
  // ? slinch                  lbf s^2 / inch  # Mass unit derived from inch second
  // ? slinchf                 slinch force    #   pound-force system.  Used in space
  // ? geepound                slug
  // ? lbf                     lb force
  // ? tonf                    ton force
  // ? lbm                     lb
  kip(1000, "lbf"), // from kilopoun
  // ? ksi                     kip / in^2
  mil(0.001, "inch"),
  thou(0.001, "inch"),
  tenth(0.0001, "inch"), // one tenth of one thousandth of an inc
  millionth(1e-6, "inch"), // one millionth of an inc
  // : circularinch            1|4 pi in^2  # area of a one-inch diameter circle
  // ? circleinch              circularinch #    A circle with diameter d inches has
  // ? cylinderinch         circleinch inch # Cylinder h inch tall, d inches diameter
  // : circularmil             1|4 pi mil^2 # area of one-mil diameter circle
  // ? cmil                    circularmil
  cental(100, "pound"),
  // ? centner                 cental
  caliber(0.01, "inch"), // for measuring bullet
  // ? duty                    ft lbf
  // ? celo                    ft / s^2
  // ? jerk                    ft / s^3
  australiapoint(0.01, "inch"), // The "point" is used to measure rainfal
  // ? sabin                   ft^2         # Measure of sound absorption equal to the
  standardgauge(4, "ft + 8.5 in"), // Standard width between railroad trac
  flag(5, "ft^2"), // Construction term referring to sidewalk
  rollwallpaper(30, "ft^2"), // Area of roll of wall pape
  // ? fillpower              in^3 / ounce  # Density of down at standard pressure.
  // : pinlength              1|16 inch     # A #17 pin is 17/16 in long in the USA.
  // : buttonline             1|40 inch     # The line was used in 19th century USA
  // : beespace               1|4 inch      # Bees will fill any space that is smaller
  // : diamond                8|5 ft        # Marking on US tape measures that is
  retmaunit(1.75, "in"), // Height of rack mountable equipment
  // ? U                      retmaunit     #   Equipment should be 1|32 inch narrower
  // ^ RU                     U             #   than its U measurement indicates to
  // ? count                  per pound     # For measuring the size of shrimp
  flightlevel(100, "ft"), // Flight levels are used to ensure saf
  // ^ FL                     flightlevel   #   vertical separation between aircraft
  // ## Other units of work, energy, power, etc
  // ^ ENERGY                  joule
  // ^ WORK                    joule
  // ## Calorie: approximate energy to raise a gram of water one degree celsius
  // ? calorie                 cal_th       # Default is the thermochemical calorie
  // ? cal                     calorie
  calorie_th(4.184, "J"), // Thermochemical calorie, defined in 193
  // ? thermcalorie            calorie_th   #   by Frederick Rossini as 4.1833 J to
  // ? cal_th                  calorie_th   #   avoid difficulties associated with the
  calorie_IT(4.1868, "J"), // International (Steam) Table calorie
  // ? cal_IT                  calorie_IT   #   defined in 1929 as watt-hour/860 or
  calorie_15(4.18580, "J"), // Energy to go from 14.5 to 15.5 deg
  // ? cal_15                  calorie_15
  // ? calorie_fifteen         cal_15
  calorie_20(4.18190, "J"), // Energy to go from 19.5 to 20.5 deg
  // ? cal_20                  calorie_20
  // ? calorie_twenty          calorie_20
  calorie_4(4.204, "J"), // Energy to go from 3.5 to 4.5 deg
  // ? cal_4                   calorie_4
  // ? calorie_four            calorie_4
  cal_mean(4.19002, "J"), // 1|100 energy to go from 0 to 100 deg
  // ? Calorie                 kilocalorie  # the food Calorie
  thermie(1e6, "cal_15"), // Heat required to raise th
  // ## btu definitions: energy to raise a pound of water 1 degF
  // ? btu                     btu_IT       # International Table BTU is the default
  // ? britishthermalunit      btu
  // ? btu_IT                  cal_IT lb degF / gram K
  // ? btu_th                  cal_th lb degF / gram K
  // ? btu_mean                cal_mean lb degF / gram K
  // ? btu_15                  cal_15 lb degF / gram K
  btu_ISO(1055.06, "J"), // Exact, rounded ISO definition base
  // ? quad                    quadrillion btu
  ECtherm(1e5, "btu_ISO"), // Exact definitio
  UStherm(1.054804e8, "J"), // Exact definition
  // ? therm                   UStherm
  // ## Water latent heat from [23]
  water_fusion_heat(6.01, "kJ/mol / (18.015 g/mol)"), // At 0 deg
  water_vaporization_heat(2256.4, "J/g"), // At saturation, 100 deg C, 101.42 kP
  // ## Specific heat capacities of various substances
  // ? specificheat_water      calorie / g K
  // ? water_specificheat      specificheat_water
  specificheat_aluminum(0.91, "J/g K"),
  specificheat_antimony(0.21, "J/g K"),
  specificheat_barium(0.20, "J/g K"),
  specificheat_beryllium(1.83, "J/g K"),
  specificheat_bismuth(0.13, "J/g K"),
  specificheat_cadmium(0.23, "J/g K"),
  specificheat_cesium(0.24, "J/g K"),
  specificheat_chromium(0.46, "J/g K"),
  specificheat_cobalt(0.42, "J/g K"),
  specificheat_copper(0.39, "J/g K"),
  specificheat_gallium(0.37, "J/g K"),
  specificheat_germanium(0.32, "J/g K"),
  specificheat_gold(0.13, "J/g K"),
  specificheat_hafnium(0.14, "J/g K"),
  specificheat_indium(0.24, "J/g K"),
  specificheat_iridium(0.13, "J/g K"),
  specificheat_iron(0.45, "J/g K"),
  specificheat_lanthanum(0.195, "J/g K"),
  specificheat_lead(0.13, "J/g K"),
  specificheat_lithium(3.57, "J/g K"),
  specificheat_lutetium(0.15, "J/g K"),
  specificheat_magnesium(1.05, "J/g K"),
  specificheat_manganese(0.48, "J/g K"),
  specificheat_mercury(0.14, "J/g K"),
  specificheat_molybdenum(0.25, "J/g K"),
  specificheat_nickel(0.44, "J/g K"),
  specificheat_osmium(0.13, "J/g K"),
  specificheat_palladium(0.24, "J/g K"),
  specificheat_platinum(0.13, "J/g K"),
  specificheat_plutonum(0.13, "J/g K"),
  specificheat_potassium(0.75, "J/g K"),
  specificheat_rhenium(0.14, "J/g K"),
  specificheat_rhodium(0.24, "J/g K"),
  specificheat_rubidium(0.36, "J/g K"),
  specificheat_ruthenium(0.24, "J/g K"),
  specificheat_scandium(0.57, "J/g K"),
  specificheat_selenium(0.32, "J/g K"),
  specificheat_silicon(0.71, "J/g K"),
  specificheat_silver(0.23, "J/g K"),
  specificheat_sodium(1.21, "J/g K"),
  specificheat_strontium(0.30, "J/g K"),
  specificheat_tantalum(0.14, "J/g K"),
  specificheat_thallium(0.13, "J/g K"),
  specificheat_thorium(0.13, "J/g K"),
  specificheat_tin(0.21, "J/g K"),
  specificheat_titanium(0.54, "J/g K"),
  specificheat_tungsten(0.13, "J/g K"),
  specificheat_uranium(0.12, "J/g K"),
  specificheat_vanadium(0.39, "J/g K"),
  specificheat_yttrium(0.30, "J/g K"),
  specificheat_zinc(0.39, "J/g K"),
  specificheat_zirconium(0.27, "J/g K"),
  specificheat_ethanol(2.3, "J/g K"),
  specificheat_ammonia(4.6, "J/g K"),
  specificheat_freon(0.91, "J/g K"), // R-12 at 0 degrees Fahrenhei
  specificheat_gasoline(2.22, "J/g K"),
  specificheat_iodine(2.15, "J/g K"),
  specificheat_oliveoil(1.97, "J/g K"),
  specificheat_hydrogen(14.3, "J/g K"),
  specificheat_helium(5.1932, "J/g K"),
  specificheat_argon(0.5203, "J/g K"),
  specificheat_tissue(3.5, "J/g K"),
  specificheat_diamond(0.5091, "J/g K"),
  specificheat_granite(0.79, "J/g K"),
  specificheat_graphite(0.71, "J/g K"),
  specificheat_ice(2.11, "J/g K"),
  specificheat_asphalt(0.92, "J/g K"),
  specificheat_brick(0.84, "J/g K"),
  specificheat_concrete(0.88, "J/g K"),
  specificheat_glass_silica(0.84, "J/g K"),
  specificheat_glass_flint(0.503, "J/g K"),
  specificheat_glass_pyrex(0.753, "J/g K"),
  specificheat_gypsum(1.09, "J/g K"),
  specificheat_marble(0.88, "J/g K"),
  specificheat_sand(0.835, "J/g K"),
  specificheat_soil(0.835, "J/g K"),
  specificheat_wood(1.7, "J/g K"),
  specificheat_sucrose(1.244, "J/g K"), // www.sugartech.co.za/heatcapacity/index.ph
  tonoil(1e10, "cal_IT"), // Ton oil equivalent. A conventiona
  // ? toe                     tonoil         # burning one metric ton of oil. [18,E2]
  toncoal(7e9, "cal_IT"), // Energy in metric ton coal from [18]
  barreloil(5.8, "Mbtu"), // Conventional value for barrel of crud
  naturalgas_HHV(1027, "btu/ft3"), // Energy content of natural gas. HH
  naturalgas_LHV(930, "btu/ft3"), // is for Higher Heating Value an
  // ? naturalgas              naturalgas_HHV # includes energy from condensation
  charcoal(30, "GJ/tonne"),
  woodenergy_dry(20, "GJ/tonne"), // HHV, a cord weights about a tonn
  woodenergy_airdry(15, "GJ/tonne"), // 20% moisture conten
  coal_bituminous(27, "GJ / tonne"),
  coal_lignite(15, "GJ / tonne"),
  coal_US(22, "GJ / uston"), // Average for US coal (short ton), 199
  ethanol_HHV(84000, "btu/usgallon"),
  ethanol_LHV(75700, "btu/usgallon"),
  diesel(130500, "btu/usgallon"),
  gasoline_LHV(115000, "btu/usgallon"),
  gasoline_HHV(125000, "btu/usgallon"),
  // ? gasoline                gasoline_HHV
  heating(37.3, "MJ/liter"),
  fueloil(39.7, "MJ/liter"), // low sulphu
  propane(93.3, "MJ/m^3"),
  butane(124, "MJ/m^3"),
  uranium_pure(200, "MeV avogadro / (235.0439299 g/mol)"), // Pure U-23
  // ? uranium_natural         0.7% uranium_pure        # Natural uranium: 0.7% U-235
  // ## Celsius heat unit: energy to raise a pound of water 1 degC
  // ? celsiusheatunit         cal lb degC / gram K
  // ? chu                     celsiusheatunit
  // ^ POWER                   watt
  // ^ VA                      volt ampere
  // ? kWh                     kilowatt hour
  horsepower(550, "foot pound force / sec"), // Invented by James Wat
  // ? mechanicalhorsepower    horsepower
  // ? hp                      horsepower
  metrichorsepower(75, "kilogram force meter / sec"), // PS=Pferdestaerke i
  electrichorsepower(746, "W"), // German
  boilerhorsepower(9809.50, "W"),
  waterhorsepower(746.043, "W"),
  // ? brhorsepower            horsepower   # Value corrected Dec, 2019.  Was 745.7 W.
  donkeypower(250, "W"),
  // ? chevalvapeur            metrichorsepower
  // ^ THERMAL_CONDUCTIVITY    POWER / AREA (TEMPERATURE_DIFFERENCE/LENGTH)
  // ^ THERMAL_RESISTIVITY     1/THERMAL_CONDUCTIVITY
  // ^ THERMAL_CONDUCTANCE     POWER / TEMPERATURE_DIFFERENCE
  // ^ THERMAL_RESISTANCE      1/THERMAL_CONDUCTANCE
  // ^ THERMAL_ADMITTANCE      THERMAL_CONDUCTIVITY / LENGTH
  // ^ THERMAL_INSULANCE       THERMAL_RESISTIVITY LENGTH
  // ^ THERMAL_INSULATION      THERMAL_RESISTIVITY LENGTH
  // ? Rvalue                  degF ft^2 hr / btu
  // ? Uvalue                  1/Rvalue
  // ? europeanUvalue          watt / m^2 K
  // ^ RSI                     degC m^2 / W
  clo(0.155, "degC m^2 / W"), // Supposed to be the insulanc
  tog(0.1, "degC m^2 / W"), // Also used for clothing
  // ## Thermal Conductivity of a few materials
  diamond_natural_thermal_conductivity(2200, "W / m K"),
  diamond_synthetic_thermal_conductivity(3320, "W / m K"), // 99% pure C1
  silver_thermal_conductivity(406, "W / m K"),
  aluminum_thermal_conductivity(205, "W / m K"),
  copper_thermal_conductivity(385, "W / m K"),
  gold_thermal_conductivity(314, "W / m K"),
  iron_thermal_conductivity(79.5, "W / m K"),
  stainless_304_thermal_conductivity(15.5, "W / m K"), // average valu
  // () bel(x)     units=[1;1] range=(0,) 10^(x);    log(bel)    # Basic bel definition
  // () decibel(x) units=[1;1] range=(0,) 10^(x/10); 10 log(decibel) # Basic decibel
  // () dB()       decibel                                           # Abbreviation
  // () dBW(x)     units=[1;W] range=(0,) dB(x) W ;  ~dB(dBW/W)      # Reference = 1 W
  // () dBk(x)     units=[1;W] range=(0,) dB(x) kW ; ~dB(dBk/kW)     # Reference = 1 kW
  // () dBf(x)     units=[1;W] range=(0,) dB(x) fW ; ~dB(dBf/fW)     # Reference = 1 fW
  // () dBm(x)     units=[1;W] range=(0,) dB(x) mW ; ~dB(dBm/mW)     # Reference = 1 mW
  // () dBmW(x)    units=[1;W] range=(0,) dBm(x) ;   ~dBm(dBmW)      # Reference = 1 mW
  // () dBJ(x)     units=[1;J] range=(0,) dB(x) J; ~dB(dBJ/J)        # Energy relative
  // () dBV(x)  units=[1;V] range=(0,) dB(0.5 x) V;~dB(dBV^2 / V^2) # Reference = 1 V
  // () dBmV(x) units=[1;V] range=(0,) dB(0.5 x) mV;~dB(dBmV^2/mV^2)# Reference = 1 mV
  // () dBuV(x) units=[1;V] range=(0,) dB(0.5 x) microV ; ~dB(dBuV^2 / microV^2)
  // () dBA(x)  units=[1;A] range=(0,) dB(0.5 x) A;~dB(dBA^2 / A^2) # Reference = 1 A
  // () dBmA(x) units=[1;A] range=(0,) dB(0.5 x) mA;~dB(dBmA^2/mA^2)# Reference = 1 mA
  // () dBuA(x) units=[1;A] range=(0,) dB(0.5 x) microA ; ~dB(dBuA^2 / microA^2)
  // () dBu(x) units=[1;V] range=(0,) dB(0.5 x) sqrt(mW 600 ohm) ;  <> ~dB(dBu^2 / mW 600 ohm)
  // () dBv(x) units=[1;V] range=(0,) dBu(x) ; ~dBu(dBv)  # Synonym for dBu
  // () dBSPL(x) units=[1;Pa] range=(0,) dB(0.5 x) 20 microPa ;   <> ~dB(dBSPL^2 / (20 microPa)^2) #
  // pressure
  // () dBSIL(x) units=[1;W/m^2] range=(0,) dB(x) 1e-12 W/m^2;  <> ~dB(dBSIL / (1e-12 W/m^2)) #
  // intensity
  // () dBSWL(x) units=[1;W] range=(0,) dB(x) 1e-12 W; ~dB(dBSWL/1e-12 W)
  // ## Misc other measures
  // ^ ENTROPY                 ENERGY / TEMPERATURE
  clausius(1e3, "cal/K"), // A unit of physical entrop
  // ? langley                 thermcalorie/cm^2    # Used in radiation theory
  poncelet(100, "kg force m / s"),
  // ? tonrefrigeration        uston 144 btu / lb day # One ton refrigeration is
  // ? tonref                  tonrefrigeration
  // ? refrigeration           tonref / ton
  frigorie(1000, "cal_15"), // Used in refrigeration engineering
  tnt(1e9, "cal_th / ton"), // So you can write tons tnt. Thi
  airwatt(8.5, "(ft^3/min) inH2O"), // Measure of vacuum power a
  // ## Nuclear weapon yields
  davycrocket(10, "ton tnt"), // lightest US tactical nuclear weapo
  hiroshima(15.5, "kiloton tnt"), // Uranium-235 fission bom
  nagasaki(21, "kiloton tnt"), // Plutonium-239 fission bom
  // ? fatman                  nagasaki
  // ? littleboy               hiroshima
  ivyking(500, "kiloton tnt"), // most powerful fission bom
  castlebravo(15, "megaton tnt"), // most powerful US tes
  tsarbomba(50, "megaton tnt"), // most powerful test ever: USSR
  b53bomb(9, "megaton tnt"),
  trinity(18, "kiloton tnt"), // July 16, 194
  // ? gadget                  trinity
  // ? perm_0C                 grain / hr ft^2 inHg
  // ? perm_zero               perm_0C
  // ? perm_0                  perm_0C
  // ? perm                    perm_0C
  // ? perm_23C                grain / hr ft^2 in Hg23C
  // ? perm_twentythree        perm_23C
  // ## Counting measures
  pair(2, ""),
  brace(2, ""),
  nest(3, ""), // often used for items like bowls tha
  hattrick(3, ""), // Used in sports, especially cricket and ic
  dicker(10, ""),
  dozen(12, ""),
  bakersdozen(13, ""),
  score(20, ""),
  flock(40, ""),
  timer(40, ""),
  shock(60, ""),
  toncount(100, ""), // Used in sports in the U
  longhundred(120, ""), // From a germanic counting syste
  gross(144, ""),
  greatgross(12, "gross"),
  // : tithe                   1|10  # From Anglo-Saxon word for tenth
  // ## Paper counting measure
  shortquire(24, ""),
  quire(25, ""),
  shortream(480, ""),
  ream(500, ""),
  perfectream(516, ""),
  bundle(2, "reams"),
  bale(5, "bundles"),
  // ## Paper measures
  // ## USA paper sizes
  lettersize(8.5, "inch 11 inch"),
  legalsize(8.5, "inch 14 inch"),
  ledgersize(11, "inch 17 inch"),
  executivesize(7.25, "inch 10.5 inch"),
  Apaper(8.5, "inch 11 inch"),
  Bpaper(11, "inch 17 inch"),
  Cpaper(17, "inch 22 inch"),
  Dpaper(22, "inch 34 inch"),
  Epaper(34, "inch 44 inch"),
  envelope6_25size(3.5, "inch 6 inch"),
  envelope6_75size(3.625, "inch 6.5 inch"),
  envelope7size(3.75, "inch 6.75 inch"),
  envelope7_75size(3.875, "inch 7.5 inch"),
  envelope8_625size(3.625, "inch 8.625 inch"),
  envelope9size(3.875, "inch 8.875 inch"),
  envelope10size(4.125, "inch 9.5 inch"),
  envelope11size(4.5, "inch 10.375 inch"),
  envelope12size(4.75, "inch 11 inch"),
  envelope14size(5, "inch 11.5 inch"),
  envelope16size(6, "inch 12 inch"),
  // ## Announcement envelope sizes (no relation to metric paper sizes like A4)
  envelopeA1size(3.625, "inch 5.125 inch"), // same as 4ba
  envelopeA2size(4.375, "inch 5.75 inch"),
  envelopeA6size(4.75, "inch 6.5 inch"),
  envelopeA7size(5.25, "inch 7.25 inch"),
  envelopeA8size(5.5, "inch 8.125 inch"),
  envelopeA9size(5.75, "inch 8.75 inch"),
  envelopeA10size(6, "inch 9.5 inch"),
  // ## Baronial envelopes
  envelope4bar(3.625, "inch 5.125 inch"), // same as A
  envelope5_5bar(4.375, "inch 5.75 inch"),
  envelope6bar(4.75, "inch 6.5 inch"),
  // ## Coin envelopes
  envelope1baby(2.25, "inch 3.5 inch"), // same as #1 coi
  envelope00coin(1.6875, "inch 2.75 inch"),
  envelope1coin(2.25, "inch 3.5 inch"),
  envelope3coin(2.5, "inch 4.25 inch"),
  envelope4coin(3, "inch 4.5 inch"),
  envelope4_5coin(3, "inch 4.875 inch"),
  envelope5coin(2.875, "inch 5.25 inch"),
  envelope5_5coin(3.125, "inch 5.5 inch"),
  envelope6coin(3.375, "inch 6 inch"),
  envelope7coin(3.5, "inch 6.5 inch"),
  A0paper(841, "mm 1189 mm"), // The basic size in the A serie
  A1paper(594, "mm 841 mm"), // is defined to have an area o
  A2paper(420, "mm 594 mm"), // one square meter
  A3paper(297, "mm 420 mm"),
  A4paper(210, "mm 297 mm"),
  A5paper(148, "mm 210 mm"),
  A6paper(105, "mm 148 mm"),
  A7paper(74, "mm 105 mm"),
  A8paper(52, "mm 74 mm"),
  A9paper(37, "mm 52 mm"),
  A10paper(26, "mm 37 mm"),
  B0paper(1000, "mm 1414 mm"), // The basic B size has an are
  B1paper(707, "mm 1000 mm"), // of sqrt(2) square meters
  B2paper(500, "mm 707 mm"),
  B3paper(353, "mm 500 mm"),
  B4paper(250, "mm 353 mm"),
  B5paper(176, "mm 250 mm"),
  B6paper(125, "mm 176 mm"),
  B7paper(88, "mm 125 mm"),
  B8paper(62, "mm 88 mm"),
  B9paper(44, "mm 62 mm"),
  B10paper(31, "mm 44 mm"),
  C0paper(917, "mm 1297 mm"), // The basic C size has an are
  C1paper(648, "mm 917 mm"), // of sqrt(sqrt(2)) square meters
  C2paper(458, "mm 648 mm"),
  C3paper(324, "mm 458 mm"), // Intended for envelope size
  C4paper(229, "mm 324 mm"),
  C5paper(162, "mm 229 mm"),
  C6paper(114, "mm 162 mm"),
  C7paper(81, "mm 114 mm"),
  C8paper(57, "mm 81 mm"),
  C9paper(40, "mm 57 mm"),
  C10paper(28, "mm 40 mm"),
  // ## gsm (Grams per Square Meter), a sane, metric paper weight measure
  // ? gsm                     grams / meter^2
  // ? poundbookpaper          lb / 25 inch 38 inch ream
  // ? lbbook                  poundbookpaper
  // ? poundtextpaper          poundbookpaper
  // ? lbtext                  poundtextpaper
  // ? poundoffsetpaper        poundbookpaper    # For offset printing
  // ? lboffset                poundoffsetpaper
  // ? poundbiblepaper         poundbookpaper    # Designed to be lightweight, thin,
  // ? lbbible                 poundbiblepaper   # strong and opaque.
  // ? poundtagpaper           lb / 24 inch 36 inch ream
  // ? lbtag                   poundtagpaper
  // ? poundbagpaper           poundtagpaper
  // ? lbbag                   poundbagpaper
  // ? poundnewsprintpaper     poundtagpaper
  // ? lbnewsprint             poundnewsprintpaper
  // ? poundposterpaper        poundtagpaper
  // ? lbposter                poundposterpaper
  // ? poundtissuepaper        poundtagpaper
  // ? lbtissue                poundtissuepaper
  // ? poundwrappingpaper      poundtagpaper
  // ? lbwrapping              poundwrappingpaper
  // ? poundwaxingpaper        poundtagpaper
  // ? lbwaxing                poundwaxingpaper
  // ? poundglassinepaper      poundtagpaper
  // ? lbglassine              poundglassinepaper
  // ? poundcoverpaper         lb / 20 inch 26 inch ream
  // ? lbcover                 poundcoverpaper
  // ? poundindexpaper         lb / 25.5 inch 30.5 inch ream
  // ? lbindex                 poundindexpaper
  // ? poundindexbristolpaper  poundindexpaper
  // ? lbindexbristol          poundindexpaper
  // ? poundbondpaper          lb / 17 inch 22 inch ream  # Bond paper is stiff and
  // ? lbbond                  poundbondpaper             # durable for repeated
  // ? poundwritingpaper       poundbondpaper             # filing, and it resists
  // ? lbwriting               poundwritingpaper          # ink penetration.
  // ? poundledgerpaper        poundbondpaper
  // ? lbledger                poundledgerpaper
  // ? poundcopypaper          poundbondpaper
  // ? lbcopy                  poundcopypaper
  // ? poundblottingpaper      lb / 19 inch 24 inch ream
  // ? lbblotting              poundblottingpaper
  // ? poundblankspaper        lb / 22 inch 28 inch ream
  // ? lbblanks                poundblankspaper
  // ? poundpostcardpaper      lb / 22.5 inch 28.5 inch ream
  // ? lbpostcard              poundpostcardpaper
  // ? poundweddingbristol     poundpostcardpaper
  // ? lbweddingbristol        poundweddingbristol
  // ? poundbristolpaper       poundweddingbristol
  // ? lbbristol               poundbristolpaper
  // ? poundboxboard           lb / 1000 ft^2
  // ? lbboxboard              poundboxboard
  // ? poundpaperboard         poundboxboard
  // ? lbpaperboard            poundpaperboard
  // ? paperM                  lb / 1000
  pointthickness(0.001, "in"),
  paperdensity(0.8, "g/cm^3"), // approximate--paper densities vary
  // ? papercaliper            in paperdensity
  // ? paperpoint              pointthickness paperdensity
  // ## Printing
  fournierpoint(0.1648, "inch / 12"), // First definition of the printer
  /// *olddidotpoint           1|72 frenchinch   # Fran
  // : bertholdpoint           1|2660 m          # H. Berthold tried to create a
  INpoint(0.4, "mm"), // This point was created by
  germandidotpoint(0.376065, "mm"), // Exact definition appears in DI
  // : metricpoint             3|8 mm            # Proposed in 1977 by Eurograf
  // : oldpoint                1|72.27 inch      # The American point was invented
  // ? printerspoint           oldpoint          # by Nelson Hawks in 1879 and
  // ? texpoint                oldpoint          # dominates USA publishing.
  // : texscaledpoint          1|65536 texpoint  # The TeX typesetting system uses
  // ? texsp                   texscaledpoint    # this for all computations.
  // : computerpoint           1|72 inch         # The American point was rounded
  // ? point                   computerpoint
  computerpica(12, "computerpoint"), // to an even 1|72 inch by compute
  // ? postscriptpoint         computerpoint     # people at some point.
  // ? pspoint                 postscriptpoint
  // : twip                    1|20 point        # TWentieth of an Imperial Point
  // : Q                       1|4 mm            # Used in Japanese phototypesetting
  // ? frenchprinterspoint     olddidotpoint
  // ? didotpoint              germandidotpoint  # This seems to be the dominant value
  // ? europeanpoint           didotpoint        # for the point used in Europe
  cicero(12, "didotpoint"),
  stick(2, "inches"),
  // ## Type sizes
  excelsior(3, "oldpoint"),
  brilliant(3.5, "oldpoint"),
  diamondtype(4, "oldpoint"),
  pearl(5, "oldpoint"),
  agate(5.5, "oldpoint"), // Originally agate type was 14 lines pe
  // ? ruby                    agate         # British
  nonpareil(6, "oldpoint"),
  mignonette(6.5, "oldpoint"),
  // ? emerald                 mignonette    # British
  minion(7, "oldpoint"),
  brevier(8, "oldpoint"),
  bourgeois(9, "oldpoint"),
  longprimer(10, "oldpoint"),
  smallpica(11, "oldpoint"),
  pica(12, "oldpoint"),
  english(14, "oldpoint"),
  columbian(16, "oldpoint"),
  greatprimer(18, "oldpoint"),
  paragon(20, "oldpoint"),
  meridian(44, "oldpoint"),
  canon(48, "oldpoint"),
  // ## German type sizes
  nonplusultra(2, "didotpoint"),
  brillant(3, "didotpoint"),
  diamant(4, "didotpoint"),
  perl(5, "didotpoint"),
  nonpareille(6, "didotpoint"),
  kolonel(7, "didotpoint"),
  petit(8, "didotpoint"),
  borgis(9, "didotpoint"),
  korpus(10, "didotpoint"),
  // ? corpus                  korpus
  // ? garamond                korpus
  mittel(14, "didotpoint"),
  tertia(16, "didotpoint"),
  text(18, "didotpoint"),
  kleine_kanon(32, "didotpoint"),
  kanon(36, "didotpoint"),
  grobe_kanon(42, "didotpoint"),
  missal(48, "didotpoint"),
  kleine_sabon(72, "didotpoint"),
  grobe_sabon(84, "didotpoint"),
  // ^ INFORMATION             bit
  // ? nat                     (1/ln(2)) bits       # Entropy measured base e
  // ? hartley                 log2(10) bits        # Entropy of a uniformly
  // ? ban                     hartley              #   distributed random variable
  // ? dit                     hartley              # from Decimal digIT
  // ## Computer
  // ? bps                     bit/sec              # Sometimes the term "baud" is
  // . byte(8, "bit"), // Not all machines had 8 bi
  // ? B                       byte                 #   bytes, but these days most of
  octet(8, "bits"), // The octet is always 8 bit
  nybble(4, "bits"), // Half of a byte. Sometime
  // ? nibble                  nybble
  nyp(2, "bits"), // Donald Knuth asks in an exercis
  // ? meg                     megabyte             # Some people consider these
  // ? gig                     gigabyte             # to be defined according to
  jiffy(0.01, "sec"), // This is defined in the Jargon Fil
  // ? jiffies                 jiffy        # (http://www.jargon.org) as being the
  cdaudiospeed(44.1, "kHz 2*16 bits"), // CD audio data rate at 44.1 kHz with
  cdromspeed(75, "2048 bytes / sec"), // For data CDs (mode1) 75 sectors are rea
  dvdspeed(1385, "kB/s"), // This is the "1x" speed of a DVD usin
  // ^ FIT         / 1e9 hour # Failures In Time, number of failures per billion hours
  // () ipv4subnetsize(prefix_len) units=[1;1]  domain=[0,32] range=[1,4294967296]  <>
  // 2^(32-prefix_len) ; 32-log2(ipv4subnetsize)
  // ? ipv4classA               ipv4subnetsize(8)
  // ? ipv4classB               ipv4subnetsize(16)
  // ? ipv4classC               ipv4subnetsize(24)
  // () ipv6subnetsize(prefix_len) units=[1;1] domain=[0,128]  <>
  // range=[1,340282366920938463463374607431768211456]  <> 2^(128-prefix_len) ;
  // 128-log2(ipv6subnetsize)
  // ## Perfect intervals
  octave(2, ""),
  // ? majorsecond             musicalfifth^2 / octave
  // : majorthird              5|4
  // : minorthird              6|5
  // : musicalfourth           4|3
  // : musicalfifth            3|2
  // ? majorsixth              musicalfourth majorthird
  // ? minorsixth              musicalfourth minorthird
  // ? majorseventh            musicalfifth majorthird
  // ? minorseventh            musicalfifth minorthird
  // ? pythagoreanthird        majorsecond musicalfifth^2 / octave
  // ? syntoniccomma           pythagoreanthird / majorthird
  // ? pythagoreancomma        musicalfifth^12 / octave^7
  // ## Equal tempered definitions
  // : semitone                octave^(1|12)
  // () musicalcent(x) units=[1;1] range=(0,) semitone^(x/100) ;  <> 100
  // log(musicalcent)/log(semitone)
  // ## Musical note lengths.
  // ? wholenote               !
  // ^ MUSICAL_NOTE_LENGTH     wholenote
  // : halfnote                1|2 wholenote
  // : quarternote             1|4 wholenote
  // : eighthnote              1|8 wholenote
  // : sixteenthnote           1|16 wholenote
  // : thirtysecondnote        1|32 wholenote
  // : sixtyfourthnote         1|64 wholenote
  // : dotted                  3|2
  // : doubledotted            7|4
  // ? breve                   doublewholenote
  // ? semibreve               wholenote
  // ? minimnote               halfnote
  // ? crotchet                quarternote
  // ? quaver                  eighthnote
  // ? semiquaver              sixteenthnote
  // ? demisemiquaver          thirtysecondnote
  // ? hemidemisemiquaver      sixtyfourthnote
  // ? semidemisemiquaver      hemidemisemiquaver
  // ## yarn and cloth measures
  // ## yarn linear density
  woolyarnrun(1600, "yard/pound"), // 1600 yds of "number 1 yarn" weigh
  yarncut(300, "yard/pound"), // Less common system used i
  cottonyarncount(840, "yard/pound"),
  linenyarncount(300, "yard/pound"), // Also used for hemp and rami
  worstedyarncount(1680, "ft/pound"),
  // ? metricyarncount         meter/gram
  // : denier                  1|9 tex            # used for silk and rayon
  // ? manchesteryarnnumber    drams/1000 yards   # old system used for silk
  // ? pli                     lb/in
  typp(1000, "yd/lb"), // abbreviation for Thousand Yard Per Poun
  asbestoscut(100, "yd/lb"), // used for glass and asbestos yar
  // ? tex                     gram / km    # rational metric yarn measure, meant
  drex(0.1, "tex"), // to be used for any kind of yar
  // ? poumar                  lb / 1e6 yard
  // ## yarn and cloth length
  // ? skeincotton             80*54 inch   # 80 turns of thread on a reel with a
  cottonbolt(120, "ft"), // cloth measuremen
  woolbolt(210, "ft"),
  // ? bolt                    cottonbolt
  heer(600, "yards"),
  cut(300, "yards"), // used for wet-spun linen yar
  lea(300, "yards"),
  sailmakersyard(28.5, "in"),
  // ? sailmakersounce         oz / sailmakersyard 36 inch
  // ? silkmomme               momme / 25 yards 1.49 inch  # Traditional silk weight
  // ? silkmm                  silkmomme        # But it is also defined as
  // ## drug dosage
  // ? mcg                     microgram        # Frequently used for vitamins
  iudiptheria(62.8, "microgram"), // IU is for international uni
  iupenicillin(0.6, "microgram"),
  iuinsulin(41.67, "microgram"),
  // : drop                    1|20 ml          # The drop was an old "unit" that was
  bloodunit(450, "ml"), // For whole blood. For bloo
  // ## misc medical measure
  // : frenchcathetersize      1|3 mm           # measure used for the outer diameter
  // ? charriere               frenchcathetersize
  // ## fixup units for times when prefix handling doesn't do the job
  // ? hectare                 hectoare
  // ? megohm                  megaohm
  // ? kilohm                  kiloohm
  // ? microhm                 microohm
  // ? megalerg                megaerg    # 'L' added to make it pronounceable [18].
  // ? unitedstatesdollar      US$
  // ? usdollar                US$
  // ? $                       dollar
  // ? mark                    germanymark
  // ? peseta                  spainpeseta
  // ? rand                    southafricarand
  // ? escudo                  portugalescudo
  // ? guilder                 netherlandsguilder
  // ? hollandguilder          netherlandsguilder
  // ? peso                    mexicopeso
  // ? yen                     japanyen
  // ? lira                    italylira
  // ? rupee                   indiarupee
  // ? drachma                 greecedrachma
  // ? franc                   francefranc
  // ? markka                  finlandmarkka
  // ? britainpound            unitedkingdompound
  // ? greatbritainpound       unitedkingdompound
  // ? unitedkingdompound      ukpound
  // ? poundsterling           britainpound
  // ? yuan                    chinayuan
  // ## Unicode Currency Names
  // !utf8
  // !endutf8
  // ^ UKP                     GBP        # Not an ISO code, but looks like one, and
  // !include currency.units
  olddollargold(23.22, "grains goldprice"), // Used until 193
  // : newdollargold           96|7 grains goldprice   # After Jan 31, 1934
  // ? dollargold              newdollargold
  poundgold(113, "grains goldprice"), // British poun
  // ## Precious metals
  // ? goldounce               goldprice troyounce
  // ? silverounce             silverprice troyounce
  // ? platinumounce           platinumprice troyounce
  // ^ XAU                     goldounce
  // ^ XPT                     platinumounce
  // ^ XAG                     silverounce
  USpennyweight(2.5, "grams"), // Since 1982, 48 grains befor
  USnickelweight(5, "grams"),
  // ? USdimeweight            US$ 0.10 / (20 US$ / lb)   # Since 1965
  // ? USquarterweight         US$ 0.25 / (20 US$ / lb)   # Since 1965
  // ? UShalfdollarweight      US$ 0.50 / (20 US$ / lb)   # Since 1971
  USdollarweight(8.1, "grams"), // Weight of Susan B. Anthony an
  // ## British currency
  // ? quid                    britainpound        # Slang names
  fiver(5, "quid"),
  tenner(10, "quid"),
  monkey(500, "quid"),
  brgrand(1000, "quid"),
  // ? bob                     shilling
  // : shilling                1|20 britainpound   # Before decimalisation, there
  // : oldpence                1|12 shilling       # were 20 shillings to a pound,
  // : farthing                1|4 oldpence        # each of twelve old pence
  guinea(21, "shilling"), // Still used in horse racin
  crown(5, "shilling"),
  florin(2, "shilling"),
  groat(4, "oldpence"),
  tanner(6, "oldpence"),
  brpenny(0.01, "britainpound"),
  // ? pence                   brpenny
  tuppence(2, "pence"),
  // ? tuppenny                tuppence
  // ? ha'penny                halfbrpenny
  // ? hapenny                 ha'penny
  // ? oldpenny                oldpence
  oldtuppence(2, "oldpence"),
  // ? oldtuppenny             oldtuppence
  threepence(3, "oldpence"), // threepence never refers to new mone
  // ? threepenny              threepence
  // ? oldthreepence           threepence
  // ? oldthreepenny           threepence
  // ? oldhalfpenny            halfoldpenny
  // ? oldha'penny             oldhalfpenny
  // ? oldhapenny              oldha'penny
  brpony(25, "britainpound"),
  // ## Canadian currency
  loony(1, "canadadollar"), // This coin depicts a loo
  toony(2, "canadadollar"),
  // ## Cryptocurrency
  satoshi(1e-8, "bitcoin"),
  // ^ XBT                     bitcoin           # nonstandard code
  // ## Units used for measuring volume of wood
  // ? cord                    4*4*8 ft^3   # 4 ft by 4 ft by 8 ft bundle of wood
  // : facecord                1|2 cord
  // : cordfoot                1|8 cord     # One foot long section of a cord
  // ? cordfeet                cordfoot
  // : housecord               1|3 cord     # Used to sell firewood for residences,
  // ? boardfoot               ft^2 inch    # Usually 1 inch thick wood
  // ? boardfeet               boardfoot
  // ? fbm                     boardfoot    # feet board measure
  stack(4, "yard^3"), // British, used for firewood and coal [18
  rick(4, "ft 8 ft 16 inches"), // Stack of firewood, supposedl
  // ? stere                   m^3
  // ? timberfoot              ft^3         # Used for measuring solid blocks of wood
  standard(120, "12 ft 11 in 1.5 in"), // This is the St Petersburg o
  // ? hoppusfoot               (4/pi) ft^3 # Volume calculation suggested in 1736
  // : hoppusboardfoot      1|12 hoppusfoot #   forestry manual by Edward Hoppus, for
  hoppuston(50, "hoppusfoot"), // estimating the usable volume of a log
  deal(12, "ft 11 in 2.5 in"), // The standard North American deal [OED
  wholedeal(12, "ft 11 in 1.25 in"), // If it's half as thick as the standar
  splitdeal(12, "ft 11 in 5|8 in"), // And half again as thick is a split deal
  // ## Used for shellac mixing rate
  // ? poundcut            pound / gallon
  // ? lbcut               poundcut
  // ## Gas and Liquid flow units
  // ^ FLUID_FLOW              VOLUME / TIME
  // ## Some obvious volumetric gas flow units (cu is short for cubic)
  // ? cumec                   m^3/s
  // ? cusec                   ft^3/s
  // ## Conventional abbreviations for fluid flow units
  // ? gph                     gal/hr
  // ? gpm                     gal/min
  // ? mgd                     megagal/day
  // ? cfs                     ft^3/s
  // ? cfh                     ft^3/hour
  // ? cfm                     ft^3/min
  // ? lpm                     liter/min
  // ? lfm                     ft/min     # Used to report air flow produced by fans.
  // ? pru                     mmHg / (ml/min)  # peripheral resistance unit, used in
  minersinchAZ(1.5, "ft^3/min"),
  minersinchCA(1.5, "ft^3/min"),
  minersinchMT(1.5, "ft^3/min"),
  minersinchNV(1.5, "ft^3/min"),
  minersinchOR(1.5, "ft^3/min"),
  minersinchID(1.2, "ft^3/min"),
  minersinchKS(1.2, "ft^3/min"),
  minersinchNE(1.2, "ft^3/min"),
  minersinchNM(1.2, "ft^3/min"),
  minersinchND(1.2, "ft^3/min"),
  minersinchSD(1.2, "ft^3/min"),
  minersinchUT(1.2, "ft^3/min"),
  minersinchCO(1, "ft^3/sec / 38.4"), // 38.4 miner's inches = 1 ft^3/se
  minersinchBC(1.68, "ft^3/min"), // British Columbi
  // ## Oceanographic flow
  sverdrup(1e6, "m^3 / sec"), // Used to express flow of ocea
  // ^ GAS_FLOW                PRESSURE FLUID_FLOW
  // ? sccm                    atm cc/min     # 's' is for "standard" to indicate
  // ? sccs                    atm cc/sec     # flow at standard pressure
  // ? scfh                    atm ft^3/hour  #
  // ? scfm                    atm ft^3/min
  // ? slpm                    atm liter/min
  // ? slph                    atm liter/hour
  // ? lusec                   liter micron Hg / s  # Used in vacuum science
  // ## temperature lapse rate, -dT/dz, in troposphere
  lapserate(6.5, "K/km"), // US Std Atm (1976
  air_1976(
      78.084,
      "% 28.0134 <> + 20.9476 % 31.9988 <> + 9340 ppm 39.948 <> + 314 ppm 44.00995 <> + 18.18 ppm 20.183 <> + 5.24 ppm 4.0026 <> + 2 ppm 16.04303 <> + 1.14 ppm 83.80 <> + 0.55 ppm 2.01594 <> + 0.087 ppm 131.30"),
  R_1976(8.31432e3, "N m/(kmol K)"),
  // ? polyndx_1976    air_1976 (kg/kmol) gravity/(R_1976 lapserate) - 1
  // ## If desired, redefine using current values for air mol wt and R
  // ? polyndx         polyndx_1976
  // ## polyndx       air (kg/kmol) gravity/(R lapserate) - 1
  // ## for comparison with various references
  // ? polyexpnt       (polyndx + 1) / polyndx
  stdatmT0(288.15, "K"),
  // ? stdatmP0        atm
  earthradUSAtm(6356766, "m"),
  // () stdatmTH(h) units=[m;K] domain=[-5000,11e3] range=[217,321]  <> stdatmT0+(-lapserate h) ;
  // (stdatmT0+(-stdatmTH))/lapserate
  // () stdatmT(z) units=[m;K] domain=[-5000,11e3] range=[217,321]  <> stdatmTH(geop_ht(z)) ;
  // ~geop_ht(~stdatmTH(stdatmT))
  // () stdatmPH(h) units=[m;Pa] domain=[-5000,11e3] range=[22877,177764]  <> atm (1 -
  // (lapserate/stdatmT0) h)^(polyndx + 1) ;  <> (stdatmT0/lapserate)
  // (1+(-(stdatmPH/stdatmP0)^(1/(polyndx + 1))))
  // () stdatmP(z) units=[m;Pa] domain=[-5000,11e3] range=[22877,177764]  <> stdatmPH(geop_ht(z));
  // ~geop_ht(~stdatmPH(stdatmP))
  // () geop_ht(z) units=[m;m] domain=[-5000,) range=[-5004,)  <> (earthradUSAtm z) / (earthradUSAtm
  // + z) ;  <> (earthradUSAtm geop_ht) / (earthradUSAtm + (-geop_ht))
  // () g_phi(lat) units=[deg;m/s2] domain=[0,90] noerror   <> 980.6160e-2 (1+(-0.0026373) cos(2
  // lat)+0.0000059 cos(2 lat)^2) m/s2
  // () earthradius_eff(lat) units=[deg;m] domain=[0,90] noerror  <> m 2 9.780356 (1+0.0052885
  // sin(lat)^2+(-0.0000059) sin(2 lat)^2) /  <> (3.085462e-6 + 2.27e-9 cos(2 lat) + (-2e-12) cos(4
  // lat))
  // ? Patm    atm
  // () gaugepressure(x) units=[Pa;Pa] domain=[-101325,) range=[0,)  <> x + Patm ;
  // gaugepressure+(-Patm)
  // () psig(x) units=[1;Pa] domain=[-14.6959487755135,) range=[0,)  <> gaugepressure(x psi) ;
  // ~gaugepressure(psig) / psi
  // ## Pressure for underwater diving
  seawater(0.1, "bar / meter"),
  // ? msw                  meter seawater
  // ? fsw                  foot seawater
  // ? g00                      (-1)
  // ? g000                     (-2)
  // ? g0000                    (-3)
  // ? g00000                   (-4)
  // ? g000000                  (-5)
  // ? g0000000                 (-6)
  // () wiregauge(g) units=[1;m] range=(0,)  <> 1|200 92^((36+(-g))/39) in; 36+(-39)ln(200
  // wiregauge/in)/ln(92)
  // () awg()        wiregauge
  // [] brwiregauge[in]   <> -6 0.5     <> -5 0.464   <> -3 0.4     <> -2 0.372   <> 3 0.252   <> 6
  // 0.192   <> 10 0.128   <> 14 0.08    <> 19 0.04    <> 23 0.024   <> 26 0.018   <> 28 0.0148  <>
  // 30 0.0124  <> 39 0.0052  <> 49 0.0012  <> 50 0.001
  // ## Old plate gauge for iron
  // [] plategauge[(oz/ft^2)/(480*lb/ft^3)]  <> -5 300    <> 1 180    <> 14  50    <> 16  40    <>
  // 17  36    <> 20  24    <> 26  12    <> 31   7    <> 36   4.5  <> 38   4
  // ## Manufacturers Standard Gage
  // [] stdgauge[(oz/ft^2)/(501.84*lb/ft^3)]  <> -5 300    <> 1 180    <> 14  50    <> 16  40    <>
  // 17  36    <> 20  24    <> 26  12    <> 31   7    <> 36   4.5  <> 38   4
  // [] zincgauge[in]     <> 1 0.002   <> 10 0.02    <> 15 0.04    <> 19 0.06    <> 23 0.1     <> 24
  // 0.125   <> 27 0.5     <> 28 1
  // [] drillgauge[in]  <> 1  0.2280  <> 2  0.2210  <> 3  0.2130  <> 4  0.2090  <> 5  0.2055  <> 6
  // 0.2040  <> 7  0.2010  <> 8  0.1990  <> 9  0.1960  <> 10  0.1935  <> 11  0.1910  <> 12  0.1890
  // <> 13  0.1850  <> 14  0.1820  <> 15  0.1800  <> 16  0.1770  <> 17  0.1730  <> 18  0.1695  <> 19
  //  0.1660  <> 20  0.1610  <> 22  0.1570  <> 23  0.1540  <> 24  0.1520  <> 25  0.1495  <> 26
  // 0.1470  <> 27  0.1440  <> 28  0.1405  <> 29  0.1360  <> 30  0.1285  <> 31  0.1200  <> 32
  // 0.1160  <> 33  0.1130  <> 34  0.1110  <> 35  0.1100  <> 36  0.1065  <> 38  0.1015  <> 39
  // 0.0995  <> 40  0.0980  <> 41  0.0960  <> 42  0.0935  <> 43  0.0890  <> 44  0.0860  <> 45
  // 0.0820  <> 46  0.0810  <> 48  0.0760  <> 51  0.0670  <> 52  0.0635  <> 53  0.0595  <> 54
  // 0.0550  <> 55  0.0520  <> 56  0.0465  <> 57  0.0430  <> 65  0.0350  <> 66  0.0330  <> 68
  // 0.0310  <> 69  0.0292  <> 70  0.0280  <> 71  0.0260  <> 73  0.0240  <> 74  0.0225  <> 75
  // 0.0210  <> 76  0.0200  <> 78  0.0160  <> 79  0.0145  <> 80  0.0135  <> 88  0.0095  <> 104
  // 0.0031
  drillA(0.234, "in"),
  drillB(0.238, "in"),
  drillC(0.242, "in"),
  drillD(0.246, "in"),
  drillE(0.250, "in"),
  drillF(0.257, "in"),
  drillG(0.261, "in"),
  drillH(0.266, "in"),
  drillI(0.272, "in"),
  drillJ(0.277, "in"),
  drillK(0.281, "in"),
  drillL(0.290, "in"),
  drillM(0.295, "in"),
  drillN(0.302, "in"),
  drillO(0.316, "in"),
  drillP(0.323, "in"),
  drillQ(0.332, "in"),
  drillR(0.339, "in"),
  drillS(0.348, "in"),
  drillT(0.358, "in"),
  drillU(0.368, "in"),
  drillV(0.377, "in"),
  drillW(0.386, "in"),
  drillX(0.397, "in"),
  drillY(0.404, "in"),
  drillZ(0.413, "in"),
  // () screwgauge(g) units=[1;m] range=[0,)  <> (.06 + .013 g) in ; (screwgauge/in + (-.06)) / .013
  // [] grit_P[micron]  <> 12 1815  <> 16 1324  <> 20 1000  <> 24 764  <> 30 642  <> 36 538  <> 40
  // 425  <> 50 336  <> 60 269  <> 80 201  <> 100 162  <> 120 125  <> 150 100  <> 180 82  <> 220 68
  // <> 240 58.5  <> 280 52.2  <> 320 46.2  <> 360 40.5  <> 400 35  <> 500 30.2  <> 600 25.8  <> 800
  // 21.8  <> 1000 18.3  <> 1200 15.3  <> 1500 12.6  <> 2000 10.3  <> 2500 8.4
  // [] grit_F[micron]  <> 4 4890  <> 5 4125  <> 6 3460  <> 7 2900  <> 8 2460  <> 10 2085  <> 12
  // 1765  <> 14 1470  <> 16 1230  <> 20 1040  <> 22 885  <> 24 745  <> 30 625  <> 36 525  <> 40 438
  //  <> 46 370  <> 54 310  <> 60 260  <> 70 218  <> 80 185  <> 90 154  <> 100 129  <> 120 109  <>
  // 150 82  <> 180 69  <> 220 58  <> 230 53  <> 240 44.5  <> 280 36.5  <> 320 29.2  <> 360 22.8  <>
  // 400 17.3  <> 500 12.8  <> 600 9.3  <> 800 6.5  <> 1000 4.5  <> 1200 3  <> 1500 2.0  <> 2000 1.2
  // [] ansibonded[micron]  <> 4 4890  <> 5 4125  <> 6 3460  <> 7 2900  <> 8 2460  <> 10 2085  <> 12
  // 1765  <> 14 1470  <> 16 1230  <> 20 1040  <> 22 885  <> 24 745  <> 30 625  <> 36 525  <> 40 438
  //  <> 46 370  <> 54 310  <> 60 260  <> 70 218  <> 80 185  <> 90 154  <> 100 129  <> 120 109  <>
  // 150 82  <> 180 69  <> 220 58  <> 240 50  <> 280 39.5  <> 320 29.5  <> 360 23  <> 400 18.25  <>
  // 500 13.9  <> 600 10.55  <> 800 7.65  <> 1000 5.8  <> 1200 3.8
  // () grit_ansibonded() ansibonded
  // [] ansicoated[micron] noerror  <> 4 4890  <> 5 4125  <> 6 3460  <> 7 2900  <> 8 2460  <> 10
  // 2085  <> 12 1765  <> 14 1470  <> 16 1230  <> 20 1040  <> 22 885  <> 24 745  <> 30 625  <> 36
  // 525  <> 40 438  <> 46 370  <> 54 310  <> 60 260  <> 70 218  <> 80 185  <> 90 154  <> 100 129
  // <> 120 109  <> 150 82  <> 180 69  <> 220 58  <> 240 50  <> 280 39.5  <> 320 29.5  <> 360 23  <>
  // 400 18.25  <> 500 13.9  <> 600 10.55  <> 800 11.5  <> 1000 9.5  <> 2000 7.2  <> 2500 5.5  <>
  // 3000 4  <> 4000 3  <> 6000 2  <> 8000 1.2
  // () grit_ansicoated()  ansicoated
  // [] jisgrit[micron]  <> 150 75  <> 180 63  <> 220 53  <> 280 48  <> 320 40  <> 360 35  <> 400 30
  //  <> 600 20  <> 700 17  <> 800 14  <> 1000 11.5  <> 1200 9.5  <> 1500 8  <> 2000 6.7  <> 2500
  // 5.5  <> 3000 4  <> 4000 3  <> 6000 2  <> 8000 1.2
  // [] grit_A[micron] <> 16 15.3  <> 25 21.8  <> 30 23.6  <> 35 25.75  <> 45 35  <> 60 46.2  <> 65
  // 53.5  <> 75 58.5  <> 90 65  <> 110 78  <> 130 93  <> 160 127  <> 200 156
  dmtxxcoarse(120, "micron"), // 120 mes
  // ? dmtsilver    dmtxxcoarse
  // ? dmtxx        dmtxxcoarse
  dmtxcoarse(60, "micron"), // 220 mes
  // ? dmtx         dmtxcoarse
  // ? dmtblack     dmtxcoarse
  dmtcoarse(45, "micron"), // 325 mes
  // ? dmtc         dmtcoarse
  // ? dmtblue      dmtcoarse
  dmtfine(25, "micron"), // 600 mes
  // ? dmtred       dmtfine
  // ? dmtf         dmtfine
  dmtefine(9, "micron"), // 1200 mes
  // ? dmte         dmtefine
  // ? dmtgreen     dmtefine
  dmtceramic(7, "micron"), // 2200 mes
  // ? dmtcer       dmtceramic
  // ? dmtwhite     dmtceramic
  dmteefine(3, "micron"), // 8000 mes
  // ? dmttan       dmteefine
  // ? dmtee        dmteefine
  hardtranslucentarkansas(6, "micron"), // Natural novaculite (silicon quartz
  softarkansas(22, "micron"), // stone
  extrafineindia(22, "micron"), // India stones are Norton's manufacture
  fineindia(35, "micron"), // aluminum oxide produc
  mediumindia(53.5, "micron"),
  coarseindia(97, "micron"),
  finecrystolon(45, "micron"), // Crystolon stones are Norton'
  mediumcrystalon(78, "micron"), // manufactured silicon carbide produc
  coarsecrystalon(127, "micron"),
  hardblackarkansas(6, "micron"),
  hardwhitearkansas(11, "micron"),
  washita(35, "micron"),
  // [] meshtyler[micron]  <> 2.5 8000  <> 3   6727  <> 3.5 5657  <> 4   4757  <> 5   4000  <> 6
  // 3364  <> 7   2828  <> 8   2378  <> 9   2000  <> 10   1682  <> 12   1414  <> 14   1189  <> 16
  // 1000  <> 20    841  <> 24    707  <> 28    595  <> 32    500  <> 35    420  <> 42    354  <> 48
  //    297  <> 60    250  <> 65    210  <> 80    177  <> 100    149  <> 115    125  <> 150    105
  // <> 170     88  <> 200     74  <> 250     63  <> 270     53  <> 325     44  <> 400     37
  // [] sieve[micron]  <> 3.5   5600  <> 4     4750  <> 5     4000  <> 6     3350  <> 7     2800  <>
  // 8     2360  <> 10     2000  <> 12     1700  <> 14     1400  <> 16     1180  <> 18     1000  <>
  // 20      850  <> 25      710  <> 30      600  <> 35      500  <> 40      425  <> 45      355  <>
  // 50      300  <> 60      250  <> 70      212  <> 80      180  <> 100      150  <> 120      125
  // <> 140      106  <> 170       90  <> 200       75  <> 230       63  <> 270       53  <> 325
  //   45  <> 400       38  <> 450       32  <> 500       25  <> 625       20   # These last two
  // values are not in the standard series
  // () meshUS()  sieve        # were in common usage.
  // [] meshbritish[micron]  <> 3    5657  <> 3.5  4757  <> 4    4000  <> 5    3364  <> 6    2828
  // <> 7    2378  <> 8    2000  <> 10    1682  <> 12    1414  <> 14    1189  <> 16    1000  <> 18
  //   841  <> 22     707  <> 25     595  <> 30     500  <> 36     420  <> 44     354  <> 52     297
  //  <> 60     250  <> 72     210  <> 85     177  <> 100     149  <> 120     125  <> 150     105
  // <> 170      88  <> 200      74  <> 240      63  <> 300      53  <> 350      44  <> 400      37
  // [] meshtamis[micron]  <> 17   40  <> 18   50  <> 19   63  <> 20   80  <> 21  100  <> 22  125
  // <> 23  160  <> 24  200  <> 25  250  <> 26  315  <> 27  400  <> 28  500  <> 29  630  <> 30  800
  // <> 31 1000  <> 32 1250  <> 33 1600  <> 34 2000  <> 35 2500  <> 36 3150  <> 37 4000  <> 38 5000
  // ## Ring size. All ring sizes are given as the circumference of the ring.
  // () ringsize(n) units=[1;in] domain=[2,) range=[1.6252,)  <> (1.4216+.1018 n) in ; (ringsize/in
  // + (-1.4216))/.1018
  sizeAring(37.50, "mm"),
  sizeBring(38.75, "mm"),
  sizeCring(40.00, "mm"),
  sizeDring(41.25, "mm"),
  sizeEring(42.50, "mm"),
  sizeFring(43.75, "mm"),
  sizeGring(45.00, "mm"),
  sizeHring(46.25, "mm"),
  sizeIring(47.50, "mm"),
  sizeJring(48.75, "mm"),
  sizeKring(50.00, "mm"),
  sizeLring(51.25, "mm"),
  sizeMring(52.50, "mm"),
  sizeNring(53.75, "mm"),
  sizeOring(55.00, "mm"),
  sizePring(56.25, "mm"),
  sizeQring(57.50, "mm"),
  sizeRring(58.75, "mm"),
  sizeSring(60.00, "mm"),
  sizeTring(61.25, "mm"),
  sizeUring(62.50, "mm"),
  sizeVring(63.75, "mm"),
  sizeWring(65.00, "mm"),
  sizeXring(66.25, "mm"),
  sizeYring(67.50, "mm"),
  sizeZring(68.75, "mm"),
  // () jpringsize(n)  units=[1;mm] domain=[1,) range=[0.040840704,)  <> (38|3 + n/3) pi mm ; 3
  // jpringsize/ pi mm + (-38)
  // ## The European ring sizes are the length of the circumference in mm minus 40.
  // () euringsize(n)  units=[1;mm] (n+40) mm ; euringsize/mm + (-40)
  // ## Abbreviations
  // ? mph                     mile/hr
  // ? mpg                     mile/gal
  // ? kph                     km/hr
  // ? fL                      footlambert
  // ? fpm                     ft/min
  // ? fps                     ft/s
  // ? rpm                     rev/min
  // ? rps                     rev/sec
  // ? mi                      mile
  // ? smi                     mile
  // ? nmi                     nauticalmile
  mbh(1e3, "btu/hour"),
  mcm(1e3, "circularmil"),
  // ? ipy                     inch/year    # used for corrosion rates
  ccf(100, "ft^3"), // used for selling water [18
  Mcf(1000, "ft^3"), // not million cubic feet [18
  // ? kp                      kilopond
  // ? kpm                     kp meter
  // ? Wh                      W hour
  // ? hph                     hp hour
  // ? plf                     lb / foot    # pounds per linear foot
  // ## Compatibility units with Unix version
  // ? pa                      Pa
  // ? ev                      eV
  // ? hg                      Hg
  // ? oe                      Oe
  // ? mh                      mH
  // ? rd                      rod
  // ? pf                      pF
  // ? gr                      grain
  // ? nt                      N
  // ? hz                      Hz
  // ? hd                      hogshead
  // ? dry                     drygallon/gallon
  // ? nmile                   nauticalmile
  // ? beV                     GeV
  // ? bev                     beV
  // ? coul                    C
  // ## Radioactivity units
  // ? becquerel               /s           # Activity of radioactive source
  // ? Bq                      becquerel    #
  curie(3.7e10, "Bq"), // Defined in 1910 as the radioactivit
  // ? Ci                      curie        # emitted by the amount of radon that is
  rutherford(1e6, "Bq"), //
  // ^ RADIATION_DOSE          gray
  // ? gray                    J/kg         # Absorbed dose of radiation
  // ? Gy                      gray         #
  rad(1e-2, "Gy"), // From Radiation Absorbed Dos
  rep(8.38, "mGy"), // Roentgen Equivalent Physical, the amoun
  // ? sievert                 J/kg         # Dose equivalent:  dosage that has the
  // ? Sv                      sievert      #   same effect on human tissues as 200
  rem(1e-2, "Sv"), // keV X-rays. Different types o
  banana_dose(0.1e-6, "sievert"), // Informal measure of the dose due t
  roentgen(2.58e-4, "C / kg"), // Ionizing radiation that produce
  // ? rontgen                 roentgen     # Sometimes it appears spelled this way
  sievertunit(8.38, "rontgen"), // Unit of gamma ray dose delivered in on
  eman(1e-7, "Ci/m^3"), // radioactive concentratio
  mache(3.7e-7, "Ci/m^3"),
  actinium(227.0278, ""),
  aluminum(26.981539, ""),
  americium(243.0614, ""), // Longest lived. 241.0
  antimony(121.760, ""),
  argon(39.948, ""),
  arsenic(74.92159, ""),
  astatine(209.9871, ""), // Longest live
  barium(137.327, ""),
  berkelium(247.0703, ""), // Longest lived. 249.0
  beryllium(9.012182, ""),
  bismuth(208.98037, ""),
  boron(10.811, ""),
  bromine(79.904, ""),
  cadmium(112.411, ""),
  calcium(40.078, ""),
  californium(251.0796, ""), // Longest lived. 252.0
  carbon(12.011, ""),
  cerium(140.115, ""),
  cesium(132.90543, ""),
  chlorine(35.4527, ""),
  chromium(51.9961, ""),
  cobalt(58.93320, ""),
  copper(63.546, ""),
  curium(247.0703, ""),
  deuterium(2.0141017778, ""),
  dysprosium(162.50, ""),
  einsteinium(252.083, ""), // Longest live
  erbium(167.26, ""),
  europium(151.965, ""),
  fermium(257.0951, ""), // Longest live
  fluorine(18.9984032, ""),
  francium(223.0197, ""), // Longest live
  gadolinium(157.25, ""),
  gallium(69.723, ""),
  germanium(72.61, ""),
  gold(196.96654, ""),
  hafnium(178.49, ""),
  helium(4.002602, ""),
  holmium(164.93032, ""),
  hydrogen(1.00794, ""),
  indium(114.818, ""),
  iodine(126.90447, ""),
  iridium(192.217, ""),
  iron(55.845, ""),
  krypton(83.80, ""),
  lanthanum(138.9055, ""),
  lawrencium(262.11, ""), // Longest live
  lead(207.2, ""),
  lithium(6.941, ""),
  lutetium(174.967, ""),
  magnesium(24.3050, ""),
  manganese(54.93805, ""),
  mendelevium(258.10, ""), // Longest live
  mercury(200.59, ""),
  molybdenum(95.94, ""),
  neodymium(144.24, ""),
  neon(20.1797, ""),
  neptunium(237.0482, ""),
  nickel(58.6934, ""),
  niobium(92.90638, ""),
  nitrogen(14.00674, ""),
  nobelium(259.1009, ""), // Longest live
  osmium(190.23, ""),
  oxygen(15.9994, ""),
  palladium(106.42, ""),
  phosphorus(30.973762, ""),
  platinum(195.08, ""),
  plutonium(244.0642, ""), // Longest lived. 239.0
  polonium(208.9824, ""), // Longest lived. 209.9
  potassium(39.0983, ""),
  praseodymium(140.90765, ""),
  promethium(144.9127, ""), // Longest lived. 146.9
  protactinium(231.03588, ""),
  radium(226.0254, ""),
  radon(222.0176, ""), // Longest live
  rhenium(186.207, ""),
  rhodium(102.90550, ""),
  rubidium(85.4678, ""),
  ruthenium(101.07, ""),
  samarium(150.36, ""),
  scandium(44.955910, ""),
  selenium(78.96, ""),
  silicon(28.0855, ""),
  silver(107.8682, ""),
  sodium(22.989768, ""),
  strontium(87.62, ""),
  sulfur(32.066, ""),
  tantalum(180.9479, ""),
  technetium(97.9072, ""), // Longest lived. 98.90
  tellurium(127.60, ""),
  terbium(158.92534, ""),
  thallium(204.3833, ""),
  thorium(232.0381, ""),
  thullium(168.93421, ""),
  tin(118.710, ""),
  titanium(47.867, ""),
  tungsten(183.84, ""),
  uranium(238.0289, ""),
  vanadium(50.9415, ""),
  xenon(131.29, ""),
  ytterbium(173.04, ""),
  yttrium(88.90585, ""),
  zinc(65.39, ""),
  zirconium(91.224, ""),
  // ? air            78.08% nitrogen 2  <> + 20.95% oxygen 2  <> + 9340 ppm argon  <> +  400 ppm
  // (carbon + oxygen 2)  <> +   18.18 ppm neon  <> +    5.24 ppm helium  <> +    1.7  ppm (carbon +
  // 4 hydrogen)  <> +    1.14 ppm krypton  <> +    0.55 ppm hydrogen 2
  // ## Density of gas phase elements at STP
  hydrogendensity(0.08988, "g/l"),
  heliumdensity(0.1786, "g/l"),
  neondensity(0.9002, "g/l"),
  nitrogendensity(1.2506, "g/l"),
  oxygendensity(1.429, "g/l"),
  fluorinedensity(1.696, "g/l"),
  argondensity(1.784, "g/l"),
  chlorinedensity(3.2, "g/l"),
  kryptondensity(3.749, "g/l"),
  xenondensity(5.894, "g/l"),
  radondensity(9.73, "g/l"),
  // ## Density of liquid phase elements near room temperature
  brominedensity(3.1028, "g/cm^3"),
  mercurydensity(13.534, "g/cm^3"),
  // ## Density of solid elements near room temperature
  lithiumdensity(0.534, "g/cm^3"),
  potassiumdensity(0.862, "g/cm^3"),
  sodiumdensity(0.968, "g/cm^3"),
  rubidiumdensity(1.532, "g/cm^3"),
  calciumdensity(1.55, "g/cm^3"),
  magnesiumdensity(1.738, "g/cm^3"),
  phosphorus_white_density(1.823, "g/cm^3"),
  berylliumdensity(1.85, "g/cm^3"),
  sulfur_gamma_density(1.92, "g/cm^3"),
  cesiumdensity(1.93, "g/cm^3"),
  carbon_amorphous_density(1.95, "g/cm^3"), // average valu
  sulfur_betadensity(1.96, "g/cm^3"),
  sulfur_alpha_density(2.07, "g/cm^3"),
  carbon_graphite_density(2.267, "g/cm^3"),
  phosphorus_red_density(2.27, "g/cm^3"), // average valu
  silicondensity(2.3290, "g/cm^3"),
  phosphorus_violet_density(2.36, "g/cm^3"),
  borondensity(2.37, "g/cm^3"),
  strontiumdensity(2.64, "g/cm^3"),
  phosphorus_black_density(2.69, "g/cm^3"),
  aluminumdensity(2.7, "g/cm^3"),
  bariumdensity(3.51, "g/cm^3"),
  carbon_diamond_density(3.515, "g/cm^3"),
  scandiumdensity(3.985, "g/cm^3"),
  selenium_vitreous_density(4.28, "g/cm^3"),
  selenium_alpha_density(4.39, "g/cm^3"),
  titaniumdensity(4.406, "g/cm^3"),
  yttriumdensity(4.472, "g/cm^3"),
  selenium_gray_density(4.81, "g/cm^3"),
  iodinedensity(4.933, "g/cm^3"),
  europiumdensity(5.264, "g/cm^3"),
  germaniumdensity(5.323, "g/cm^3"),
  radiumdensity(5.5, "g/cm^3"),
  arsenicdensity(5.727, "g/cm^3"),
  tin_alpha_density(5.769, "g/cm^3"),
  galliumdensity(5.91, "g/cm^3"),
  vanadiumdensity(6.11, "g/cm^3"),
  lanthanumdensity(6.162, "g/cm^3"),
  telluriumdensity(6.24, "g/cm^3"),
  zirconiumdensity(6.52, "g/cm^3"),
  antimonydensity(6.697, "g/cm^3"),
  ceriumdensity(6.77, "g/cm^3"),
  praseodymiumdensity(6.77, "g/cm^3"),
  ytterbiumdensity(6.9, "g/cm^3"),
  neodymiumdensity(7.01, "g/cm^3"),
  zincdensity(7.14, "g/cm^3"),
  chromiumdensity(7.19, "g/cm^3"),
  manganesedensity(7.21, "g/cm^3"),
  promethiumdensity(7.26, "g/cm^3"),
  tin_beta_density(7.265, "g/cm^3"),
  indiumdensity(7.31, "g/cm^3"),
  samariumdensity(7.52, "g/cm^3"),
  irondensity(7.874, "g/cm^3"),
  gadoliniumdensity(7.9, "g/cm^3"),
  terbiumdensity(8.23, "g/cm^3"),
  dysprosiumdensity(8.54, "g/cm^3"),
  niobiumdensity(8.57, "g/cm^3"),
  cadmiumdensity(8.65, "g/cm^3"),
  holmiumdensity(8.79, "g/cm^3"),
  cobaltdensity(8.9, "g/cm^3"),
  nickeldensity(8.908, "g/cm^3"),
  erbiumdensity(9.066, "g/cm^3"),
  polonium_alpha_density(9.196, "g/cm^3"),
  thuliumdensity(9.32, "g/cm^3"),
  polonium_beta_density(9.398, "g/cm^3"),
  bismuthdensity(9.78, "g/cm^3"),
  lutetiumdensity(9.841, "g/cm^3"),
  actiniumdensity(10, "g/cm^3"),
  molybdenumdensity(10.28, "g/cm^3"),
  silverdensity(10.49, "g/cm^3"),
  technetiumdensity(11, "g/cm^3"),
  leaddensity(11.34, "g/cm^3"),
  thoriumdensity(11.7, "g/cm^3"),
  thalliumdensity(11.85, "g/cm^3"),
  americiumdensity(12, "g/cm^3"),
  palladiumdensity(12.023, "g/cm^3"),
  rhodiumdensity(12.41, "g/cm^3"),
  rutheniumdensity(12.45, "g/cm^3"),
  berkelium_beta_density(13.25, "g/cm^3"),
  hafniumdensity(13.31, "g/cm^3"),
  curiumdensity(13.51, "g/cm^3"),
  berkelium_alphadensity(14.78, "g/cm^3"),
  californiumdensity(15.1, "g/cm^3"),
  protactiniumdensity(15.37, "g/cm^3"),
  tantalumdensity(16.69, "g/cm^3"),
  uraniumdensity(19.1, "g/cm^3"),
  tungstendensity(19.3, "g/cm^3"),
  golddensity(19.30, "g/cm^3"),
  plutoniumdensity(19.816, "g/cm^3"),
  neptuniumdensity(20.45, "g/cm^3"), // alpha form, only one at room tem
  rheniumdensity(21.02, "g/cm^3"),
  platinumdensity(21.45, "g/cm^3"),
  iridiumdensity(22.56, "g/cm^3"),
  osmiumdensity(22.59, "g/cm^3"),
  // ## A few alternate names
  // ? tin_gray tin_alpha_density
  // ? tin_white tin_beta_density
  // ? graphitedensity carbon_graphite_density
  // ? diamonddensity carbon_diamond_density
  franciumdensity(2.48, "g/cm^3"), // liquid, predicted melting point 8 deg
  astatinedensity(6.35, "g/cm^3"),
  einsteiniumdensity(8.84, "g/cm^3"),
  fermiumdensity(9.7, "g/cm^3"),
  nobeliumdensity(9.9, "g/cm^3"),
  mendeleviumdensity(10.3, "g/cm^3"),
  lawrenciumdensity(16, "g/cm^3"),
  rutherfordiumdensity(23.2, "g/cm^3"),
  roentgeniumdensity(28.7, "g/cm^3"),
  dubniumdensity(29.3, "g/cm^3"),
  darmstadtiumdensity(34.8, "g/cm^3"),
  seaborgiumdensity(35, "g/cm^3"),
  bohriumdensity(37.1, "g/cm^3"),
  meitneriumdensity(37.4, "g/cm^3"),
  hassiumdensity(41, "g/cm^3"),
  // ## population units
  people(1, ""),
  // ? person                  people
  // ? death                   people
  // ? capita                  people
  // ? percapita               per capita
  // ? Tim                     12^-4 hour         # Time
  // ? Grafut                  gravity Tim^2      # Length based on gravity
  // ? Surf                    Grafut^2           # area
  // ? Volm                    Grafut^3           # volume
  // ? Vlos                    Grafut/Tim         # speed
  // ? Denz                    Maz/Volm           # density
  // ? Mag                     Maz gravity        # force
  // ? Maz                     Volm kg / oldliter # mass based on water
  // ? Tm                      Tim                # Abbreviations
  // ? Gf                      Grafut
  // ? Sf                      Surf
  // ? Vm                      Volm
  // ? Vl                      Vlos
  // ? Mz                      Maz
  // ? Dz                      Denz
  // ## Dozen based unit prefixes
  // -Zena-                   12
  // -Duna-                   12^2
  // -Trina-                  12^3
  // -Quedra-                 12^4
  // -Quena-                  12^5
  // -Hesa-                   12^6
  // -Seva-                   12^7
  // -Aka-                    12^8
  // -Neena-                  12^9
  // -Dexa-                   12^10
  // -Lefa-                   12^11
  // -Zennila-                12^12
  // -Zeni-                   12^-1
  // -Duni-                   12^-2
  // -Trini-                  12^-3
  // -Quedri-                 12^-4
  // -Queni-                  12^-5
  // -Hesi-                   12^-6
  // -Sevi-                   12^-7
  // -Aki-                    12^-8
  // -Neeni-                  12^-9
  // -Dexi-                   12^-10
  // -Lefi-                   12^-11
  // -Zennili-                12^-12
  // : wari_proportion      1|10
  // ? wari                 wari_proportion
  // : bu_proportion        1|100    # The character bu can also be read fun or bun
  // : rin_proportion       1|1000
  // : mou_proportion       1|10000
  // : shaku              1|3.3 m
  // : mou                1|10000 shaku
  // : rin                1|1000 shaku
  // : bu_distance        1|100 shaku
  // : sun                1|10 shaku
  jou_distance(10, "shaku"),
  // ? jou                jou_distance
  // ? kanejakusun        sun      # Alias to emphasize architectural name
  // ? kanejaku           shaku
  // ? kanejakujou        jou
  /// *taichi             shaku   # http://zh.wikipedia.org/wiki/
  /// *taicun             sun     # http://zh.wikipedia.org/wiki/
  // !utf8
  // !endutf8
  // : kujirajaku         10|8 shaku
  // : kujirajakusun      1|10 kujirajaku
  // : kujirajakubu       1|100 kujirajaku
  kujirajakujou(10, "kujirajaku"),
  tan_distance(3, "kujirajakujou"),
  ken(6, "shaku"), // Also sometimes 6.3, 6.5, or 6.
  chou_distance(60, "ken"),
  // ? chou               chou_distance
  ri(36, "chou"),
  // ## Japanese Area Measures
  // : gou_area             1|10 tsubo
  tsubo(36, "shaku^2"), // Size of two tatami = ken^2 ?
  se(30, "tsubo"),
  tan_area(10, "se"),
  chou_area(10, "tan_area"),
  /// *ping                 tsubo     # http://zh.wikipedia.org/wiki/
  /// *jia                  2934 ping # http://zh.wikipedia.org/wiki/
  /// *fen                  1|10 jia  # http://zh.wikipedia.org/wiki/
  // : fen_area             1|10 jia  # Protection against future collisions
  // !utf8
  // !endutf8
  // ? edoma                (5.8*2.9) shaku^2
  // ? kyouma               (6.3*3.15) shaku^2
  // ? chuukyouma           (6*3) shaku^2
  // ? jou_area             edoma
  // ? tatami               jou_area
  // ## Japanese Volume Measures
  // : shaku_volume         1|10 gou_volume
  // : gou_volume           1|10 shou
  // ? gou                  gou_volume
  // ? shou                 (4.9*4.9*2.7) sun^3   # The character shou which is
  to(10, "shou"),
  koku(10, "to"), // No longer used; historically a measure of ric
  // ## Not really used anymore.
  // : rin_weight           1|10 bu_weight
  // : bu_weight            1|10 monme
  // : fun                  1|10 monme
  // ? monme                momme
  kin(160, "monme"),
  kan(1000, "monme"),
  // ? kwan                 kan         # This was the old pronounciation of the unit.
  /// *taijin               kin      # http://zh.wikipedia.org/wiki/
  /// *tailiang             10 monme # http://zh.wikipedia.org/wiki/
  /// *taiqian              monme    # http://zh.wikipedia.org/wiki/
  // !utf8
  // !endutf8
  // ## Australian unit
  // ? australiasquare         (10 ft)^2   # Used for house area
  // ## A few German units as currently in use.
  zentner(50, "kg"),
  doppelzentner(2, "zentner"),
  pfund(500, "g"),
  austriaklafter(1.89648384, "m"), // Exact definition, 23 July 187
  // : austriafoot             1|6 austriaklafter
  prussiaklafter(1.88, "m"),
  // : prussiafoot             1|6 prussiaklafter
  bavariaklafter(1.751155, "m"),
  // : bavariafoot             1|6 bavariaklafter
  hesseklafter(2.5, "m"),
  // : hessefoot               1|6 hesseklafter
  // ? switzerlandklafter      metricklafter
  // : switzerlandfoot         1|6 switzerlandklafter
  // ? swissklafter            switzerlandklafter
  // : swissfoot               1|6 swissklafter
  metricklafter(1.8, "m"),
  austriayoke(8, "austriaklafter * 200 austriaklafter"),
  liechtensteinsquareklafter(3.596652, "m^2"), // Used until 2017 to measure land are
  // ? liechtensteinklafter  sqrt(liechtensteinsquareklafter)
  prussiawoodklafter(0.5, "prussiaklafter^3"),
  austriawoodklafter(0.5, "austriaklafter^3"),
  // ? festmeter               m^3             # modern measure of wood, solid cube
  raummeter(0.7, "festmeter"), // Air space between the logs, stacke
  schuettraummeter(0.65, "raummeter"), // A cubic meter volume of split and cu
  /// *sch
  verklinje(2.0618125, "mm"),
  verktum(12, "verklinje"),
  kvarter(6, "verktum"),
  fot(2, "kvarter"),
  aln(2, "fot"),
  famn(3, "aln"),
  dessiatine(2400, "sazhen^2"), // Land measur
  // ? dessjatine              dessiatine
  funt(409.51718, "grams"), // similar to poun
  // : zolotnik                1|96 funt        # used for precious metal measure
  pood(40, "funt"), // common in agricultural measur
  // ? arshin                  (2 + 1|3) feet
  sazhen(3, "arshin"), // analogous to fatho
  verst(500, "sazhen"), // of similar use to mil
  // ? versta                  verst
  borderverst(1000, "sazhen"),
  russianmile(7, "verst"),
  // : frenchfoot              144|443.296 m     # pied de roi, the standard of Paris.
  // ? pied                    frenchfoot        #   Half of the hashimicubit,
  // ? frenchfeet              frenchfoot        #   instituted by Charlemagne.
  // : frenchinch              1|12 frenchfoot   #   This exact definition comes from
  // ? frenchthumb             frenchinch        #   a law passed on 10 Dec 1799 which
  // ? pouce                   frenchthumb       #   fixed the meter at
  // : frenchline              1|12 frenchinch   # This is supposed to be the size
  // ? ligne                   frenchline        #   of the average barleycorn
  // : frenchpoint             1|12 frenchline
  toise(6, "frenchfeet"),
  // ? arpent                  180^2 pied^2      # The arpent is 100 square perches,
  // : frenchgrain             1|18827.15 kg     # Weight of a wheat grain, hence
  frenchpound(9216, "frenchgrain"),
  // ## Scots linear measure
  scotsinch(1.00540054, "UKinch"),
  // : scotslink        1|100 scotschain
  scotsfoot(12, "scotsinch"),
  // ? scotsfeet        scotsfoot
  scotsell(37, "scotsinch"),
  scotsfall(6, "scotsell"),
  scotschain(4, "scotsfall"),
  scotsfurlong(10, "scotschain"),
  scotsmile(8, "scotsfurlong"),
  // ## Scots area measure
  scotsrood(40, "scotsfall^2"),
  scotsacre(4, "scotsrood"),
  // ## Irish linear measure
  // ? irishinch       UKinch
  irishpalm(3, "irishinch"),
  irishspan(3, "irishpalm"),
  irishfoot(12, "irishinch"),
  // ? irishfeet       irishfoot
  irishcubit(18, "irishinch"),
  irishyard(3, "irishfeet"),
  irishpace(5, "irishfeet"),
  irishfathom(6, "irishfeet"),
  irishpole(7, "irishyard"), // Only these value
  // ? irishperch      irishpole        # are different from
  irishchain(4, "irishperch"), // the British Imperia
  // : irishlink       1|100 irishchain # or English values for
  irishfurlong(10, "irishchain"), // these lengths
  irishmile(8, "irishfurlong"), //
  // ##  Irish area measure
  irishrood(40, "irishpole^2"),
  irishacre(4, "irishrood"),
  // ## English wine capacity measures (Winchester measures)
  // : winepint       1|2 winequart
  // : winequart      1|4 winegallon
  winegallon(231, "UKinch^3"), // Sometimes called the Winchester Wine Gallon
  winerundlet(18, "winegallon"),
  winebarrel(31.5, "winegallon"),
  winetierce(42, "winegallon"),
  winehogshead(2, "winebarrel"),
  winepuncheon(2, "winetierce"),
  winebutt(2, "winehogshead"),
  // ? winepipe       winebutt
  winetun(2, "winebutt"),
  // ## English beer and ale measures used 1803-1824 and used for beer before 1688
  // : beerpint       1|2 beerquart
  // : beerquart      1|4 beergallon
  beergallon(282, "UKinch^3"),
  beerbarrel(36, "beergallon"),
  beerhogshead(1.5, "beerbarrel"),
  // ## English ale measures used from 1688-1803 for both ale and beer
  // : alepint        1|2 alequart
  // : alequart       1|4 alegallon
  // ? alegallon      beergallon
  alebarrel(34, "alegallon"),
  alehogshead(1.5, "alebarrel"),
  // ## Scots capacity measure
  // : scotsgill      1|4 mutchkin
  // : mutchkin       1|2 choppin
  // : choppin        1|2 scotspint
  // : scotspint      1|2 scotsquart
  // : scotsquart     1|4 scotsgallon
  scotsgallon(827.232, "UKinch^3"),
  scotsbarrel(8, "scotsgallon"),
  // ? jug            scotspint
  // ## Scots dry capacity measure
  scotswheatlippy(137.333, "UKinch^3"), // Also used for peas, beans, rye, sal
  // ? scotswheatlippies scotswheatlippy
  scotswheatpeck(4, "scotswheatlippy"),
  scotswheatfirlot(4, "scotswheatpeck"),
  scotswheatboll(4, "scotswheatfirlot"),
  scotswheatchalder(16, "scotswheatboll"),
  scotsoatlippy(200.345, "UKinch^3"), // Also used for barley and mal
  // ? scotsoatlippies   scotsoatlippy
  scotsoatpeck(4, "scotsoatlippy"),
  scotsoatfirlot(4, "scotsoatpeck"),
  scotsoatboll(4, "scotsoatfirlot"),
  scotsoatchalder(16, "scotsoatboll"),
  // ## Scots Tron weight
  // : trondrop       1|16 tronounce
  // : tronounce      1|20 tronpound
  tronpound(9520, "grain"),
  tronstone(16, "tronpound"),
  // ## Irish liquid capacity measure
  // : irishnoggin    1|4 irishpint
  // : irishpint      1|2 irishquart
  // : irishquart     1|2 irishpottle
  // : irishpottle    1|2 irishgallon
  irishgallon(217.6, "UKinch^3"),
  irishrundlet(18, "irishgallon"),
  irishbarrel(31.5, "irishgallon"),
  irishtierce(42, "irishgallon"),
  irishhogshead(2, "irishbarrel"),
  irishpuncheon(2, "irishtierce"),
  irishpipe(2, "irishhogshead"),
  irishtun(2, "irishpipe"),
  // ## Irish dry capacity measure
  irishpeck(2, "irishgallon"),
  irishbushel(4, "irishpeck"),
  irishstrike(2, "irishbushel"),
  irishdrybarrel(2, "irishstrike"),
  irishquarter(2, "irishbarrel"),
  // ## English Tower weights, abolished in 1528
  towerpound(5400, "grain"),
  // : towerounce       1|12 towerpound
  // : towerpennyweight 1|20 towerounce
  // : towergrain       1|32 towerpennyweight
  // ## English Mercantile weights, used since the late 12th century
  mercpound(6750, "grain"),
  // : mercounce      1|15 mercpound
  // : mercpennyweight 1|20 mercounce
  // ## English weights for lead
  leadstone(12.5, "lb"),
  fotmal(70, "lb"),
  leadwey(14, "leadstone"),
  fothers(12, "leadwey"),
  // ## English Hay measure
  newhaytruss(60, "lb"), // New and old here seem to refer to "new
  newhayload(36, "newhaytruss"), // hay and "old" hay rather than a new uni
  oldhaytruss(56, "lb"), // and an old unit
  oldhayload(36, "oldhaytruss"),
  // ## English wool measure
  woolclove(7, "lb"),
  woolstone(2, "woolclove"),
  wooltod(2, "woolstone"),
  woolwey(13, "woolstone"),
  woolsack(2, "woolwey"),
  woolsarpler(2, "woolsack"),
  woollast(6, "woolsarpler"),
  romanfoot(296, "mm"), // There is some uncertainty in this definitio
  // ? romanfeet    romanfoot       # from which all the other units are derived.
  // ? pes          romanfoot       # This value appears in numerous sources. In "The
  // ? pedes        romanfoot       # Roman Land Surveyors", Dilke gives 295.7 mm.
  // : romaninch    1|12 romanfoot  # The subdivisions of the Roman foot have the
  // : romandigit   1|16 romanfoot  #   same names as the subdivisions of the pound,
  // : romanpalm    1|4 romanfoot   #   but we can't have the names for different
  romancubit(18, "romaninch"), // units
  romanpace(5, "romanfeet"), // Roman double pace (basic military unit
  // ? passus       romanpace
  romanperch(10, "romanfeet"),
  stade(125, "romanpaces"),
  // ? stadia       stade
  // ? stadium      stade
  romanmile(8, "stadia"), // 1000 pace
  romanleague(1.5, "romanmile"),
  schoenus(4, "romanmile"),
  // ## Other values for the Roman foot (from Dilke)
  earlyromanfoot(29.73, "cm"),
  pesdrusianus(33.3, "cm"), // or 33.35 cm, used in Gaul & Germany in 1st c B
  lateromanfoot(29.42, "cm"),
  // ## Roman areas
  actuslength(120, "romanfeet"), // length of a Roman furro
  // ? actus        120*4 romanfeet^2 # area of the furrow
  // ? squareactus  120^2 romanfeet^2 # actus quadratus
  // ? acnua        squareactus
  iugerum(2, "squareactus"),
  // ? iugera       iugerum
  // ? jugerum      iugerum
  // ? jugera       iugerum
  heredium(2, "iugera"), // heritable plo
  // ? heredia      heredium
  centuria(100, "heredia"),
  // ? centurium    centuria
  // ## Roman volumes
  sextarius(35.4, "in^3"), // Basic unit of Roman volume. As always
  // ? sextarii        sextarius      # there is uncertainty.  Six large Roman
  // : cochlearia      1|48 sextarius
  // : cyathi          1|12 sextarius
  // : acetabula       1|8 sextarius
  // : quartaria       1|4 sextarius
  // ? quartarius      quartaria
  // : heminae         1|2 sextarius
  // ? hemina          heminae
  cheonix(1.5, "sextarii"),
  // ## Dry volume measures (usually)
  semodius(8, "sextarius"),
  // ? semodii         semodius
  modius(16, "sextarius"),
  // ? modii           modius
  // ## Liquid volume measures (usually)
  congius(12, "heminae"),
  // ? congii          congius
  amphora(8, "congii"),
  // ? amphorae        amphora      # Also a dry volume measure
  culleus(20, "amphorae"),
  // ? quadrantal      amphora
  // ## Roman weights
  libra(5052, "grain"), // The Roman pound varied significantl
  // ? librae          libra        # from 4210 grains to 5232 grains.  Most of
  // ? romanpound      libra        # the standards were obtained from the weight
  // : uncia           1|12 libra   # of particular coins.  The one given here is
  // ? unciae          uncia        # based on the Gold Aureus of Augustus which
  // ? romanounce      uncia        # was in use from BC 27 to AD 296.
  deunx(11, "uncia"),
  dextans(10, "uncia"),
  dodrans(9, "uncia"),
  bes(8, "uncia"),
  seprunx(7, "uncia"),
  semis(6, "uncia"),
  quincunx(5, "uncia"),
  triens(4, "uncia"),
  quadrans(3, "uncia"),
  sextans(2, "uncia"),
  sescuncia(1.5, "uncia"),
  // : semuncia        1|2 uncia
  // : siscilius       1|4 uncia
  // : sextula         1|6 uncia
  // : semisextula     1|12 uncia
  // : scriptulum      1|24 uncia
  // ? scrupula        scriptulum
  // : romanobol       1|2 scrupula
  romanaspound(4210, "grain"), // Old pound based on bronze coinage, th
  // ## Egyptian length measure
  egyptianroyalcubit(20.63, "in"), // plus or minus .2 i
  // : egyptianpalm            1|7 egyptianroyalcubit
  // : egyptiandigit           1|4 egyptianpalm
  egyptianshortcubit(6, "egyptianpalm"),
  doubleremen(29.16, "in"), // Length of the diagonal of a square wit
  // : remendigit       1|40 doubleremen # side length of 1 royal egyptian cubit.
  // ## Greek length measures
  greekfoot(12.45, "in"), // Listed as being derived from th
  // ? greekfeet               greekfoot     # Egyptian Royal cubit in [11].  It is
  greekcubit(1.5, "greekfoot"), // said to be 3|5 of a 20.75 in cubit
  // ? pous                    greekfoot
  // ? podes                   greekfoot
  orguia(6, "greekfoot"),
  // ? greekfathom             orguia
  stadion(100, "orguia"),
  akaina(10, "greekfeet"),
  plethron(10, "akaina"),
  // : greekfinger             1|16 greekfoot
  homericcubit(20, "greekfingers"), // Elbow to end of knuckles
  shortgreekcubit(18, "greekfingers"), // Elbow to start of fingers
  ionicfoot(296, "mm"),
  doricfoot(326, "mm"),
  olympiccubit(25, "remendigit"), // These olympic measures were not a
  // : olympicfoot             2|3 olympiccubit # common as the other greek measures.
  // : olympicfinger           1|16 olympicfoot # They were used in agriculture.
  // ? olympicfeet             olympicfoot
  // ? olympicdakylos          olympicfinger
  // : olympicpalm             1|4 olympicfoot
  // ? olympicpalestra         olympicpalm
  // : olympicspithame         3|4 foot
  // ? olympicspan             olympicspithame
  olympicbema(2.5, "olympicfeet"),
  // ? olympicpace             olympicbema
  olympicorguia(6, "olympicfeet"),
  // ? olympicfathom           olympicorguia
  olympiccord(60, "olympicfeet"),
  // ? olympicamma             olympiccord
  olympicplethron(100, "olympicfeet"),
  olympicstadion(600, "olympicfeet"),
  // ## Greek capacity measure
  greekkotyle(270, "ml"), // This approximate value is obtaine
  xestes(2, "greekkotyle"), // from two earthenware vessels tha
  khous(12, "greekkotyle"), // were reconstructed from fragments
  metretes(12, "khous"), // The kotyle is a day's corn ratio
  choinix(4, "greekkotyle"), // for one man
  hekteos(8, "choinix"),
  medimnos(6, "hekteos"),
  aeginastater(192, "grain"), // Varies up to 199 grai
  // : aeginadrachmae          1|2 aeginastater
  // : aeginaobol              1|6 aeginadrachmae
  aeginamina(50, "aeginastaters"),
  aeginatalent(60, "aeginamina"), // Supposedly the mass of a cubic foo
  atticstater(135, "grain"), // Varies 134-138 grai
  // : atticdrachmae           1|2 atticstater
  // : atticobol               1|6 atticdrachmae
  atticmina(50, "atticstaters"),
  attictalent(60, "atticmina"), // Supposedly the mass of a cubic foo
  northerncubit(26.6, "in"), // plus/minus .2 i
  // : northernfoot            1|2 northerncubit
  sumeriancubit(495, "mm"),
  // ? kus                     sumeriancubit
  // : sumerianfoot            2|3 sumeriancubit
  assyriancubit(21.6, "in"),
  // : assyrianfoot            1|2 assyriancubit
  // : assyrianpalm            1|3 assyrianfoot
  // : assyriansusi            1|20 assyrianpalm
  // ? susi                    assyriansusi
  persianroyalcubit(7, "assyrianpalm"),
  hashimicubit(25.56, "in"), // Standard of linear measure use
  blackcubit(21.28, "in"),
  // : arabicfeet              1|2 blackcubit
  // ? arabicfoot              arabicfeet
  // : arabicinch              1|12 arabicfoot
  arabicmile(4000, "blackcubit"),
  silverdirhem(45, "grain"), // The weights were derived from these tw
  tradedirhem(48, "grain"), // units with two identically named system
  // : silverkirat             1|16 silverdirhem
  silverwukiyeh(10, "silverdirhem"),
  silverrotl(12, "silverwukiyeh"),
  // ? arabicsilverpound       silverrotl
  // : tradekirat              1|16 tradedirhem
  tradewukiyeh(10, "tradedirhem"),
  traderotl(12, "tradewukiyeh"),
  // ? arabictradepound        traderotl
  // ## Miscellaneous ancient units
  parasang(3.5, "mile"), // Persian unit of length usually though
  biblicalcubit(21.8, "in"),
  hebrewcubit(17.58, "in"),
// : li                      10|27.8 mile  # Chinese unit of length
// : liang                   11|3 oz       # Chinese weight unit
// : timepoint               1|5 hour  # also given as 1|4
// : timeminute              1|10 hour
// : timeostent              1|60 hour
// : timeounce               1|8 timeostent
// : timeatom                1|47 timeounce
// : mite                    1|20 grain
// : droit                   1|24 mite
// : periot                  1|20 droit
// : blanc                   1|24 periot
// ## Localization
// !var UNITS_ENGLISH US
// ? hundredweight           ushundredweight
// ? ton                     uston
// ? scruple                 apscruple
// ? fluidounce              usfluidounce
// ? gallon                  usgallon
// ? bushel                  usbushel
// ? quarter                 quarterweight
// ? cup                     uscup
// ? tablespoon              ustablespoon
// ? teaspoon                usteaspoon
// ? dollar                  US$
// ? cent                    $ 0.01
// ? penny                   cent
// ? minim                   minimvolume
// ? pony                    ponyvolume
// ? grand                   usgrand
// ? firkin                  usfirkin
// ? hogshead                ushogshead
// !endvar
// !var UNITS_ENGLISH GB
// ? hundredweight           brhundredweight
// ? ton                     brton
// ? scruple                 brscruple
// ? fluidounce              brfluidounce
// ? gallon                  brgallon
// ? bushel                  brbushel
// ? quarter                 brquarter
// ? chaldron                brchaldron
// ? cup                     brcup
// ? teacup                  brteacup
// ? tablespoon              brtablespoon
// ? teaspoon                brteaspoon
// ? dollar                  US$
// ? cent                    $ 0.01
// ? penny                   brpenny
// ? minim                   minimnote
// ? pony                    brpony
// ? grand                   brgrand
// ? firkin                  brfirkin
// ? hogshead                brhogshead
// !endvar
// !varnot UNITS_ENGLISH GB US
// !message Unknown value for environment variable UNITS_ENGLISH.  Should be GB or US.
// !endvar
// !utf8
// !endutf8
// !unitlist hms hr;min;sec
// !unitlist time year;day;hr;min;sec
// !unitlist dms deg;arcmin;arcsec
// !unitlist ftin ft;in;1|8 in
// !unitlist inchfine in;1|8 in;1|16 in;1|32 in;1|64 in
// !unitlist usvol cup;3|4 cup;2|3 cup;1|2 cup;1|3 cup;1|4 cup; <> tbsp;tsp;1|2 tsp;1|4 tsp;1|8 tsp
;

  public final double val;
  public final String units;

  private Units(double v, String u) {
    val = v;
    units = u;
  }
}
