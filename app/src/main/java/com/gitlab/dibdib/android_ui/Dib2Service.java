// Copyright (C) 2022, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.android_ui;

import android.app.*;
import android.content.*;
import android.graphics.drawable.Icon;
import android.os.*;
import net.sf.dibdib.config.*;

public class Dib2Service extends Service {

  static Dib2Service zService = null;
  private static PowerManager.WakeLock wakeLock = null;
  public static Icon qIconX = null;
  public static int qIconNotificationOn = -1;
  public static int qIconNotificationOff = -1;

  public static boolean forceActive = false;
  public static boolean restartJob = false;
  public static boolean keepNotification = false;

  @Override
  public void onCreate() {
    super.onCreate();
    zService = this;
    FrameDroid.create(this);
    if (forceActive && (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)) {
      Notification notification = createNotification();
      startForeground(1, notification);
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    UtilDroid.logNToast("SVC: end");
    //mFrame = null;
    zService = null;
  }

  @Override
  public IBinder onBind(Intent intent) {
    UtilDroid.logNToast("SVC: Reject binding");
    return null;
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    if (!"1".equals(UtilDroid.loadConfigValue(this, "bPermitted"))) {
      return START_NOT_STICKY;
    }
    Dib2Service.zService = this;
    if (!forceActive && (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)) {
      Notification notification = createNotification();
      startForeground(1, notification);
    }
    if (intent != null) {
      String action = intent.getAction();
      UtilDroid.logNToast("SVC: onStartCommand: " + action + ' ' + startId);
      if ("START".equals(action)) {
        startJob();
        // Re-start job if it gets killed?
        return restartJob ? START_REDELIVER_INTENT : START_NOT_STICKY;
      } else if ("STOP".equals(action)) {
        stopService(true);

        // TODO
        // } else if ("ALARM".equals(action)){
        // } else if ("QUEUE".equals(action)){

      } else {
        UtilDroid.logNToast("SVC: Unknown action");
      }
    } else {
      UtilDroid.logNToast("SVC: onStartCommand: re-start job");
      startJob();
    }
    return START_NOT_STICKY;
  }

  @Override
  public void onTaskRemoved(Intent rootIntent) {
    if (Dib2Root.app.bServiceThreadsHalted || (0 >= Dib2Root.app.serviceInterval_msec)) {
      return;
    }
    Dib2Receiver.startTimer(this);
  }

  private static void sleepSound(long msec, Context ctx) {
    if (0 < msec) {
      long tim = System.currentTimeMillis();
      try {
        Thread.sleep((int) msec);
      } catch (Exception e0) {
      }
      UtilDroid.logNToast("(SVC): sleep + " + (System.currentTimeMillis() - tim));
    }
    if (0 < Dib2Root.app.alarmTime_msec) {
      UtilDroid.playSound(ctx, true);
    }
  }

  private static void startWorker(Context ctx) {
    long time = System.currentTimeMillis();
    if ((0 < Dib2Root.app.alarmTime_msec)
        && ((Dib2Root.app.alarmTime_msec - Dib2Root.app.jSound0_msec - 999) <= time)) {
      sleepSound(Dib2Root.app.alarmTime_msec - time, ctx);
      time = System.currentTimeMillis();
      sleepSound(Dib2Root.app.alarmTime_msec + Dib2Root.app.jSound1_msec - time, ctx);
      Dib2Root.app.alarmTime_msec = 0;
    }
    Dib2Root.schedulerTrigger.trigger(null);
  }

  private void startJob() {
    UtilDroid.logNToast("SVC: start");
    wakeLockAcquire(this);
    Dib2Receiver.cancelTimer(false);
    Dib2Root.app.bServiceThreadsHalted = false;

    final Context ctx = this;
    new Thread() {
      public void run() {
        startWorker(ctx);
        FrameDroid.instance.invalidate();
        if (!forceActive) {
          try {
            // Show notification for 2 secs ...
            Thread.sleep(2000);
          } catch (Exception e0) {
          }
          stopService(true);
        }
      }
    }.start();
  }

  void stopService(boolean halt) {
    Dib2Root.app.bServiceThreadsHalted = halt;
    if (halt) {
      Dib2Receiver.cancelTimer(true);
      Dib2Root.app.alarmTime_msec = 0;
    }
    UtilDroid.logNToast("SVC: stop");
    wakeLockRelease(this);
    try {
      if (halt || ((0 >= Dib2Root.app.alarmTime_msec) && (0 >= Dib2Root.app.serviceInterval_msec))) {
        stopForeground(!keepNotification);
        //Dib2Root.app.appState = Dib2Lang.AppState.CREATE;
        //mFrame = null;
        stopSelf();
      }
    } catch (Exception e) {
      UtilDroid.logNToast("SVC: exc " + e.getMessage());
    }
    //zService = null;
  }

  private Notification createNotification() {
    String notificationChannelId = "Dib2_" + Dib2Root.app.appName;
    boolean show = null != Dib2Root.app.msg4Notification;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
      NotificationChannel it = new NotificationChannel(notificationChannelId, "Dib2",
          (0 < Dib2Root.app.alarmTime_msec) ? NotificationManager.IMPORTANCE_HIGH : NotificationManager.IMPORTANCE_LOW);
      it.setDescription("Dib2");
      it.enableVibration(show);
      it.enableLights(show);
      if (Build.VERSION.SDK_INT >= 29) {
        it.setAllowBubbles(false);
      }
      if (show) {
        // it.setLightColor(Color.BLUE);
        it.setVibrationPattern(new long[] { 100, 200, 400, 200 });
      }
      notificationManager.createNotificationChannel(it);
    }

    Class<?> cl = Dib2Activity.class;
    try {
      cl = Class.forName(Dib2Root.app.mainClassName);
    } catch (Exception e0) {
    }
    Intent intent = new Intent(this, cl);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    //final int FLAG_IM/MUTABLE = 0x0/4000000;
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);

    Notification.Builder builder;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      builder = new Notification.Builder(this, notificationChannelId);
    } else {
      builder = new Notification.Builder(this);
      if (Build.VERSION.SDK_INT >= 16) {
        builder.setPriority(Notification.PRIORITY_HIGH);
      }
    }

    builder.setContentTitle("Dib2");
    builder.setContentText(show ? Dib2Root.app.msg4Notification : "Dib2 SVC");
    builder.setContentIntent(pendingIntent);
    builder.setSmallIcon(show ? qIconNotificationOn : qIconNotificationOff);
    builder.setAutoCancel(true);
    if (Build.VERSION.SDK_INT >= 16) {
      return builder.build();
    } else {
      return builder.getNotification();
    }
  }

  static boolean triggerService(Context context, String action) {
    context = (null != context) ? context : Dib2Service.zService;
    context = (null != context) ? context : Dib2Activity.getActivity();
    if ((null == context) || !"1".equals(UtilDroid.loadConfigValue(context, "bPermitted"))) {
      return false;
    }
    wakeLockRelease(context);
    if (Dib2Root.app.bServiceThreadsHalted && "STOP".equals(action)) {
      return true;
    }
    Intent it = new Intent(context, Dib2Service.class);
    it.setAction(action);
    UtilDroid.logNToast("trigger " + action);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      context.startForegroundService(it);
    } else {
      context.startService(it);
    }
    wakeLockAcquire(context);
    return true;
  }

  public boolean isDeviceActive() {
    final PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
    if (Build.VERSION.SDK_INT < 20) {
      return powerManager.isScreenOn();
    }
    return powerManager.isInteractive();
  }

  /////

  private static void wakeLockRelease(Context ctx) {
    if ((null != wakeLock) && wakeLock.isHeld()) {
      wakeLock.release();
    }
    wakeLock = null;
  }

  private static void wakeLockAcquire(Context ctx) {
    if ((null != wakeLock) && wakeLock.isHeld()) {
      return;
    }
    int lvlNFlags = Dib2Root.app.bWakeUp ? (PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_DIM_WAKE_LOCK)
        : PowerManager.PARTIAL_WAKE_LOCK;
    wakeLock = ((PowerManager) ctx.getSystemService(Context.POWER_SERVICE)).newWakeLock(lvlNFlags, "dib2:lock");
    wakeLock.setReferenceCounted(false);
    wakeLock.acquire();
  }

}
