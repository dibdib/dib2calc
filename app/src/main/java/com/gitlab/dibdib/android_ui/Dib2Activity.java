// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.android_ui;

import android.Manifest;
import android.app.Activity;
import android.content.*;
import android.content.pm.PackageManager;
import android.os.*;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;
import androidx.core.app.*;
import androidx.core.content.*;
import java.io.File;
import java.util.Arrays;
import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Lang.AppState;
import net.sf.dibdib.generic.QToken;
import net.sf.dibdib.thread_any.DateFunc;
import net.sf.dibdib.thread_feed.QOpFeed;

public class Dib2Activity extends Activity {
  // =====

  private CanvasDroid mView;

  private static Dib2Activity zActivity = null;

  private static int hPermissionRequest = 0;

  /** To be overridden. */
  protected void createStep0(File mainDir, boolean restrict) {
  }

  protected boolean createStep1() {
    // appShort etc. are supposed to be set by now.
    UtilDroid.saveConfigValue(this, "appName", Dib2Root.app.appName);
    UtilDroid.saveConfigValue(this, "appShort", Dib2Root.app.appShort);
    UtilDroid.saveConfigValue(this, "bAllowDummyPass", Dib2Root.app.bAllowDummyPass ? "1" : "0");

    mView = new CanvasDroid(this);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(mView);
    return true;
  }

  private void close() {

    zActivity = null;

    if (Dib2Lang.AppState.ACTIVE.ordinal() == Dib2Root.app.appState.ordinal()) {
      Dib2Root.schedulerTrigger.trigger(QToken.createTask(QOpFeed.zzSAV0));
    } else {
      FrameDroid frame = FrameDroid.instance;
      if (null != frame) {
        frame.invalidate();
      }
    }
  }

  public void exit() {
    close();
    finish();
  }

  static String[] checkPermission_requesting = null;

  private void checkPermission(String permission, boolean flag) {
    if (null == permission) {
      if (null == checkPermission_requesting) {
        UtilDroid.saveConfigValue(this, "bPermitted", "1");
        return;
      }
      ActivityCompat.requestPermissions(this, checkPermission_requesting, ++hPermissionRequest);
      return;
    }
    if (flag
        && !(ContextCompat.checkSelfPermission(this, permission)
            == PackageManager.PERMISSION_GRANTED)) {
      if (null == checkPermission_requesting) {
        checkPermission_requesting = new String[] {permission};
      } else {
        checkPermission_requesting =
            Arrays.copyOf(checkPermission_requesting, checkPermission_requesting.length + 1);
        checkPermission_requesting[checkPermission_requesting.length - 1] = permission;
      }
    }
  }

  @Override
  public void onRequestPermissionsResult(
      int permsRequestCode, String[] permissions, int[] grantResults) {
    Log.d("onRequestPermissionsR", "" + permissions.length);
    zActivity = this;
    // This checks the permissions and triggers the service:
    callUpdateViews();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    zActivity = this;

    boolean restrict = ContextCompat.checkSelfPermission(this,
        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

    // Implemented by subclass:
    createStep0(getFilesDir(), restrict);

    FrameDroid.create(this);
    createStep1();

    if (0 >= hPermissionRequest) {
      hPermissionRequest = (int) (DateFunc.currentTimeNanobisLinearized(false) >> 30);
      if (Build.VERSION.SDK_INT >= 23) { // Build.VERSION_CODES.M) {
        if (!Dib2Root.app.bAllowDummyPass) {
          checkPermission(Manifest.permission.WAKE_LOCK, true);
          checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, Dib2Root.app.bStorage);
          checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Dib2Root.app.bStorage);
          checkPermission(Manifest.permission.SET_ALARM, Dib2Root.app.bWakeUp);
          checkPermission(Manifest.permission.RECEIVE_BOOT_COMPLETED, Dib2Root.app.bAutostart);
          checkPermission(Manifest.permission.INTERNET, Dib2Root.app.bInternet);
        }
        checkPermission(Manifest.permission.VIBRATE, Dib2Root.app.bVibrate);
      }
      if (Build.VERSION.SDK_INT >= 28) {
        checkPermission(Manifest.permission.FOREGROUND_SERVICE, true);
      }
      if (Build.VERSION.SDK_INT >= 29) {
        checkPermission(Manifest.permission.USE_FULL_SCREEN_INTENT, true); // Dib2Root.app.bWakeUp);
      }
      checkPermission(null, true);
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    Log.d("onResume", ".");
    if (Dib2Root.app.appState.ordinal() > AppState.ACTIVE.ordinal()) {
      finish();
    }

    zActivity = this;

    // Device size:
    CanvasDroid.qDm = getApplicationContext().getResources().getDisplayMetrics();

    Dib2Root.resume();
  }

  @Override
  public void onPause() {
    close();
    if (null == checkPermission_requesting) {
      Dib2Service.triggerService(this, "START");
    }
    zActivity = null;
    Log.d("onPause", ".");
    super.onPause();
  }

  private boolean onBackPressed_recently = false;

  @Override
  public void onBackPressed() {
    if (onBackPressed_recently || !Dib2Root.ui.bExitConfirmation) {
      super.onBackPressed();
      return;
    }
    onBackPressed_recently = true;
    Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();
    new Handler()
        .postDelayed(
            new Runnable() {

              @Override
              public void run() {
                onBackPressed_recently = false;
              }
            },
            3000);
  }

  public boolean pushClipboardA(String label, String text) {
    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
    ClipData clip = ClipData.newPlainText(label, text);
    clipboard.setPrimaryClip(clip);
    return true;
  }

  public String getClipboardTextA() {
    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
    if (!(clipboard.hasPrimaryClip())) {
      return null;
    }
    ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
    return item.coerceToText(this).toString();
  }

  public void toastA(String msg) {
    String t0 = msg;
    if (100 < msg.length()) {
      t0 = "Use 'Vw' to display larger contents ...\n" + msg.substring(0, 35) + "...";
    }
    Toast.makeText(this, t0, Toast.LENGTH_LONG).show();
  }

  // May be overridden ...
  protected void updateViews() {
    if (null != mView) {
      mView.postInvalidate();
    }
  }

  // =====

  ///// Any thread.

  void callUpdateViews() {
    runOnUiThread(
        new Runnable() {
          @Override
          public void run() {
            updateViews();
          }
        });
  }

  public static Activity getActivity() {
    return zActivity;
  }

  public static boolean pushClipboard(String label, String text) {
    final Dib2Activity act = zActivity;
    if (act instanceof Dib2Activity) {
      return act.pushClipboardA(label, text);
    }
    return false;
  }

  public static String getClipboardText() {
    final Dib2Activity act = zActivity;
    if (act instanceof Dib2Activity) {
      return act.getClipboardTextA();
    }
    return null;
  }

  public static void toast(String msg) {
    final Dib2Activity act = zActivity;
    if (act instanceof Dib2Activity) {
      act.toastA(msg);
    }
  }

  // =====
}
