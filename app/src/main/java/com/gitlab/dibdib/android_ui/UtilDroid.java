// Copyright (C) 2021, 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.android_ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;
import net.sf.dibdib.config.*;

public class UtilDroid {

  // private static boolean bPlaySound = false;
  private static byte[] generatedSnd = null;
  private static final int generatedSndDuration = 1; // seconds
  private static final int generatedSndSampleRate = 8000;

  public static void playSound(Context context, boolean force) {
    MediaPlayer mp = null;
    try {
      Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
      mp = (null == sound) ? null : MediaPlayer.create(context, sound);
      if (null == mp) {
        sound = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE);
        mp = (null == sound) ? null : MediaPlayer.create(context, sound);
        if (null == mp) {
          mp = MediaPlayer.create(context, Settings.System.DEFAULT_RINGTONE_URI);
        }
      }
      if (null != mp) {
        mp.start();
        try {
          Thread.sleep(Dib2Root.app.soundLength_msec);
        } catch (InterruptedException e) {
        }
        mp.stop();
        mp.release();
      }
    } catch (Exception e) {
    }
    if ((null == mp) || force) {
      try {
        AudioManager mgr = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int old = 0;
        if (null != mgr) {
          old = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
          mgr.setStreamVolume(
              AudioManager.STREAM_MUSIC, mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        }
        playGenSound();
        if (null != mgr) {
          mgr.setStreamVolume(AudioManager.STREAM_MUSIC, old, 0);
        }
      } catch (Exception e) {
      }
      return;
    }
  }

  public static void cancelSound() {
    // bPlaySound = false;
  }

  // Cmp. https://stackoverflow.com/questions/2413426/playing-an-arbitrary-tone-with-android,
  // @2021-07-17
  static void genTone() {
    final int numSamples = generatedSndDuration * generatedSndSampleRate;
    final double sample[] = new double[numSamples];
    final double freqOfTone = 440; // hz
    generatedSnd = new byte[2 * numSamples];
    for (int i = 0; i < numSamples; ++i) {
      sample[i] = Math.sin(2 * Math.PI * i / (generatedSndSampleRate / freqOfTone));
    }
    // convert to 16 bit pcm sound array
    // assumes the sample buffer is normalised.
    int idx = 0;
    for (final double dVal : sample) {
      // scale to maximum amplitude
      final short val = (short) ((dVal * 32767));
      // in 16 bit wav PCM, first byte is the low order byte
      generatedSnd[idx++] = (byte) (val & 0x00ff);
      generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
    }
  }

  static void playGenSound() {
    if (null == generatedSnd) {
      genTone();
    }
    final AudioTrack audioTrack =
        new AudioTrack(
            AudioManager.STREAM_MUSIC,
            generatedSndSampleRate,
            AudioFormat.CHANNEL_OUT_MONO,
            AudioFormat.ENCODING_PCM_16BIT,
            generatedSnd.length,
            AudioTrack.MODE_STATIC);
    audioTrack.write(generatedSnd, 0, generatedSnd.length);
    //        audioTrack.setVolume(
    audioTrack.play();
  }

  private static String[] logData = new String[40];
  private static long lastToast = 0;

  public static void logNToast(String xMsg) {
    long tim = System.currentTimeMillis();
    final String msg = xMsg + " @" + ((tim / 100) % 10000);
    System.arraycopy(logData, 0, logData, 1, logData.length - 1);
    logData[0] = msg;
    Log.d("Dib2", msg);
    final Object ctx = Dib2Activity.getActivity();
    if (ctx instanceof Dib2Activity) {
      if (Dib2Root.app.bNotificationToast && ((lastToast + 500) <= tim)) {
        final String log = getLogData();
        lastToast = tim;
        ((Dib2Activity) ctx)
            .runOnUiThread(
                new Runnable() {
                  @Override
                  public void run() {
                    int offs = (log.length() < 35) ? 0 : (log.length() - 35);
                    Toast.makeText(
                            (Context) ctx,
                            log.substring(offs).replace('\n', '.'),
                            Toast.LENGTH_SHORT)
                        .show();
                  }
                });
      }
    }
  }

  public static String getLogData() {
    StringBuilder log = new StringBuilder(999);
    for (String line : logData) {
      if (null != line) {
        log.append('\n');
        log.append(line);
      }
    }
    return log.toString();
  }

  private static SharedPreferences getPreferences(Context context) {
    return context.getSharedPreferences("Dib2_SHARED", 0);
  }

  public static void saveConfigValue(Context context, String key, String val) {
    SharedPreferences sharedPrefs = getPreferences(context);
    SharedPreferences.Editor it = sharedPrefs.edit();
    it.putString(key, val);
    it.apply();
  }

  public static String loadConfigValue(Context context, String key) {
    SharedPreferences sharedPrefs = getPreferences(context);
    return sharedPrefs.getString(key, "");
  }

  public static void setConfigValuesAll(Context context) {
    int flags =
        (Dib2Root.app.bStorage ? 1 : 0)
            | (Dib2Root.app.bWakeUp ? 2 : 0)
            | (Dib2Root.app.bAutostart ? 4 : 0)
            | (Dib2Root.app.bInternet ? 8 : 0)
            | (Dib2Root.app.bVibrate ? 0x10 : 0)
            | (Dib2Root.app.bNotificationSound ? 0x20 : 0)
            | (Dib2Root.app.bNotificationToast ? 0x40 : 0)
            | (Dib2Root.app.bAutoRefresh ? 0x80 : 0);
    SharedPreferences sharedPrefs = getPreferences(context);
    SharedPreferences.Editor it = sharedPrefs.edit();
    it.putString("bServiceThreadsHalted", Dib2Root.app.bServiceThreadsHalted ? "1" : "0");
    it.putString("bPermitted", Dib2Root.app.bPermitted ? "1" : "0");
    it.putString("bAllowDummyPass", Dib2Root.app.bAllowDummyPass ? "1" : "0");
    it.putString("appName", Dib2Root.app.appName);
    it.putString("appShort", Dib2Root.app.appShort);
    it.putString("mainClassName", Dib2Root.app.mainClassName);
    it.putString("flags", "" + flags);
    it.putString("alarmTime", "" + Dib2Root.app.alarmTime_msec);
    it.putString("jSound0", "" + Dib2Root.app.jSound0_msec);
    it.putString("jSound1", "" + Dib2Root.app.jSound1_msec);
    it.putString("soundLength", "" + Dib2Root.app.soundLength_msec);
    it.apply();
    // Dib2Root.app.appState = 0x3;
  }

  public static boolean loadConfigValuesAll(Context context) {
    if (Dib2Lang.AppState.CREATE != Dib2Root.app.appState) {
      return true;
    }
    SharedPreferences sharedPrefs = getPreferences(context);
    String val = sharedPrefs.getString("appName", "");
    if (3 > val.length()) {
      return false;
    }
    Dib2Root.app.appName = val;
    Dib2Root.app.appShort = sharedPrefs.getString("appShort", "");
    Dib2Root.app.bPermitted = ("1".equals(sharedPrefs.getString("bPermitted", "")));
    // Dib2Root.app.appState = 0x3;
    try {
      val = sharedPrefs.getString("bServiceThreadsHalted", "0");
      Dib2Root.app.bServiceThreadsHalted = "1".equals(val);
      val = sharedPrefs.getString("mainClassName", "");
      Dib2Root.app.mainClassName = val;
      val = sharedPrefs.getString("flags", "0");
      long num = Long.parseLong(val);
      Dib2Root.app.bStorage = (0 != (1 & num));
      Dib2Root.app.bWakeUp = (0 != (2 & num));
      Dib2Root.app.bAutostart = (0 != (4 & num));
      Dib2Root.app.bInternet = (0 != (8 & num));
      Dib2Root.app.bVibrate = (0 != (0x10 & num));
      Dib2Root.app.bNotificationSound = (0 != (0x20 & num));
      Dib2Root.app.bNotificationToast = (0 != (0x40 & num));
      Dib2Root.app.bAutoRefresh = (0 != (0x80 & num));
      val = sharedPrefs.getString("alarmTime", "0");
      Dib2Root.app.alarmTime_msec = Long.parseLong(val);
      val = sharedPrefs.getString("jSound0", "0");
      Dib2Root.app.jSound0_msec = (int) Long.parseLong(val);
      val = sharedPrefs.getString("jSound1", "0");
      Dib2Root.app.jSound1_msec = (int) Long.parseLong(val);
      val = sharedPrefs.getString("soundLength", "0");
      Dib2Root.app.soundLength_msec = (int) Long.parseLong(val);
    } catch (Exception e0) {
      return false;
    }
    return true;
  }
}
