// Copyright (C) 2022  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.android_ui;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import net.sf.dibdib.config.*;

public class Dib2Receiver extends BroadcastReceiver {

  private static AlarmManager alarmManager = null;
  private static PendingIntent pendingTimerIntent;
  private static int cIntent = 0;

  @Override
  public void onReceive(Context context, Intent intent) {
    String action = intent.getAction();
    UtilDroid.logNToast("RCV: " + action);
    if (!"1".equals(UtilDroid.loadConfigValue(context, "bPermitted"))) {
      return;
    }
    if ((null == action)
        || "1".equals(UtilDroid.loadConfigValue(context, "bServiceThreadsHalted"))) {
      if (null == action) {
        UtilDroid.logNToast("Received intent w/o action.");
      }
      return;
    }
    // Intent it = new Intent(context, Dib2Service.class);
    if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
      action = "START";
    } // else if TIMEZONE, ...
    Dib2Service.triggerService(context, action);
  }

  public static void startTimer(Context ctx) {
    UtilDroid.logNToast(
        "(RCV): startTimer " + !Dib2Root.app.bServiceThreadsHalted + (null == alarmManager));
    if (Dib2Root.app.bServiceThreadsHalted) {
      return;
    }
    if (null == alarmManager) {
      Intent serviceIntent = new Intent(ctx.getApplicationContext(), Dib2Service.class);
      serviceIntent.setPackage(ctx.getPackageName());
      serviceIntent.setAction("START");
      pendingTimerIntent =
          PendingIntent.getService(ctx, ++cIntent, serviceIntent, PendingIntent.FLAG_ONE_SHOT);
      alarmManager =
          (AlarmManager) ctx.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
    }
    final long tim = System.currentTimeMillis();
    if (tim > (999 + Dib2Root.app.alarmTime_msec + Dib2Root.app.jSound1_msec)) {
      Dib2Root.app.alarmTime_msec = 0;
    }
    if (0 < Dib2Root.app.alarmTime_msec) {
      long at = Dib2Root.app.alarmTime_msec;
      at -= Dib2Root.app.jSound0_msec + (tim & 0xf);
      if (((at - 10000) > (tim + Dib2Root.app.serviceInterval_msec))
          && (0 < Dib2Root.app.serviceInterval_msec)) {
        at = tim + Dib2Root.app.serviceInterval_msec;
      } else if (tim >= (at - 1999)) {
        at = tim + 1999;
      }
      UtilDroid.logNToast("(RCV): alarm +" + (at - tim));
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, at, pendingTimerIntent);
      } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, at, pendingTimerIntent);
      } else {
        alarmManager.set(AlarmManager.RTC_WAKEUP, at, pendingTimerIntent);
      }
    } else if (100 < Dib2Root.app.serviceInterval_msec) {
      long at = tim + Dib2Root.app.serviceInterval_msec;
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, at, pendingTimerIntent);
      } else {
        alarmManager.set(AlarmManager.RTC_WAKEUP, at, pendingTimerIntent);
      }
    }
  }

  public static void cancelTimer(boolean xbAll) {
    UtilDroid.logNToast("(RCV): cancel");
    if (null == alarmManager) {
      return;
    }
    alarmManager.cancel(pendingTimerIntent);
    alarmManager = null;
  }
}
