// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.android_ui;

import static net.sf.dibdib.config.Dib2Constants.*;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.*;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Style;
import android.util.DisplayMetrics;
import android.view.*;
import androidx.core.content.*;
import java.util.Arrays;
import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Lang.AppState;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.UiFunc;
import net.sf.dibdib.thread_ui.*;

public final class CanvasDroid extends View implements GraphicsIf {

  static DisplayMetrics qDm = null;

  private Canvas zCanvas;
  private Canvas zGraphics = null;
  private int zWidthPx = 320;
  private int zHeightPx = 320;
  private boolean zMeasured = false;
  private final Paint mTextPaint;
  private final Paint mToolPaint;
  private final Rect zBounds = new Rect();
  private Bitmap[] zImages = new Bitmap[8];
  private int[] zImageBorderColor = new int[8];
  private Canvas zClipped = null;
  private Bitmap zClipBm = null;
  private int zClipX = 0;
  private int zClipY = 0;

  private static final String kDigits = "0123456789";

  public CanvasDroid(Context context) {
    super(context);
    setFocusable(true);
    setFocusableInTouchMode(true);
    mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    mTextPaint.setTextAlign(Paint.Align.LEFT);
    mToolPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    mToolPaint.setStyle(Paint.Style.FILL_AND_STROKE);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    // Frame size:
    zWidthPx = MeasureSpec.getSize(widthMeasureSpec);
    zHeightPx = MeasureSpec.getSize(heightMeasureSpec);
    setMeasuredDimension(zWidthPx, zHeightPx);
    zMeasured = true;
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    float x = event.getX();
    float y = event.getY();
    if (Dib2Root.app.appState.ordinal() < AppState.DISCLAIMER.ordinal()) {
      // || (!Dib2Root.app.bPermitted && !Dib2Root.app.bAllowDummyPass)) {
      invalidate();
      return false;
    }
    boolean done = true;
    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        UiPres.INSTANCE.handleMouse((int) x, (int) y);
        return true;
      case MotionEvent.ACTION_MOVE:
      case MotionEvent.ACTION_CANCEL:
        break;
      case MotionEvent.ACTION_UP:
        ClickRepeater.stopMouseRep();
        invalidate();
        return true;
      default:
        done = false;
    }
    ClickRepeater.stopMouseRep();
    return done;
  }

  private Rect drawCharMono_size = null;

  /** Workaround for Android GUI bug regarding fallback for monospaced glyphs. */
  private void drawCharMono(Canvas canvas, char ch, int nX, int nY) {
    final String sCh = "" + ch;
    if (0x7f <= ch) {
      Rect bounds = new Rect();
      if (null == drawCharMono_size) {
        drawCharMono_size = new Rect();
        mTextPaint.getTextBounds(new char[] {'N', 'n', 'g'}, 0, 3, drawCharMono_size);
      }
      mTextPaint.getTextBounds(new char[] {'N', ch, 'g'}, 0, 3, bounds);
      if ((drawCharMono_size.right + 2) < bounds.right) {
        nX += -2 + (drawCharMono_size.right - bounds.right) / 2;
      } else if ((drawCharMono_size.right - 2) > bounds.right) {
        nX += (drawCharMono_size.right - bounds.right) / 2;
      }
      if ((drawCharMono_size.bottom + 1) < bounds.bottom) {
        if ((drawCharMono_size.top - 1) > bounds.top) {
          nY += -2 + (drawCharMono_size.bottom - bounds.bottom) / 2;
        } else {
          nY += (drawCharMono_size.bottom - bounds.bottom);
        }
      } else if ((drawCharMono_size.top - 2) > bounds.top) {
        nY += (drawCharMono_size.top - bounds.top) / 2;
      }
    } else if (' ' > ch) {
      return;
    }
    canvas.drawText(sCh, nX, nY, mTextPaint);
  }

  @Override
  protected void onDraw(Canvas xGr) {
    zCanvas = xGr;
    zGraphics = zCanvas;
    xGr.translate(0, 0);

    final Object act = Dib2Activity.getActivity();
    String txt = (null != act) ? "Starting ..." : "Missing data ...";
    boolean permitted = (null == act) || (null == Dib2Activity.checkPermission_requesting);
    if (!permitted) {
      permitted = true;
      txt = "Need permission:";
      for (String perm : Dib2Activity.checkPermission_requesting) {
        if (ContextCompat.checkSelfPermission((Dib2Activity) act, perm)
            != PackageManager.PERMISSION_GRANTED) {
          permitted = false;
          txt += " " + perm;
        }
      }
      if (permitted) {
        Dib2Activity.checkPermission_requesting = null;
        UtilDroid.saveConfigValue((Dib2Activity) act, "bPermitted", "1");
      }
      Dib2Root.app.bPermitted = permitted;
    }
    if ((null == act) || (null == qDm) || (null == FrameDroid.mFrame) || !permitted) {
      if (null != mToolPaint) {
        mTextPaint.setTextSize(zCanvas.getWidth() / 20 + 8);
        mToolPaint.setColor(0xff444444);
        mToolPaint.setStyle(Style.FILL);
        zCanvas.drawRect(0, 0, zCanvas.getWidth(), zCanvas.getHeight(), mToolPaint);
        mToolPaint.setStyle(Style.STROKE);
        mTextPaint.setColor(0xffff0000);
        zCanvas.drawText(txt, 10, zCanvas.getHeight() / 4, mTextPaint);
        mToolPaint.setColor(0xff808080);
        mTextPaint.setColor(0xff808080);
      }
      return;
    }

    if (zMeasured) {
      zMeasured = false;
      mTextPaint.setTextSize(FrameDroid.mFrame.getTextSize4Ui());
      int xDpi = (UI_PT_P_INCH > qDm.xdpi) ? UI_PT_P_INCH : (int) qDm.xdpi;
      int yDpi = (UI_PT_P_INCH > qDm.ydpi) ? UI_PT_P_INCH : (int) qDm.ydpi;
      final int widPt10 = (zWidthPx * Dib2Constants.UI_PT10_P_INCH) / xDpi;
      final int hghPt10 = (zHeightPx * Dib2Constants.UI_PT10_P_INCH) / yDpi;
      UiFrame.checkSizeNZoom(zWidthPx, zHeightPx, (widPt10 < hghPt10) ? widPt10 : hghPt10);
    }
    if (!FrameDroid.mFrame.paint4Frame(this)) {
      if (Dib2Root.app.appState.ordinal() >= AppState.EXIT_DONE.ordinal()) {
        ((Dib2Activity) act).exit();
      }
    }
  }

  ///// QGraphicsIf

  private Rect getBounds(String txt, Rect bounds) {
    mTextPaint.getTextBounds(txt, 0, txt.length(), bounds);
    return bounds;
  }

  @Override
  public void drawImage(int index, int left, int top) {
    final int wid = zImages[index].getWidth();
    final int hgh = zImages[index].getHeight();
    zCanvas.drawBitmap(zImages[index], left, top, mToolPaint);
    if (0 != zImageBorderColor[index]) {
      mToolPaint.setColor(zImageBorderColor[index]);
      zCanvas.drawRect(left - 1, top - 1, left + wid, top + hgh, mToolPaint);
    }
    zGraphics = zCanvas;
    zClipped = null;
  }

  @Override
  public void drawLine(int startX, int startY, int stopX, int stopY) {
    zGraphics.drawLine(startX, startY, stopX, stopY, mToolPaint);
  }

  @Override
  public void drawText(String text, int x, int y) {
    zGraphics.drawText(text, x, y, mTextPaint);
  }

  @Override
  public int getBoundsLeft(String txt) {
    return getBounds(txt, zBounds).left;
  }

  @Override
  public int getBoundsRight(String txt) {
    return getBounds(txt, zBounds).right;
  }

  @Override
  public int getBoundsMiddle(String txt) {
    getBounds(txt, zBounds);
    return (1 + zBounds.left + zBounds.right) >> 1;
  }

  int setBackground_color = 0xffffffff;

  public void setBackground(int xColor) {
    setBackground_color = xColor;
  }

  @Override
  public void setCanvasImage(
      int index, int width, int height, int colorBackground, int colorBorder) {
    if (null != zClipped) {
      zGraphics = zClipped;
      zGraphics.drawBitmap(zClipBm, zClipX, zClipY, mToolPaint);
      zClipped = null;
      zClipBm = null;
    }
    if (zImages.length <= index) {
      zImages = Arrays.copyOf(zImages, index + 8);
      zImageBorderColor = Arrays.copyOf(zImageBorderColor, index + 8);
    }
    setBackground(colorBackground);
    if (null == zImages[index]) {
      zImages[index] = Bitmap.createBitmap(width, height, Config.ARGB_8888);
    }
    zImageBorderColor[index] = colorBorder;
    // Leave one pixel for border line:
    if (0 != colorBorder) {
      --width;
      --height;
    }
    zGraphics = new Canvas(zImages[index]);
    mToolPaint.setColor(setBackground_color);
    mToolPaint.setStyle(Style.FILL);
    zGraphics.drawRect(0, 0, width, height, mToolPaint);
    mToolPaint.setStyle(Style.STROKE);
    zGraphics.drawRect(0, 0, width, height, mToolPaint);
    mToolPaint.setColor(0xff808080);
  }

  @Override
  public void setClip(int left, int top, int right, int bottom) {
    // Region.Op.REPLACE not supported ...
    // zGraphics.clipRect(left, top, right, bottom, Region.Op.REPLACE);
    if (null != zClipped) {
      zGraphics = zClipped;
      zGraphics.drawBitmap(zClipBm, zClipX, zClipY, mToolPaint);
      zClipped = null;
      zClipBm = null;
    }
    if ((0 == left) && (0 == top)) {
      if ((right >= zGraphics.getWidth()) && (bottom >= zGraphics.getHeight())) {
        return;
      }
    }
    zClipped = zGraphics;
    zClipBm = Bitmap.createBitmap(right - left, bottom - top, Config.ARGB_8888);
    zGraphics = new Canvas(zClipBm);
    zGraphics.translate(-left, -top);
    zClipX = left;
    zClipY = top;
  }

  @Override
  public void setColor(int xColor) {
    mTextPaint.setColor(xColor);
    mToolPaint.setColor(xColor);
  }

  @Override
  public void setColorText(int xColor) {
    mTextPaint.setColor(xColor);
  }

  @Override
  public void setColorTool(int xColor) {
    mToolPaint.setColor(xColor);
  }

  @Override
  public int setMatching128Font(int xHeightPx, String font, int type) {
    // TODO
    return setMatching128FontHeight(xHeightPx);
  }

  private Rect setMatching128FontHeight_size = new Rect();
  private int setMatching128FontHeight_cmp128 = 0;

  @Override
  public int setMatching128FontHeight(int xHeightPx) {
    xHeightPx = (2 < xHeightPx) ? xHeightPx : 2;
    // TODO
    mTextPaint.setTextSize(xHeightPx);
    if (2 >= xHeightPx) {
      return xHeightPx;
    }
    mTextPaint.getTextBounds(kDigits, 0, kDigits.length(), setMatching128FontHeight_size);
    final int wid = setMatching128FontHeight_size.right - setMatching128FontHeight_size.left;
    if (0 >= setMatching128FontHeight_cmp128) {
      setMatching128FontHeight_cmp128 = UiFunc.boundWidthMono(kDigits, 128);
    }
    final int cmp = (setMatching128FontHeight_cmp128 * xHeightPx) >> 7;
    if ((wid - kDigits.length()) > cmp) {
      --xHeightPx;
      if ((7 < xHeightPx) && ((wid - 2 * kDigits.length()) > cmp)) {
        --xHeightPx;
      }
      mTextPaint.setTextSize(xHeightPx);
    }
    return xHeightPx;
  }

  @Override
  public void show() {
    // Nothing to do.
  }
}
