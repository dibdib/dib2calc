// Copyright (C) 2020, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2calc;

import android.content.*;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.*;
import com.gitlab.dibdib.android_ui.*;
import java.io.File;
//import com.gitlab.dibdib.picked.net.MessengerQm;
import net.sf.dibdib.config.*;
//import net.sf.dibdib.thread_net.QOpNet;


public final class Main extends Dib2Activity {
  // =====

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Dib2Service.qIconNotificationOff = R.drawable.ic_notific_off;
    Dib2Service.qIconNotificationOn = R.drawable.ic_notific_on;
    super.onCreate(savedInstanceState);
  }

  @Override
  protected void createStep0(File mainDir, boolean restrict) {

    File fp = new File(mainDir, Dib2Root.PASS_FILENAME);
    Dib2Root.app.bAllowDummyPass = !fp.isFile(); // true
    //QOpNet.messenger = new MessengerQm();
    Dib2Root.app.appShort = "calc";
    // Dib2Root.app.bWakeUp = true;
    Dib2Root.app.bNotificationToast = Dib2Constants.DEBUG;
    Dib2Root.app.mainClassName = Main.class.getName();
  }

  public static void sendWA() {

    //  if (contact.equals(null)) {
    //  try {
    //    // case 1:
    //    String smsNumber = contact.phone_get().substring(1); // without '+'
    //    Intent sendIntent = new Intent("android.intent.action.MAIN");
    //    sendIntent.setAction(Intent.ACTION_SEND);
    //    // sendIntent.setType("text/plain");
    //    // sendIntent.putExtra(Intent.EXTRA_TEXT, "text");
    //    Uri uri = Uri.fromFile(null); // file in download area
    //    sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
    //    // SetType("application/pdf")
    //    sendIntent.setType("application/vnd.ms-word");
    //    sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net");
    //    sendIntent.setPackage("com.whatsapp");
    //    startActivity(sendIntent);
    //  } catch (Exception e) {
    //    // Toast.makeText(this, "Error " + e, Toast.LENGTH_SHORT).show();
    //  }
    // }
  }

  public Object uriFromMediaLibrary(
      String name, String filename, String basetype, String type, long time) {
    Object uri;
    if (basetype.equals("image")) {
      ContentValues values = new ContentValues(5);
      values.put(Images.Media.TITLE, filename);
      values.put(Images.Media.DISPLAY_NAME, name);
      values.put(Images.Media.DATE_TAKEN, time);
      values.put(Images.Media.MIME_TYPE, type);
      values.put(Images.Media.DATA, name);
      ContentResolver cr = getContentResolver();
      uri = cr.insert(Images.Media.EXTERNAL_CONTENT_URI, values);
    } else if (basetype.equals("video")) {
      ContentValues values = new ContentValues(5);
      values.put(Video.Media.TITLE, filename);
      values.put(Video.Media.DISPLAY_NAME, name);
      values.put(Video.Media.DATE_TAKEN, time);
      values.put(Video.Media.MIME_TYPE, type);
      values.put(Video.Media.DATA, name);
      ContentResolver cr = getContentResolver();
      uri = cr.insert(Video.Media.EXTERNAL_CONTENT_URI, values);
    } else if (basetype.equals("audio")) {
      ContentValues values = new ContentValues(5);
      values.put(Audio.Media.TITLE, filename);
      values.put(Audio.Media.DISPLAY_NAME, name);
      values.put(Audio.Media.DATE_ADDED, time);
      values.put(Audio.Media.MIME_TYPE, type);
      values.put(Audio.Media.DATA, name);
      ContentResolver cr = getContentResolver();
      uri = cr.insert(Audio.Media.EXTERNAL_CONTENT_URI, values);
    } else {
      uri = Uri.parse(name);
    }
    return uri;
  }

  // =====
}
